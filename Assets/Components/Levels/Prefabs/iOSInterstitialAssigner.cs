﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class iOSInterstitialAssigner : AssignerBase, IAssigner{
	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{	
		switch (args.EventName) {
		case "on_show_interstitial"		: Do ("ShowInterstitial"); break;
		case "on_load_interstitial"		: Do ("LoadInterstitial"); break;
		}
	}

	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
		switch (args.EventName) {
		case "EventOnInterstitialNotLoaded":
			SendActiveLogic ("end_interstitial_not_loaded", args.Args);
			break;
		case "EventOnInterstitialShowed":
			SendActiveLogic ("end_interstitial_showed", args.Args);
			break;
		}
	}
}
