﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using ru.appforge.logic;

public class iOSInterstitialAdapter : GameComponentBase, IGameComponent {

	private string idBanner = "ca-app-pub-3936176332568699/9854853083";
	private string idInterstitial = "ca-app-pub-3936176332568699/1068505885";
	private static InterstitialAd interstitial;
	public static BannerView bannerView;

	public override event ru.appforge.logic.DLogicEvent OnResult;

	IEnumerator Start ()
	{
		if (AdsOffComponent.IsAdEnabled) {
			RequestBanner ();
			bannerView.Show ();
		}
		
		yield return new WaitForEndOfFrame ();
	}

	private void RequestBanner ()
	{
		// Create a 320x50 banner at the top of the screen.
		if (bannerView == null)
			bannerView = new BannerView (idBanner, AdSize.SmartBanner, AdPosition.Bottom);

		bannerView.LoadAd (createAdRequest ());
	}

	public void LoadInterstitial ()
	{
		Debug.Log(2222);
//		debugOnGUI += "interstitial load\n";
		RequestInterstitial ();
	}

	public void ShowInterstitial(){
		Debug.Log ("1111");
		if (interstitial != null && interstitial.IsLoaded ()) {
			interstitial.Show ();
//			debugOnGUI += "interstitial show\n";
		} else {
			//StartCoroutine(TryToShow());
//			debugOnGUI += "EventOnInterstitialNotLoadedw\n";
			OnResult (this, new LogicEventArgs ("EventOnInterstitialNotLoaded"));
		}
	}

	string debugOnGUI = "";

	void OnGUI () 
	{
		GUILayout.Label (debugOnGUI);
	}

	IEnumerator TryToShow ()
	{
		float counter = 0;
		while ((counter += Time.deltaTime) < 3.0f || interstitial.IsLoaded () == false)
			yield return new WaitForEndOfFrame ();
		if (interstitial.IsLoaded ())
			interstitial.Show ();
		else
			OnResult (this, new LogicEventArgs ("EventOnInterstitialNotLoaded"));
	}

	private void RequestInterstitial ()
	{
		// Create an interstitial.
		interstitial = new InterstitialAd (idInterstitial);
		// Load an interstitial ad.
		interstitial.AdClosed += HandleAdClosed;
		interstitial.LoadAd (createAdRequest ());
	}

	void HandleAdClosed (object sender, System.EventArgs e)
	{
//		debugOnGUI += "EventOnInterstitialShowed\n";
		OnResult (this, new LogicEventArgs ("EventOnInterstitialShowed"));
	}

	private AdRequest createAdRequest ()
	{
//		return new AdRequest.Builder ()
//			.AddTestDevice (AdRequest.TestDeviceSimulator)
//				.AddTestDevice ("0123456789ABCDEF0123456789ABCDEF")
//				.AddKeyword ("game")
//				.SetGender (Gender.Male)
//				.SetBirthday (new System.DateTime (1985, 1, 1))
//				.TagForChildDirectedTreatment (false)
//				.AddExtra ("color_bg", "9B30FF")
//				.Build ();
		return new AdRequest.Builder ().Build ();
		
	}
}
