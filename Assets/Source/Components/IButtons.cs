﻿using System;
using ru.appforge.logic;

interface IButtons
{		
	void SwitchMode (string nameMode);

	void EnableBtn (bool enable, string name);

	void EnableButton (bool enabled, string[] names);
}
