﻿using UnityEngine;
using System.Collections;

public class tk2dSimpleButtonWithText : tk2dSimpleButton
{
	public tk2dTextMesh textOnButton;
	public bool SaveColor = false;
	public float dx = 0, dy = 0;

	public override void Disable ()
	{
		//Debug.LogError(" DISABLE in SBWT");
		base.Disable ();
		if (textOnButton != null)
			textOnButton.color = new Color (textOnButton.color.r, textOnButton.color.g, textOnButton.color.b, 0.7f);
	}

	public override void Enable ()
	{
		//Debug.LogError(" DISABLE in SBWT");
		base.Enable ();
		if (textOnButton != null && ! SaveColor) 
			textOnButton.color = new Color (1, 1, 1, 1);
	}

	public override void Down ()
	{
		base.Down ();
		if (textOnButton != null)
			textOnButton.transform.position = new Vector3 (textOnButton.transform.position.x - dx, textOnButton.transform.position.y - dy, textOnButton.transform.position.z);
	}

	public override void Up ()
	{
		base.Up ();
		if (textOnButton != null)
			textOnButton.transform.position = new Vector3 (textOnButton.transform.position.x + dx, textOnButton.transform.position.y + dy, textOnButton.transform.position.z);
	}
}
