﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ru.appforge.logic
{
    public interface ILogic
    {
		bool Active { get; set; }

		bool Busy { get; set; }

        ILogicController Parent { get; set; }

        void OnEnter (params object[] args);

        void Do (params object[] args);

        void OnExit (params object[] args);
    }
}
