﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ru.appforge.logic
{
    public interface ILogicController 
    {
        ILogic Active { get; set; }
        void SetActiveLogic (ILogic logic, params object[] args);
    }

    public interface IUnityLogicController : ILogicController
    {
        void OnUpdate (params object[] args);
    }

    public interface ILogicControllerAdvanced : ILogicController, IEnumerable
    {
        void SetActiveLogic (string name, params object[] args);
        ILogic this[string name] { get; }
        void Remove (string name);
        void Add (string name, ILogic logic);
        int Count { get; }
		
		void Do(params object[] args);
        event DLogicEvent OnGlobalEvent;
		void DoGlobalEvent(object sender, LogicEventArgs args);

		void Init ();
		void OnUpdate (params object[] args);
		void Refresh ();
    }	
}
