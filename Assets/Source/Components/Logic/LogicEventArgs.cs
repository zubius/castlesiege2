﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ru.appforge.logic
{
    public delegate void DLogicEvent (object sender, LogicEventArgs args);

    public class LogicEventArgs : System.EventArgs
    {
        public object[] Args { get; set; }
        public string EventName { get; set; }

        public LogicEventArgs (string eventName, params object[] args)
        {
            this.EventName = eventName;
            this.Args = args;
        }

        public object this[int index]
        {
            get { return this.Args[index]; }
            set { this.Args[index] = value; }
        }

        public LogicEventArgs () : this ("") { }
    }
}
