using System;
using ru.appforge.logic;

public interface IGameComponent
{
	/// <summary>
	/// Возникает при завершении выполнения действий компонента
	/// </summary>
	event DLogicEvent OnResult;
	
    /// <summary>
    /// Указывает 
    /// </summary>
    bool IsShow { get; set; }

    /// <summary>
    /// Отображает текущий элемент
    /// </summary>	
    void Show (params object[] args);

    /// <summary>
    /// Скрывает текущий эелемент
    /// </summary>
    void Hide (params object[] args);

    /// <summary>
    /// Do the specified args.
    /// </summary>	
    void Do (params object[] args);
}
