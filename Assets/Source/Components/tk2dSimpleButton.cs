using UnityEngine;
using System.Collections;

[AddComponentMenu ("2D Toolkit/GUI/tk2dSimpleButton")]
public class tk2dSimpleButton : GameComponentBase, IGameComponent
{
	public event System.EventHandler OnClickEnable;
	public event System.EventHandler OnClickDisable;
	public event System.EventHandler OnClickInactive;

	public Camera viewCamera;

	public KeyCode buttonKeyPress;

	public string buttonDownSprite = "";
	public string buttonUpSprite = "";
	public string buttonPressedSprite = "";
	public string buttonDisabledSprite = "";

	public AudioClip buttonDownSound = null;
	public AudioClip buttonUpSound = null;
	public AudioClip buttonPressedSound = null;

	public bool buttonDisable = false;

	public bool ButtonDisable { get { return this.buttonDisable; } set { buttonDisable = value; } }

	private float durationOfPressing = 0.0f;

	public bool Enabled
	{
		get { return !buttonDisable; }
		set { if (value) Enable (); else Disable (); }
	}

	public virtual void Disable ()
	{
		this.buttonDisable = true;
		if (buttonDisabledSpriteId != -1) {
			sprite.spriteId = buttonDisabledSpriteId;
		}
	}

	public virtual void Enable ()
	{
		if (this.buttonDisable) {
			this.buttonDisable = false;
			if (buttonUpSpriteId != -1)
				sprite.spriteId = buttonUpSpriteId;
		}
	}

	bool buttonPressed = false;

	public GameObject targetObject = null;
	/// <summary>
	/// The message to send to the object. This should be the name of the method which needs to be called.
	/// </summary>
	public string messageName = "";

	public bool clickByDown = false;

	tk2dBaseSprite sprite;

	int buttonDownSpriteId = -1, buttonUpSpriteId = -1, buttonDisabledSpriteId = -1, buttonPressedSpriteId = -1;

	// Use this for initialization
	void Start ()
	{
		if (viewCamera == null) {
			// Find a camera parent
			Transform node = transform;
			while (node && node.camera == null) {
				node = node.parent;
			}
			if (node && node.camera != null) {
				viewCamera = node.camera;
			}

			// See if a tk2dCamera exists
			//			if (viewCamera == null && tk2dCamera.inst)
			//			{
			//				viewCamera = tk2dCamera.inst.mainCamera;
			//			}

			// ...otherwise, use the main camera
			if (viewCamera == null) {
				viewCamera = Camera.main;
			}
		}

		viewCamera = Camera.main;

		sprite = GetComponent<tk2dBaseSprite> ();
		RefreshSpriteId ();
		RefreshButton ();

		if (collider == null) {
			BoxCollider boxCollider = gameObject.AddComponent<BoxCollider> ();
			Vector3 size = boxCollider.size;
			size.z = 0.2f;
			boxCollider.size = size;
		}
	}

	void RefreshSpriteId ()
	{
		if (sprite) {
			// Change this to use animated sprites if necessary
			// Same concept here, lookup Ids and call Play(xxx) instead of .spriteId = xxx

			buttonDownSpriteId = buttonDownSprite.Length > 0 ? sprite.GetSpriteIdByName (buttonDownSprite) : -1;
			buttonUpSpriteId = buttonUpSprite.Length > 0 ? sprite.GetSpriteIdByName (buttonUpSprite) : -1;
			buttonPressedSpriteId = buttonPressedSprite.Length > 0 ? sprite.GetSpriteIdByName (buttonPressedSprite) : -1;
			buttonDisabledSpriteId = buttonDisabledSprite.Length > 0 ? sprite.GetSpriteIdByName (buttonDisabledSprite) : -1;
		}
	}

	public void RefreshButton ()
	{
		RefreshSpriteId ();
		this.buttonDisable = !this.buttonDisable;
		Enabled = this.buttonDisable;
	}

	private void PlaySound (AudioClip source)
	{
		if (audio && source)
			audio.PlayOneShot (source);
	}

	public virtual void Down ()
	{
		durationOfPressing = 0.0f;

		if ( ! buttonDisable && this.buttonDownSpriteId != -1)
			this.sprite.spriteId = buttonDownSpriteId;

		if (buttonDisable && this.buttonPressedSpriteId != -1)
//			this.sprite.spriteId = buttonPressedSpriteId;

		if (targetObject && messageName.Length > 0) 
			if (clickByDown && !buttonDisable)
				targetObject.SendMessage (messageName);		

		SendEventClick (clickByDown);

		buttonPressed = true;
	}

	public virtual void Up ()
	{
		if ( ! buttonDisable && this.buttonUpSpriteId != -1)
			this.sprite.spriteId = buttonUpSpriteId;

		if (buttonDisable && this.buttonDisabledSpriteId != -1)
			sprite.spriteId = this.buttonDisabledSpriteId;

		if (targetObject && messageName.Length > 0) {
			if (!clickByDown && !this.buttonDisable)
				targetObject.SendMessage (messageName);
		}

		SendEventClick ( ! clickByDown);

		buttonPressed = false;
	}

	// Update is called once per frame
	void Update ()
	{
//		if (Random.Range (0, 7) == 0) {
//			if (Application.loadedLevelName == "Lobby") return;
//			if (this.name == "btnAbout" 
//			    || this.name == "btnNo" 
//			    || this.name == "lobby"
//			    || this.name == "mode"
//			    ) { 
//				return;
//			}
//
//			if (Vector3.Distance (this.transform.position, new Vector3 (0.0f, 0.0f, 0.0f)) > 5.0f) return;
//
//			int count = 1 + (Random.Range (0, 5) == 0 ? 1 : 0);
//			
//			while (count-- > 0) {
//				Down ();
//				Up ();
//			}
//			return;
//		}

		if (Input.GetKeyDown (buttonKeyPress)) {
			Down ();
			return;
		}

		if ( ! buttonPressed && Input.GetMouseButtonDown (0)) {
			Ray ray = viewCamera.ScreenPointToRay (Input.mousePosition);
			RaycastHit hitInfo;
			if (collider.Raycast (ray, out hitInfo, 1.0e8f)) {
				if (!Physics.Raycast (ray, hitInfo.distance - 0.01f)) {
					Down ();
				}
			}
		}

		// Поднятие кнопки 
		if (buttonPressed && (Input.GetMouseButtonUp (0) || Input.GetKeyUp (buttonKeyPress))) Up ();		

		// Хуита чистой воды!
		// if ( ! Input.GetMouseButton (0) && !Input.GetKeyDown (buttonKeyPress)) {
		//     if (buttonPressed)
		// 	   Up ();
		// }

		if (buttonDisable && ! buttonPressed) 
			if (this.buttonDisabledSpriteId != -1)
				sprite.spriteId = this.buttonDisabledSpriteId;		

		if (buttonPressed) {
			durationOfPressing += Time.deltaTime;
//			Debug.LogError (durationOfPressing);
		}
	}

	private void SendEventClick (bool downClick)
	{
		if (buttonDisable) {
			if ( ! downClick && OnClickDisable != null)
				OnClickDisable (this, new SimpleButtonEventArgs ("", durationOfPressing, clickByDown));

			if (downClick && OnClickInactive != null)
				OnClickInactive (this, new SimpleButtonEventArgs ("", durationOfPressing, clickByDown));

		} else {
			if (downClick && OnClickEnable != null)
				OnClickEnable (this, new SimpleButtonEventArgs ("", durationOfPressing, clickByDown));
		}
	}

	// ------------------------------------------------------------------------

	public void SendPress ()
	{
		Down ();
	}
}

public class SimpleButtonEventArgs : System.EventArgs
{
	public string EventName { get; set; }
	public object[] Args { get; set; }
	
	public SimpleButtonEventArgs (string eventName, params object[] args)
	{
		this.EventName = eventName;
		this.Args = args;
	}
}
