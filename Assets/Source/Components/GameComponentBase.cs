using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public abstract class GameComponentBase : MonoBehaviour, IGameComponent
{
    #region IComponent implementation
	
	/// <summary>
	/// Возникает при завершении выполнения действий компонента
	/// </summary>
	public virtual event DLogicEvent OnResult;
	
	/// <summary>
	/// Указывает 
	/// </summary>
	public virtual bool IsShow { get; set; }

	/// <summary>
	/// Отображает текущий элемент
	/// </summary>	
	public virtual void Show (params object[] args)
	{
		SetRenderer (true);
	}

	/// <summary>
	/// Скрывает текущий эелемент
	/// </summary>
	public virtual void Hide (params object[] args)
	{
		SetRenderer (false);
	}

	private void SetRenderer (bool enable)
	{
		try { transform.renderer.enabled = enable; }
		catch { }
		for (int i = 0; i < transform.childCount; i++) {
			try { transform.GetChild (i).renderer.enabled = enable; }
			catch { }
		}
	}

	/// <summary>
	/// Do the specified args.
	/// </summary>	
	public virtual void Do (params object[] args)
	{

	}

    #endregion
}
