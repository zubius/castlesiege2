using UnityEngine;
using System.Collections;
using ru.appforge.logic;
using System.Collections.Generic;

public class ButtonsBase : GameComponentBase, IGameComponent, IButtons
{
//    public tk2dSimpleButton[] buttons;

    public override event DLogicEvent OnResult;

	public List<tk2dSimpleButton> buttons = new List<tk2dSimpleButton> ();

    // Use this for initialization
    public void Start ()
    {
		foreach (tk2dSimpleButton button in buttons) {
			button.OnClickEnable -= OnButtonsClickEnable;
			button.OnClickEnable += OnButtonsClickEnable;
		}

		foreach (tk2dSimpleButton button in buttons) {
			button.OnClickDisable -= OnButtonsClickDisable;
			button.OnClickDisable += OnButtonsClickDisable;
		}

		foreach (tk2dSimpleButton button in buttons) {
			button.OnClickInactive -= OnButtonsClickInactive;
			button.OnClickInactive += OnButtonsClickInactive;
		}

		Initialization ();
    }

	public void UaMode (bool mode) 
	{
		if (mode) {
			GameObject obj = new GameObject ("payout", typeof (tk2dSimpleButton));
			obj.transform.parent = this.transform;
			tk2dSimpleButton button = obj.GetComponent <tk2dSimpleButton> ();
			button.buttonKeyPress = KeyCode.P;
			button.OnClickEnable += OnButtonsClickEnable;

			// Скрываем render всем кнопкам
			foreach (var item in buttons) 
				item.renderer.enabled = ! MainSettings.disableKeys;

			ru.appforge.utils.U.Insert (buttons.ToArray(), button, buttons.Count);
//			ru.appforge.utils.U.Insert (buttons, button, buttons.Length);
		}
	}

	public virtual void Initialization ()
	{

	}

    protected void OnButtonsClickEnable (object sender, System.EventArgs e)
    {
		SendEvent ("on_button_click", ((tk2dSimpleButton) sender).gameObject.name, (float) (e as SimpleButtonEventArgs).Args[0]);
    }

	private void OnButtonsClickDisable (object sender, System.EventArgs e)
	{
		SendEvent ("on_button_click_disable", ((tk2dSimpleButton) sender).gameObject.name, 0.0f);
	}

	private void OnButtonsClickInactive (object sender, System.EventArgs e)
	{
		SendEvent ("on_button_click_not_active", ((tk2dSimpleButton) sender).gameObject.name, (float) (e as SimpleButtonEventArgs).Args[0]);
	}

	protected virtual void SendEvent (string sendEventName, string buttonName, float durationOfPressing) 
	{
		if (OnResult != null) OnResult (this, new LogicEventArgs (sendEventName, buttonName, durationOfPressing));
	}

	#region IButtons implementation

	public virtual void SwitchMode (string nameMode)
	{

	}

	public virtual void EnableBtn (bool enable, string name)
	{
		foreach (tk2dSimpleButton button in buttons) {
			if (button != null && name == button.gameObject.name)
				button.Enabled = enable;
		}
	}

	public virtual void EnableButton (bool enabled, string[] names)
	{
		foreach (tk2dSimpleButton btn in buttons)
            btn.Enabled = ! enabled;

        foreach (string name in names)
            EnableBtn (enabled, name);
	}

	public virtual void EnableSelect (bool enabled, string[] names)
	{
		foreach (string name in names)
            EnableBtn (enabled, name);
	}

	#endregion
}