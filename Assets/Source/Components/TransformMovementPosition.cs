﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public class TransformMovementPosition : GameComponentBase, IGameComponent
{	
	private delegate float DProcessing (float arg);
	
	public enum TypeLaw { Linearly, ParabolicAcceleration, ParabolicBraking, HyperbolicAcceleration, HyperbolicBraking, Sinusoidal }
	
	public System.Action <object> OnResult;
	
	[DefaultValue (false)]
	public bool IsMoving { get; set; }
	
	public TypeLaw typeLawDisplacement;
	
	private float counterTime = 0.0f;
	
	private Vector3 positionStart, positionFinish;
	
	private float totalTime = 0.0f;
	
	private DProcessing processing;

	private bool pause = false;
	
	// Update is called once per frame
	void Update ()
	{	
		if (pause)
		return;
		if (IsMoving) {
			if (counterTime <= 0) {
				Finished ();
			} else {
				counterTime -= Time.deltaTime;
				Iteration (counterTime);
			}
		}
	}

	public void Pause(params object[] args) {
		pause = (bool)args[0];
	}
	
	public void Move (Vector2 finishPosition, float time) 
	{
		Move (this.transform.position, new Vector3 (finishPosition.x, finishPosition.y, this.transform.position.z), time);
	}
	
	public void Move (Vector3 finishPosition, float time) 
	{
		Move (this.transform.position, finishPosition, time);
	}
	
	public void Move (Vector2 startPosition, Vector2 finishPosition, float time)
	{
		float z = this.transform.position.z;
		Move (new Vector3 (startPosition.x, startPosition.y, z), new Vector3 (finishPosition.x, finishPosition.y, z), time);
	}
	
	public void Move (Vector3 startPosition, Vector3 finishPosition, float time) 
	{
		IsMoving = true;
		counterTime = time;
		totalTime = time;
		this.positionStart = startPosition;
		this.positionFinish = finishPosition;
		
		processing = SetProcessing ();
	}
	
	private DProcessing SetProcessing ()
	{
		switch (typeLawDisplacement) {
		case TypeLaw.Linearly : return (a) => { return 1 - a / totalTime; } ;
		case TypeLaw.ParabolicAcceleration : return (a) => { return Mathf.Pow (1 - a / totalTime, 2); } ;
		case TypeLaw.ParabolicBraking : return (a) => { return 1 - Mathf.Pow (a / totalTime, 2); } ;
		case TypeLaw.HyperbolicAcceleration : return (a) => { return Mathf.Pow (1 - a / totalTime, 3); } ;
		case TypeLaw.HyperbolicBraking : return (a) => { return 1 - Mathf.Pow (a / totalTime, 3); } ;
		case TypeLaw.Sinusoidal : return (a) => { return 0.5f * (1 + Mathf.Sin ( Mathf.PI * ((a / totalTime) + 0.5f))); } ;	
		default : return null;
		}
	}
	
	private void Finished ()
	{
		IsMoving = false;
		this.transform.position = positionFinish;
		if (OnResult != null)
			OnResult (this);
	}
	
	private void Iteration (float counterTime)
	{
		this.transform.position = Vector3.Lerp (positionStart, positionFinish, processing (counterTime));
	}
}
