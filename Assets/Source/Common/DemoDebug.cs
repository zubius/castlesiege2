using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class DemoDebug
{
	#region 2xArray

	public static string DebugArray (string[][] resultArray)
	{
		string result = "";
		for (int j = 0; j < resultArray.Length; j++) {
			for (int i = 0; i < resultArray[j].Length; i++)
				result += resultArray[j][i] + ", ";
			result += "\n";
		}
		return result;
	}

	public static string DebugArray (int[][] resultArray)
	{
		string result = "";
		for (int j = 0; j < resultArray.Length; j++) {
			for (int i = 0; i < resultArray[j].Length; i++)
				result += resultArray[j][i].ToString () + ", ";
			result += "\n";
		}
		return result;
	}

	public static string DebugArray (float[][] resultArray)
	{
		string result = "";
		for (int j = 0; j < resultArray.Length; j++) {
			for (int i = 0; i < resultArray[j].Length; i++)
				result += resultArray[j][i].ToString () + ", ";
			result += "\n";
		}
		return result;
	}

	#endregion

	#region 1xArray

	public static string DebugArray (string[] resultArray, string separator = ", ")
	{
		string result = "";
		for (int i = 0; i < resultArray.Length; i++)
			result += resultArray[i] + separator;
		return result + "; ";
	}

	public static string DebugArray (int[] resultArray)
	{
		string result = "";
		for (int i = 0; i < resultArray.Length; i++)
			result += resultArray[i].ToString () + ", ";
		return result + "; ";
	}

	public static string DebugArray (float[] resultArray)
	{
		string result = "";
		for (int i = 0; i < resultArray.Length; i++)
			result += resultArray[i].ToString ()+ ", ";
		return result + "; ";
	}

	public static string DebugArray (double[] resultArray)
	{
		string result = "";
		for (int i = 0; i < resultArray.Length; i++)
			result += resultArray[i].ToString ()+ ", ";
		return result + "; ";
	}

	public static string DebugArray (object[] resultArray)
	{
		string result = "";
		for (int i = 0; i < resultArray.Length; i++)
			result += resultArray[i].ToString ()+ ", ";
		return result + "; ";
	}

	#endregion
	
	public static string ToStringEventGUI (LogicEventArgs args) 
	{
		return args.EventName.ToUpper () + ": " + ToStringEventGUI (args.Args);
	}
	
	public static string ToStringEventGUI (object[] args)
	{
		string str = " args.Length: " + args.Length + "\n";

		foreach (object p in args) {
			str += "\t" + GetData (p) + "\n";
		}

		return str;
	}
	
	public static string ToStringEvent (LogicEventArgs args)
	{
		return "=================== OnGlobalEvent: \t" + args.EventName + " \t" + ToStringEvent (args.Args);
	}

	public static string ToStringEvent (object[] args)
	{
		string str = " args.Length: " + args.Length + " ===================\n";

		foreach (object p in args) {
			str += GetData (p) + "\n";
		}

		return str;
	}

	private static string GetData (object obj)
	{
		if (obj == null) return "Object is null";

		if (obj.GetType () == typeof (int) || obj.GetType () == typeof (bool) || obj.GetType () == typeof (float) || obj.GetType () == typeof (string) || obj.GetType () == typeof (double)) return System.Convert.ToString (obj);
//		if (obj.GetType () == typeof (LitJson.JsonData)) return ((LitJson.JsonData) obj).ToJson ();
//		if (obj.GetType () == typeof (int [])) return obj.GetType ().Name + ": " + DebugArray ((int[]) obj);
//		if (obj.GetType () == typeof (LineInfo)) return obj.GetType ().Name + ": " + ((LineInfo) obj).ToString ();

		return obj.GetType ().Name;
	}
}
