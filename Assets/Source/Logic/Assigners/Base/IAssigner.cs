using UnityEngine;
using System.Collections;

public interface IAssigner
{
    void Do (params object[] args);
}
