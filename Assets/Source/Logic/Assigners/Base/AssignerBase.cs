using System;
using System.Collections;
using System.Reflection;
using ru.appforge.logic;
using UnityEngine;
using _ = ru.appforge.utils.U;

public class AssignerBase : MonoBehaviour, IAssigner
{
    /// <summary>
    /// Содержит массив всех объектов унаследованных от IGameComponent
    /// </summary>
    protected IGameComponent[] GameComponents { get; set; }

	protected ILogicControllerAdvanced Controller { get; set; }

    /// <summary>
    /// Внимание !!! В наследованном классе не должно быть метода Start используйте void override _Start() { base._Start(); }
    /// </summary>
    void Awake ()
    {
        _Awake ();
    }

    public virtual void _Awake ()
    {
        //this.GameComponents = this.GetComponents (typeof(IGameComponent)) as IGameComponent;
        Component[] c = this.GetComponents (typeof (IGameComponent));

        GameComponents = new IGameComponent[c.Length];
        for (int i = 0; i < GameComponents.Length; i++)
            GameComponents[i] = c[i] as IGameComponent;
    }

    /// <summary>
    /// Внимание !!! В наследованном классе не должно быть метода Start используйте void override _Start() { base._Start(); }
    /// </summary>
    void Start ()
    {
		GameMainLogic logic = FindObjectOfType (typeof (GameMainLogic)) as GameMainLogic;
		this.Controller = logic.Controller;

        this.Controller.OnGlobalEvent += HandleOnGlobalEvent;
        foreach (IGameComponent g in GameComponents)
            g.OnResult += HandleOnResult;

        _Start ();
    }

	void OnDestroy ()
	{
		this.Controller.OnGlobalEvent -= HandleOnGlobalEvent;
		foreach (IGameComponent g in GameComponents)
			g.OnResult -= HandleOnResult;
	}

    public virtual void _Start ()
    {

    }

    /// <summary>
    /// Метод, привязанный к глобальному событию OnGlobalEvent. При перегрузке вызов базового класса не нужен
    /// </summary>
    protected virtual void HandleOnGlobalEvent (object sender, LogicEventArgs args)
    {

    }

    /// <summary>
    /// Метод, привязанный к cобытию OnResult, возвращаемому из GameComponents. При перегрузке вызов базового класса не нужен
    /// </summary>
    protected virtual void HandleOnResult (object sender, LogicEventArgs args)
    {

    }

	protected void SendEvent (LogicEventArgs args)
	{
		this.Controller.DoGlobalEvent (this, new LogicEventArgs ("_" + args.EventName, args.Args));
	}

	protected void SendActiveLogic (params object[] args) 
	{
		if (Controller.Active != null)
			this.Controller.Active.Do (args);
	}

	protected void DoGlobalEvent (object sender, LogicEventArgs args) 
	{
		this.Controller.DoGlobalEvent (sender, args);
	}

    #region IAssigner implementation

    /// <summary>
    /// Выполняет вызов функции
    /// </summary>
    private void call (IGameComponent gc, MethodInfo mi, object[] input)
    {
        ParameterInfo[] parameters = mi.GetParameters ();
        bool hasParams = false;
        if (parameters.Length > 0)
            hasParams = parameters[parameters.Length - 1].GetCustomAttributes (typeof (ParamArrayAttribute), false).Length > 0;

        if (hasParams) {
            int lastParamPosition = parameters.Length - 1;

            object[] realParams = new object[parameters.Length];
            for (int i = 0; i < lastParamPosition; i++)
                realParams[i] = input[i];

            Type paramsType = parameters[lastParamPosition].ParameterType.GetElementType ();
            Array extra = Array.CreateInstance (paramsType, input.Length - lastParamPosition);
            for (int i = 0; i < extra.Length; i++)
                extra.SetValue (input[i + lastParamPosition], i);

            realParams[lastParamPosition] = extra;

            input = realParams;
        }

		try {
			 	//Debug.LogWarning (string.Format ("Error call method:{2}.{0}({1}) from module  {3}; ", mi.Name, _.ToString (_.Map<object> (input, (item) => { return string.Format ("{0} ({1}), ", item.ToString (), item.GetType ().Name); })), gc.GetType ().Name, this.GetType ().Name));
            mi.Invoke (gc, input);
        } catch (System.MissingMethodException exc) {
            throw new System.Exception (exc.Message + string.Format ("Error call method:{2}.{0}({1}) from module  {3}; ", mi.Name,
                _.ToString (_.Map<object> (input, (item) => { return string.Format ("{0} ({1}), ", item.ToString (), item.GetType ().Name); })), gc.GetType ().Name, this.GetType ().Name));
        }
    }

    /// <summary>
    /// применять только для методов использующим params
    /// </summary>
    public void Do (params object[] args)
    {
        string name = (string) args[0];
        string script = "";

        GetNameScript (ref name, ref script);

        foreach (IGameComponent GameComponent in GameComponents) {
            if ((script != "") && (script != GameComponent.GetType ().Name))
                continue;

            foreach (MethodInfo mi in GameComponent.GetType ().GetMethods ()) {
                if (mi.Name == name) {
                    call (GameComponent, mi, _.RemoveFrom (args, 0, 1));
                }
            }
        }
    }

    /// <summary>
    /// Применять только для методов использующих фиксированное количество аргументов
    /// </summary>
    public void DoMethod (params object[] args)
    {
        string name = (string) args[0];
        string script = "";

        GetNameScript (ref name, ref script);

        foreach (IGameComponent GameComponent in GameComponents) {
            if ((script != "") && (script != GameComponent.GetType ().Name))
                continue;

            foreach (MethodInfo mi in GameComponent.GetType ().GetMethods ()) {
                if ((mi.Name == name) && (mi.GetParameters ().Length == args.Length - 1)) {
                    call (GameComponent, mi, _.RemoveFrom (args, 0, 1));
                }
            }
        }
    }

    /// <summary>
    /// Получает имя скрипта, в котором нужно вызвать метод
    /// </summary>
    private void GetNameScript (ref string name, ref string script)
    {
        if (name.IndexOf (".") > -1) {
            string[] _arg = name.Split ('.');
            name = _arg[1];
            script = _arg[0];
        }
    }
    #endregion
}
