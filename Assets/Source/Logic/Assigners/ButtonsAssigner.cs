using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class ButtonsAssigner : AssignerBase, IAssigner
{
	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
		switch (args.EventName) {
		case "on_btn_enable"		: Enable (true, args); break;
		case "on_btn_disable"		: Enable (false, args); break;

		case "on_btn_enable_only"	: EnableOnly (true, args); break;
		case "on_btn_disable_only"	: EnableOnly (false, args); break;
			
		case "on_set_mode_view"		: DoMethod ("ChangeMode", args.Args[0]); break;

		case "on_ua_mode_show"		: DoMethod ("UaMode", true); break; 
		case "on_ua_mode_hide"		: DoMethod ("UaMode", false); break; 

		case "on_set_logo"			: DoMethod ("SetLogo", args.Args[0]); break;
		}
	}

	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
		switch (args.EventName) {
		case "on_button_click_disable"		: this.Controller.DoGlobalEvent (this, new LogicEventArgs ("on_sound_play", "Button disable click")); break;
		case "on_button_click_not_active"	: SendActiveLogic ("btnd_" + args.Args[0], args.Args[1]); break;
		case "on_button_click"				: SendActiveLogic ("btn_" + args.Args[0], args.Args[1]); break;		
		}
	}

	protected void Enable (bool enable, LogicEventArgs args)
	{
		Enable (enable, args.Args.Length > 0 ? args.Args as string[] : new string[] { });
	}

	protected void Enable (bool enable, string[] names)
	{
		Do ("EnableButton", names.Length == 0 ^ enable, names);
	}

	protected void EnableOnly (bool enable, LogicEventArgs args)
	{
		if (args.Args.Length > 0)
			Do ("EnableSelect", enable, args.Args as string[]);
	}
}
