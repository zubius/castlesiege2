﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class LeaderBoardAssigner : AssignerBase, IAssigner
{
	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "on_save_score"			: Do("SaveScore", args.Args[0]); break;
		case "on_get_tops"				: Do("GetTop"); break;
		}
	}
	
	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "EventLeaderBoardCreated" : SendActiveLogic("end_created_leaderboard", args.Args); break;
		}
	}
}
