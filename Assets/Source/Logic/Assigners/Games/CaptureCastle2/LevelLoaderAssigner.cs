﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class LevelLoaderAssigner : AssignerBase, IAssigner
{
	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "on_load_level"			: Do("LoadLevel", args.Args[0], args.Args[1]); break;
		case "on_message"				: Do("ShowMessage", args.Args[0], args.Args[1], args.Args[2], args.Args[3], args.Args[4]); break;
		case "on_message_win"			: Do("ShowMessageWin", args.Args[0], args.Args[1], args.Args[2], args.Args[3], args.Args[4]); break;
		case "on_score_update"			: Do("UpdateScore", args.Args[0]); break;
		case "on_check_end_level"		: Do("CheckEndLevel", args.Args[0]); break;
		case "on_show_achie_msg"		: Do("ShowAchiev", args.Args[0], args.Args[1]); break;
		}
	}
	
	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "EventLevelCreated" : SendActiveLogic("end_level_created", args.Args); break;
		case "EventBuildingCaptured" : SendActiveLogic("end_building_captured", args.Args); break;
		case "EventLevelLoaderCreated" : SendActiveLogic("end_levelloader_created", args.Args); break;
		case "EventBuildingUpgraded" : SendActiveLogic("end_building_upgraded", args.Args); break;
		case "EventEndLevel" : SendActiveLogic("end_level", args.Args); break;
		case "EventAchieEarned" : SendActiveLogic("end_achie_earned", args.Args); break;
		}
	}
}
