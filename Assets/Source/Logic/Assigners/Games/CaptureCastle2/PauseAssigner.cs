﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class PauseAssigner : AssignerBase, IAssigner {

	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
		switch (args.EventName) {
		case "on_pause"			: Do("Pause", args.Args[0]); break;
		}
	}
}
