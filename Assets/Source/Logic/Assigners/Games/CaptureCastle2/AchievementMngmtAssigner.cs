﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class AchievementMngmtAssigner : AssignerBase, IAssigner {
	
	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
//				Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "on_page_back"			: Do("PrevPage", args.Args[0]); break;
		case "on_page_forwrd"		: Do("NextPage", args.Args[0]); break;
		}
	}
	
	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
		//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "EventPages" 	: SendActiveLogic("end_pages", args.Args); break;
		}
	}
}
