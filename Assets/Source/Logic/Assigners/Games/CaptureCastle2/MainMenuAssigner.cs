﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class MainMenuAssigner : AssignerBase, IAssigner {

	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "on_show_shop"			: Do("ShowShopWindow", args.Args[0]); break;
		case "on_fade"				: Do("Fade", args.Args[0]); break;
		}
	}
	
	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
//		case "EventMenuCreated" : SendActiveLogic("end_menu_created", args.Args); break;
//		case "EventChatBack" 	: SendActiveLogic("end_chat_back", args.Args); break;
		}
	}
}
