﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class AchievmentCntrlAssigner : AssignerBase, IAssigner
{
	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
		//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "on_achvmnt_earn"			: Do("EarnAchievment", args.Args[0]); break;
		}
	}
	
	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "EventAchieEarned" : SendActiveLogic("end_show_achie_earned", args.Args); break;
		}
	}
}
