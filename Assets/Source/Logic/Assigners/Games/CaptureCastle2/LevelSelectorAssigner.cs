﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class LevelSelectorAssigner : AssignerBase, IAssigner {

	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "on_levels_unlock"			: Do("SetUnlockLevels", args.Args[0], args.Args[1]); break;
		case "on_show_name_not_set"		: Do("SetNotice"); break;
		case "on_page_back"				: Do("PrevPage", args.Args[0]); break;
		case "on_page_forwrd"			: Do("NextPage", args.Args[0]); break;
		}
	}

	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "EventLevelSelectorInit" 	: SendActiveLogic("end_levelselector_init", args.Args); break;
		}
	}
}
