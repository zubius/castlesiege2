﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class DifficultSelectorssigner : AssignerBase, IAssigner
{
	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
		//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "on_diff_select"			: Do("Show", args.Args[0]); break;
		}
	}
	
	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
		//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "EventDiffSelected" : SendActiveLogic("end_diff_selected", args.Args); break;
		}
	}
}

