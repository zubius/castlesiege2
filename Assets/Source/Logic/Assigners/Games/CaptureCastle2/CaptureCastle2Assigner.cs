﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class CaptureCastle2Assigner  : AssignerBase, IAssigner
{

	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{
		switch (args.EventName) {
		case "on_create_main_menu"			: Do ("LoadMenu"); break;
		case "on_create_level_selector"		: Do ("LoadLevelSelector"); break;
		case "on_create_level_loader"		: Do ("LoadLevel"); break;
		case "on_create_leaderboard"		: Do ("LoadLeaderBoard"); break;
		case "on_create_achievs"			: Do ("LoadAhievs"); break;
		case "on_create_help"				: Do ("LoadHelp"); break;
		case "on_create_credits"			: Do ("LoadCredits"); break;
		}
	}

	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
//		Debug.LogError (args.EventName);
		switch (args.EventName) {
		case "EventLoadedMainMenu" 		: SendActiveLogic("end_created_mainmenu", args.Args); break;
		case "EventLoadedLevelSelector" : SendActiveLogic("end_created_levelselector", args.Args); break;
		case "EventLoadedLevel" 		: SendActiveLogic("end_created_level_loader", args.Args); break;
		case "EventLoadedLeaderBoard" 	: SendActiveLogic("end_created_leaderboard", args.Args); break;
		case "EventLoadedAchievs" 		: SendActiveLogic("end_created_achievements", args.Args); break;
		case "EventLoadedHelp" 			: SendActiveLogic("end_created_help", args.Args); break;
		case "EventLoadedCredits" 		: SendActiveLogic("end_created_credits", args.Args); break;
		}
	}
}
