using UnityEngine;
using System.Collections;
using ru.appforge.logic;
using System.Collections.Generic;

public class GameMainLogic : MonoBehaviour
{
	private ILogicControllerAdvanced controller;
	public ILogicControllerAdvanced Controller {
		get { return controller; }
		protected set { controller = value; } }
	
	public string gameProducer = "";
	public string state = "";

//	public GamesData.GameType typeGame = GamesData.GameType.Reels;

	public string nameGame { get; set; }
	public int freeGamesCount { get; set; }

	private bool init = false;

	private int countDebugGUI = 10;
	private List <string> debugGUI = new List<string> ();

	void Awake ()
	{
		Controller = CreateController ();
	}

	ILogicControllerAdvanced CreateController ()
	{
		MainSettings settings = this.GetComponent<MainSettings> ();
		freeGamesCount = settings.countFreeGame;
//		if (gameProducer == "") {
//			GamesData gd = new GamesData ();
//			gameProducer = gd.mainList.FindGame (MainSettings.nameGameSpecialWM).producer;
//			nameGame = MainSettings.nameGameSpecialWM;
//			return InitGameController (gameProducer, MainSettings.nameGameSpecialWM);
//		}
//		else {
			nameGame = settings.nameGame;
			return InitGameController (gameProducer, nameGame);
//		}
	}

	ILogicControllerAdvanced InitGameController(string gameProducer, string _nameGame)
	{
		switch (gameProducer) {
		case "appforge_cc2":	return this.Controller = new ru.appforge.capturecastle.CaptureCastle2Controller (this, _nameGame);
		}

		throw new System.Exception ("Unable to create logic. Unknown type of game: " + gameProducer);
	}

	// Use this for initialization
	void Start ()
	{
		Controller.OnGlobalEvent += OnGlobalEventDebug;
	}

	void OnGUI ()
	{
		// foreach (string item in debugGUI) GUILayout.Label (item);
	}

//	void OnDestroy() 
//	{
//		UnityEditor.EditorUtility.UnloadUnusedAssetsIgnoreManagedReferences ();
//	}

	// Update is called once per frame
	void Update ()
	{
		if ((!init) && ( Controller != null)){
			init = !init;
			((ILogicControllerAdvanced) Controller).Init ();
		}

		((ILogicControllerAdvanced) Controller).OnUpdate (Time.deltaTime);

		this.state = this.Controller.Active.GetType ().Name;

	}

	private void OnGlobalEventDebug (object sender, LogicEventArgs args)
	{
		// --- Filter ----------------------
//		switch (args.EventName) {
//		case "on_debug"				:
//		case "on_statusbar_changed" :
//		case "on_credit_changed"	:
//
//		case "on_sound_play"		:
//		case "on_kf_changed"		:
//		case "on_btn_enable"		:
//		case "on_btn_enable_only"	:
//		case "on_game_activate"		:
//		case "_on_show_line"		:
//		case "_on_set_jackpotdata"	:
//		case "on_btn_disable"		:
//		case "on_btn_disable_only"	:
//		case "on_json_request"		: return;
//		}

		Debug.LogWarning (DemoDebug.ToStringEvent (args));

		if (args.EventName.Length > 0 && args.EventName[0] != '_') {
			debugGUI.Add (DemoDebug.ToStringEventGUI (args));
			while (debugGUI.Count > countDebugGUI) debugGUI.RemoveAt (0);
		}
	}
}