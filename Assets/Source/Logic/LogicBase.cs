﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace ru.appforge.logic
{
    public abstract class LogicBase : ILogic
    {
		public virtual bool Active { get; set; } 

		public virtual bool Busy { get; set; }

        public ILogicController Parent { get; set; }

        public virtual void OnEnter (params object[] args)
        {

        }

        public virtual void Do (params object[] args)
        {
            if ((args.Length > 0) && ((string) args[0] == "method")) {
                foreach (MethodInfo mi in this.GetType ().GetMethods ()) {
                    if (mi.Name == (string) args[1]) {
                        mi.Invoke (this, args);
                        return;
                    }
                }
            }
        }

        public virtual void OnExit (params object[] args)
        {

        }
    }
}
