﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ru.appforge.capturecastle
{
	public class CaptureCastle2Controller : logic.LogicControllerAdvancedBase, logic.ILogicControllerAdvanced
	{
		public GameMainLogic Initializer { get; protected set; }
		public string nameGame = "";
		public int b2lCntr = 0;
		public string userName = string.Empty;
		public int stars;

		private Dictionary<int, int[]> i = new Dictionary<int, int[]>();

		public string UserName {
			get {
				return PlayerPrefs.GetString ("player_name", "");
			}
			set {
				PlayerPrefs.SetString ("player_name", value);
				PlayerPrefs.Save();
			}
		}

		public int LevelIdx {
			get {

				return PlayerPrefs.GetInt("level");
			}
			set {
				PlayerPrefs.SetInt("level", value);
				PlayerPrefs.Save();
			}
		}

		public int[] LevelsDifficult {
			get {
				string s = PlayerPrefs.GetString ("lvlsdif", "");
				if (s.Length == 0) {
					PlayerPrefs.SetString ("lvlsdif", "000000000000");
					PlayerPrefs.Save();
					return new int[] {0,0,0,0,0,0,0,0,0,0,0,0};
				}
				return s.Select(c => c - '0').ToArray();
			}
			set {
				System.Text.StringBuilder sb = new System.Text.StringBuilder ();
				for (int i = 0; i < value.Length; i++) {
					sb.Append (value[i].ToString ());
				}
				PlayerPrefs.SetString ("lvlsdif", sb.ToString ());
				PlayerPrefs.Save();
			}
		}
		
		public int SumScore {
			get {
				int s = 0;
				for (int i = 0; i < LevelIdx; i++)
					s += GetScore(i);
				return s;
			}
		}

		public int GetScore(int level) {
			return PlayerPrefs.GetInt("score_"+level.ToString());
		}

		public void SetScore(int level, int scores) {
			PlayerPrefs.SetInt("score_"+level.ToString(), scores);
			PlayerPrefs.Save();
		}

		public override void Do (params object[] args)
		{
			Debug.Log (base.Active.GetType ().Name);
			if (base.Active != null) base.Active.Do (args);
		}
		
		public override void OnUpdate (params object[] args)
		{
			if (base.Active != null) base.UnityActive.OnUpdate (args);

//			if (Input.GetKeyDown(KeyCode.Escape))
//				Application.Quit(); 
		}
		
		public override ru.appforge.logic.ILogic this[string name]
		{
			get
			{
				throw new System.NotImplementedException ();
			}
		}

		public int[] DiffLevelGet(int bundle) {
			string s = PlayerPrefs.GetString ("lvlsdif"+bundle, "");
			if (s.Length == 0) {
				PlayerPrefs.SetString ("lvlsdif"+bundle, "000000000000");
				PlayerPrefs.Save();
				return new int[] {0,0,0,0,0,0,0,0,0,0,0,0};
			}
			return s.Select(c => c - '0').ToArray();
		}

		public void DiffLevelSet(int bundle, int[] diffs) {
			System.Text.StringBuilder sb = new System.Text.StringBuilder ();
			for (int i = 0; i < diffs.Length; i++) {
				sb.Append (diffs[i].ToString ());
			}
			PlayerPrefs.SetString ("lvlsdif"+bundle, sb.ToString ());
			PlayerPrefs.Save();
		}
		
		public override void Init ()
		{
			Debug.LogError(nameGame);

//			PlayerPrefs.SetInt("l1help", 0);
//			PlayerPrefs.SetInt("l2help", 0);
//			PlayerPrefs.SetInt("l5help", 0);
//			PlayerPrefs.SetInt("l9help", 0);

//			LevelIdx = 0;
//			PlayerPrefs.SetString("lvlsdif", "");
//			LevelIdx = 12;
//			DiffLevelSet(0, new int[] {1,1,1,1,1,1,1,1,1,1,1,0} );
//			DiffLevelSet(1, new int[] {0,0,0,0,0,0,0,0,0,0,0,0} );
//			LevelIdx = 24;
//			for (int i = 0; i < 12; i++) SetScore(i, 0);
//			UserName = "";
//			foreach (var key in Achvmnts.Achvs.Keys) PlayerPrefs.SetString(Achvmnts.Achvs[key].Name, "");

//			Debug.Log(string.Join("", new List<int>(LevelsDifficult).ConvertAll(i => i.ToString()).ToArray()));

			#if UNITY_ANDROID
			GameObject g = GameObject.Find("AdMobAndroidManager");

			if (g)
				g.GetComponent<AdMobAndroidAdapter>().adMobPubliserId = "ca-app-pub-3936176332568699/5474063486";
			#endif

			int l = LevelIdx;
			LevelIdx = l == 0 ? 1 : l;

			base.SetActiveLogic ("init");
		}
		
		public CaptureCastle2Controller (GameMainLogic initializer, string nameGame)
		{
			this.Initializer = initializer;
			this.nameGame = nameGame;
			
			base.Add ("init", new Init ());

			base.Add ("mainmenu", new MainMenu ());
			base.Add ("levelselect", new LevelSelect ());
			base.Add ("achievements", new Achievements ());
			base.Add ("leaderboard", new Leaderboard ());
			base.Add ("help", new Help ());
			base.Add ("credits", new Credits ());
			base.Add ("sync", new Sync ());
			base.Add ("start", new Start ());
			base.Add ("game", new Game ());

		}
		
		public override void Refresh ()
		{
//			Debug.LogError("this.gsd.Bet " + this.gsd.Bet);
//			this.DoGlobalEvent (this, "on_bet_changed", this.gsd.Bet);
		}	
	}
}


public enum Owner {
	Player,
	Enemy,
	Neutral
}

