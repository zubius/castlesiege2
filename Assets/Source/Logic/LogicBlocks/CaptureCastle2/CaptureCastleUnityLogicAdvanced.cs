﻿using System;

namespace ru.appforge.capturecastle
{
	public class CaptureCastleUnityLogicAdvanced : logic.UnityLogicAdvanced, logic.IUnityLogic
	{
		public CaptureCastle2Controller Controller
		{
			get
			{
				return ((CaptureCastle2Controller) base.ParentAdvanced);
			}
		}
	}
}