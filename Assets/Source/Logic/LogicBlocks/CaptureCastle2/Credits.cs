﻿using System;
using ru.appforge.logic;
using System.Collections;
using UnityEngine;

namespace ru.appforge.capturecastle
{
	public class Credits : CaptureCastleUnityLogicAdvanced, logic.IUnityLogic
	{
		public override void OnEnter (params object[] args)
		{
			base.ParentAdvanced.DoGlobalEvent(this, "on_create_credits");
			
		}
		
		public override void Btn (params object[] args)
		{
			UnityEngine.Debug.Log(args[0]);
			switch ((string)args[0]) {
			case "btn_BackBtn":
				base.ParentAdvanced.SetActiveLogic("init");
				break;
			}
		}
		
		public override void End (params object[] args)
		{
			switch ((string)args [0]) {
			case "end_created_credits" :
				//				base.ParentAdvanced.DoGlobalEvent(this, "on_get_tops");
				break;
			}
		}
		
		public override void OnUpdate (params object[] args)
		{
			if (Input.GetKeyDown(KeyCode.Escape))
				base.ParentAdvanced.SetActiveLogic("init");
		}
	}
}