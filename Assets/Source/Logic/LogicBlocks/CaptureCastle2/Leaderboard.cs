﻿using System;
using ru.appforge.logic;
using System.Collections;
using UnityEngine;

namespace ru.appforge.capturecastle
{
	public class Leaderboard : CaptureCastleUnityLogicAdvanced, logic.IUnityLogic
	{
		int upds = 10;
		int cntr = 0;
		bool done = false;

		public override void OnEnter (params object[] args)
		{
			base.ParentAdvanced.DoGlobalEvent(this, "on_create_leaderboard");
			if (AdsOffComponent.IsAdEnabled) {
#if UNITY_ANDROID
			AdMobAndroid.hideBanner(true);
#elif UNITY_IPHONE
			iOSInterstitialAdapter.bannerView.Hide();
#endif
			}
		}

		public override void Btn (params object[] args)
		{
			UnityEngine.Debug.Log(args[0]);
			switch ((string)args[0]) {
			case "btn_BackBtn":
				base.ParentAdvanced.SetActiveLogic("init");
				break;
			}
		}

		public override void End (params object[] args)
		{
			switch ((string)args [0]) {
			case "end_created_leaderboard" :
				base.ParentAdvanced.DoGlobalEvent(this, "on_get_tops");
				break;
			}
		}

		public override void OnUpdate (params object[] args)
		{
			if (Input.GetKeyDown(KeyCode.Escape))
				base.ParentAdvanced.SetActiveLogic("init");
		}

		public override void OnExit (params object[] args)
		{
			if (AdsOffComponent.IsAdEnabled) {
			#if UNITY_ANDROID
			AdMobAndroid.hideBanner(false);
			#elif UNITY_IPHONE
			iOSInterstitialAdapter.bannerView.Show();
			#endif
			}
		}
	}
}