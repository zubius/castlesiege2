﻿using System;
using ru.appforge.logic;
using UnityEngine;

namespace ru.appforge.capturecastle
{
	public class MainMenu : CaptureCastleUnityLogicAdvanced, logic.IUnityLogic
	{
		private bool shopWnd = false;

		public override void OnEnter (params object[] args)
		{
			shopWnd = false;
			base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
		}

		public override void Btn (params object[] args)
		{
			switch ((string)args[0]) {
			case "btn_StartBtn":
				if (shopWnd) return;
				base.ParentAdvanced.SetActiveLogic("levelselect");
				break;
			case "btn_TopScoresBtn":
				if (shopWnd) return;
				base.ParentAdvanced.SetActiveLogic("leaderboard");
				break;
			case "btn_AchieBtn":
				if (shopWnd) return;
				base.ParentAdvanced.SetActiveLogic("achievements");
				break;
			case "btn_HelpBtn":
				if (shopWnd) return;
				base.ParentAdvanced.SetActiveLogic("help");
				break;
			case "btn_CreditBtn":
				if (shopWnd) return;
				base.ParentAdvanced.SetActiveLogic("credits");
				break;
			case "btn_AdsOffBtn":
				if (!shopWnd) return;
				if (!AdsOffComponent.IsAdEnabled) return;
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
				base.ParentAdvanced.DoGlobalEvent(this, "on_buy_items",0);
				break;
			case "btn_ShopBtn":
				shopWnd = !shopWnd;
				base.ParentAdvanced.DoGlobalEvent(this, "on_show_shop", shopWnd);
				if (shopWnd) {
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable_only", new string[] {"AdsOffBtn", "RestoreBtn", "MainMenu"});
				} else {
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable");
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"AdsOffBtn", "RestoreBtn", "MainMenu"});
				}
				base.ParentAdvanced.DoGlobalEvent(this, "on_fade", 0.5f);
				break;
			case "btn_RestoreBtn":
				if (!shopWnd) return;
				base.ParentAdvanced.DoGlobalEvent(this, "on_restore_items");

//				if (!AdsOffComponent.IsAdEnabled) return;
//				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
//				base.ParentAdvanced.DoGlobalEvent(this, "on_buy_items",0);
				break;
			case "btn_MainMenu":
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable");
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"AdsOffBtn", "RestoreBtn", "MainMenu"});
				if (shopWnd) {
					shopWnd = !shopWnd;
					base.ParentAdvanced.DoGlobalEvent(this, "on_show_shop", shopWnd);
				}
				base.ParentAdvanced.DoGlobalEvent(this, "on_fade", 1f);
				break;
			}
		}

		public override void OnUpdate (params object[] args)
		{
			if (Input.GetKeyDown(KeyCode.Escape)) {
				Application.Quit(); 
				if (!shopWnd)
					Application.Quit();
				else {
					Btn("btn_MainMenu");
				}
			}
		}

		public override void End (params object[] args)
		{
			switch ((string) args[0]) {
			case "end_rdy":
				base.ParentAdvanced.DoGlobalEvent (this, "on_btn_enable");
				if (!AdsOffComponent.IsAdEnabled) base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"AdsOffBtn"});		
				break;
			case "end_purchased_items":
				Debug.LogError ("PURCHASED");
				#if UNITY_ANDROID
				AdMobAndroid.hideBanner(true);
				#elif UNITY_IPHONE
				iOSInterstitialAdapter.bannerView.Hide();
				#endif
				base.ParentAdvanced.DoGlobalEvent (this, "on_btn_enable");
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"AdsOffBtn"});
				break;
			case "end_cancelled_items":
				Debug.LogError("PURCHASED CANCELLED!");
				base.ParentAdvanced.DoGlobalEvent (this, "on_btn_enable");		
				break;
			case "end_failed_items":
				Debug.LogError("PURCHASE FAILED!");
				base.ParentAdvanced.DoGlobalEvent (this, "on_btn_enable");
				break;
			case "end_restored_items":
				Debug.LogError("TRANSACTIONS RESTORED.");
//				base.ParentAdvanced.DoGlobalEvent (this, "on_btn_enable");
				Btn("btn_MainMenu");
				break;
			}
		}
	}
}