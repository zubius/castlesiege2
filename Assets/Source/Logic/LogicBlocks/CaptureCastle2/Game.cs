﻿using System;
using ru.appforge.logic;
using System.Linq;
using UnityEngine;

namespace ru.appforge.capturecastle
{
	public class Game : CaptureCastleUnityLogicAdvanced, logic.IUnityLogic
	{
		private int palyerBuildings;
		private int enemyBuildings;
		private int palyerTroops;
		private int enemyTroops;
		private int level;
		private int difficult;
		private bool isPause;
		private bool isLevelEnded = false;

		private bool isHgls;
		private float hglsTimer = 180f;
		private int neutralBuildings = 0;

		private int levelScore;

		private int levelIdx;
		private int lidxMod = 0;
		private bool interstitialPressed = false;
		private bool interstitialShowing = false;
		private bool endLevel = false;
		private bool isPlayerWin = false;
		private float endLevelTimer = 1f;

		private int[] difficultyLevel;
		private int bundle;

		bool failedReceiveInterstitial = false;
		bool rdyInterstitial = false;


		public override void OnEnter (params object[] args)
		{
			levelScore = 0;
			endLevelTimer = 1f;
			isPause = false;
			isHgls = false;
			isLevelEnded = false;
			levelIdx = (int)args[0];
			difficult = (int)args[1];
			interstitialPressed = false;
			interstitialShowing = false;

			bundle = (int)args[2];

			lidxMod = 12 * bundle;

			difficultyLevel = Controller.DiffLevelGet(bundle);

			base.ParentAdvanced.DoGlobalEvent(this, "on_create_level_loader");
		}

		public override void Btn (params object[] args)
		{
//			UnityEngine.Debug.Log((string)args[0]);
			switch ((string)args[0]) {
			case "btn_PauseBtn":
				if (isLevelEnded) break;
				isPause = !isPause;
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable");
				base.ParentAdvanced.DoGlobalEvent(this, "on_pause", isPause);
				base.ParentAdvanced.DoGlobalEvent(this, "on_message", isPause, "Game paused", "", 0, 0);
				break;
			case "btn_Restart":
				base.ParentAdvanced.SetActiveLogic("game", levelIdx, difficult, bundle);
				break;
			case "btn_Resume":
				isPause = false;
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable_only", new string[] {"PauseBtn"});
				base.ParentAdvanced.DoGlobalEvent(this, "on_pause", isPause);
				base.ParentAdvanced.DoGlobalEvent(this, "on_message", isPause, "", "", 0, 0);
				break;
			case "btn_Back2ListW":
			case "btn_Back2List":
//				Controller.b2lCntr++;
//				if (Controller.b2lCntr == 3) {
//					Controller.b2lCntr = 0;
#if UNITY_EDITOR
					base.ParentAdvanced.SetActiveLogic("levelselect", bundle);
#else
					ShowInterstitial();
#endif
//				} else {
//					base.ParentAdvanced.SetActiveLogic("levelselect", bundle);
//				}

				break;
			case "btn_Back2MenuW":
			case "btn_Back2Menu":
				base.ParentAdvanced.SetActiveLogic("init");
				break;
			case "btn_Next":
				int nextdiff = difficult <= difficultyLevel[levelIdx+1-lidxMod] ? difficult : difficultyLevel[levelIdx+1-lidxMod];
				if (nextdiff == 0) nextdiff = 1;
				if (levelIdx < 23)
					base.ParentAdvanced.SetActiveLogic("game", levelIdx+1, nextdiff, levelIdx < 11 ? 0 : 1);
				break;
			}
		}
#if UNITY_IPHONE
		protected void CallInterstitialAndroid ()
		{
			if (!AdsOffComponent.IsAdEnabled) return;
			Debug.Log("Call IOS");
			base.ParentAdvanced.DoGlobalEvent(this, "on_load_interstitial");
		}
		
		protected void ShowInterstitial() {
			if (!AdsOffComponent.IsAdEnabled) {
				base.ParentAdvanced.SetActiveLogic("levelselect", bundle);
				return;
			}
			Debug.Log("Show IOS");
			base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
			interstitialShowing = true;
			base.ParentAdvanced.DoGlobalEvent(this, "on_show_interstitial");
		}
#endif
#if UNITY_ANDROID

		private void CallInterstitialAndroid() {
			if (!AdsOffComponent.IsAdEnabled) return;
			if (!interstitialPressed)
				interstitialPressed = true;
//			base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
			AdMobAndroid.requestInterstital("ca-app-pub-3936176332568699/6950796683");
			AdMobAndroidManager.interstitialReceivedAdEvent += HandleinterstitialReceivedAdEvent;
			AdMobAndroidManager.interstitialDismissingScreenEvent += HandleinterstitialDismissingScreenEvent;
			AdMobAndroidManager.interstitialFailedToReceiveAdEvent += HandleinterstitialFailedToReceiveAdEvent;
		}
		
		void HandleinterstitialFailedToReceiveAdEvent (string obj)
		{
			failedReceiveInterstitial = true;
//			base.ParentAdvanced.SetActiveLogic("levelselect");
		}

		void HandleinterstitialDismissingScreenEvent ()
		{
			interstitialShowing = false;
			base.ParentAdvanced.SetActiveLogic("levelselect", bundle);
		}

		void HandleinterstitialReceivedAdEvent ()
		{
			rdyInterstitial = true;
//			if (AdMobAndroid.isInterstitalReady()) {
//				AdMobAndroid.displayInterstital();
//				interstitialShowing = true;
//			}
		}

		protected void ShowInterstitial() {
			if (failedReceiveInterstitial) {
				base.ParentAdvanced.SetActiveLogic("levelselect", bundle);
			}
			if (!interstitialPressed)
				interstitialPressed = true;
			base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
			if (AdMobAndroid.isInterstitalReady() && !failedReceiveInterstitial && AdsOffComponent.IsAdEnabled && rdyInterstitial) {
				AdMobAndroid.displayInterstital();
				interstitialShowing = true;
			} else {
				interstitialShowing = false;
				base.ParentAdvanced.SetActiveLogic("levelselect", bundle);
			}
		}
#endif

		public override void End (params object[] args)
		{
//			UnityEngine.Debug.Log((string)args[0]);
			object[] paramsArgs = args[1] as object[];
			switch ((string)args[0]) {
			case "end_levelloader_created":
				base.ParentAdvanced.DoGlobalEvent(this, "on_load_level", levelIdx, difficult);
				base.ParentAdvanced.DoGlobalEvent(this, "on_score_update", levelScore);

				CallInterstitialAndroid();
				break;
			case "end_building_upgraded":
				UpdateLevelScores(10);
				break;
			case "end_level_created":
				level = (int)paramsArgs[0];
				palyerBuildings = (int)paramsArgs[1];
				enemyBuildings = (int)paramsArgs[2];
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable_only", new string[] {"PauseBtn"});

				if (!Achvmnts.Hrglss.Earned && levelIdx == 11) isHgls = true;
				break;
			case "end_building_captured":
				BuildingCapruted((string)paramsArgs[0], (string)paramsArgs[1], (bool)paramsArgs[2], (int)paramsArgs[3]);
				if (palyerBuildings == 0) {
					base.ParentAdvanced.DoGlobalEvent(this, "on_check_end_level", Owner.Player);
				} else if (enemyBuildings == 0) {
					base.ParentAdvanced.DoGlobalEvent(this, "on_check_end_level", Owner.Enemy);
				}
				break;
			case "end_level":
				if (palyerBuildings == 0) {
					endLevel = true;
					isPlayerWin = false;
				} else if (enemyBuildings == 0) {
					endLevel = true;
					isPlayerWin = true;
				}
				break;
			case "end_achie_earned":
				EarnAchie((string)paramsArgs[0]);
				break;
			case "end_show_achie_earned":
				Achiev a = (Achiev)paramsArgs[0];
				base.ParentAdvanced.DoGlobalEvent(this, "on_show_achie_msg", a.Name, a.Description);
				break;
			case "end_interstitial_not_loaded":
			case "end_interstitial_showed":
				interstitialShowing = false;
				base.ParentAdvanced.SetActiveLogic("levelselect", bundle);
				break;
			default:
				break;
			}
		}

		void EarnAchie (string achie)
		{
			base.ParentAdvanced.DoGlobalEvent(this, "on_achvmnt_earn", achie);
		}

//		void ShowHelp(string help) {
//			if (
//		}

		public override void OnUpdate (params object[] args)
		{
			if (endLevel && !isPause) {
				endLevelTimer -= UnityEngine.Time.deltaTime;
				if (endLevelTimer <= 0) {
					isLevelEnded = true;
					EndLevel(isPlayerWin);
					endLevel = false;
				}
			}

			if (!isPause && isHgls) {
				hglsTimer -= UnityEngine.Time.deltaTime;
				if (hglsTimer <= 0)
					isHgls = false;
			}

			if (Input.GetKeyDown(KeyCode.Escape) && !isLevelEnded && !interstitialShowing) {
				Debug.Log(endLevel);
				isPause = true;
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable");
				base.ParentAdvanced.DoGlobalEvent(this, "on_pause", isPause);
				base.ParentAdvanced.DoGlobalEvent(this, "on_message", isPause, "Game paused", "", 0, 0);
			}
		}

		private void EndLevel(bool res) {
			base.ParentAdvanced.DoGlobalEvent(this, "on_pause", true);
			base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"PauseBtn"});
			base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable");
			if (res) {

				int[] d = difficultyLevel;
				Debug.Log(ru.appforge.utils.U.ToString(d));

				if (d.Sum() < 12 && d[levelIdx-lidxMod] < difficult) {
					d[levelIdx-lidxMod]++;
				} else if (d.Sum() >= 12 && d.Sum() < 24 && d[levelIdx-lidxMod] < difficult) {
					d[levelIdx-lidxMod]++;
				} else if (d.Sum() >= 24 && d.Sum() < 36 && d[levelIdx-lidxMod] < difficult) {
					d[levelIdx-lidxMod]++;
				}
				Controller.DiffLevelSet(bundle, d);
				int ds = d.Sum();

				if (!Achvmnts.LnlyStar.Earned && ds == 12) EarnAchie(Achvmnts.LnlyStar.Name);
				if (!Achvmnts.SweetCpl.Earned && ds == 24) EarnAchie(Achvmnts.SweetCpl.Name);
				if (!Achvmnts.TeamAllStrs.Earned && ds == 36) EarnAchie(Achvmnts.TeamAllStrs.Name);

				if (isHgls) EarnAchie(Achvmnts.Hrglss.Name);

				if (!Achvmnts.NbdsPrpty.Earned && neutralBuildings > 0) EarnAchie(Achvmnts.NbdsPrpty.Name);

				if (Controller.LevelIdx == (this.levelIdx+1) && Controller.LevelIdx < 24) {
					Controller.LevelIdx++;
				}
				if (Controller.GetScore(this.levelIdx) < levelScore) {
					Controller.SetScore(this.levelIdx, levelScore);
					base.ParentAdvanced.DoGlobalEvent(this, "on_save_score", Controller.SumScore);
				}
				UnityEngine.Debug.Log("level " + levelScore);
				UnityEngine.Debug.Log("total " + Controller.SumScore);
				if (ds == 12 || ds == 24 || ds == 36) {
					string rank = GetRank(Controller.SumScore);
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"PauseBtn", "Resume", "Next", "Restart"});
					base.ParentAdvanced.DoGlobalEvent(this, "on_message_win", true, "Congratulations!", "win", Controller.SumScore, rank);
				} else {
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"PauseBtn", "Resume", "Back2Menu", "Restart"});
					base.ParentAdvanced.DoGlobalEvent(this, "on_message", true, "Level Complete!", "win", Controller.SumScore, levelScore);
				}
			} else {
				base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"PauseBtn", "Resume", "Back2Menu", "Next"});
				base.ParentAdvanced.DoGlobalEvent(this, "on_message", true, "YOU LOST!", "lose", Controller.SumScore, 0);
			}
		}

		void TroopsKilled(string owner, int amount) {}

		string GetRank (int sumScore)
		{
			string r = "Squire!";

			if (sumScore >= 4100 && sumScore < 4600)
				r = "Baron!";
			else if (sumScore >= 4600 && sumScore < 5100)
				r = "Viscount!";
			else if (sumScore >= 5100 && sumScore < 5700)
				r = "Count!";
			else if (sumScore >= 5700 && sumScore < 6100)
				r = "Marquis!";
			else if (sumScore >= 6100 && sumScore < 6600)
				r = "Duke!";
			else if (sumScore >= 6600 && sumScore < 7800)
				r = "Prince!";
			else if (sumScore >= 7800)
				r = "King!";

			return r;
		}

		void BuildingCapruted (string newowner, string oldowner, bool isAcademy, int neutral)
		{
			const string enemy = "Enemy";
			const string player = "Player";
			neutralBuildings = neutral;

			switch (newowner) {
			case player:
				palyerBuildings++;
				if (!isAcademy) UpdateLevelScores(100);
				break;
			case enemy:
				enemyBuildings++;
				break;
			}

			switch (oldowner) {
			case player:
				palyerBuildings--;
				if (!isAcademy) UpdateLevelScores(-110);
				break;
			case enemy:
				enemyBuildings--;
				break;
			}
		}

		private void UpdateLevelScores(int score) {
			levelScore += score * difficult;
			if (levelScore > 800 * difficult)
				levelScore = 800 * difficult;
			if (levelScore < 0) levelScore = 0;
			base.ParentAdvanced.DoGlobalEvent(this, "on_score_update", levelScore);
		}
	}
}