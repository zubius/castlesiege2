﻿using System;
using ru.appforge.logic;
using System.Collections;
using UnityEngine;

namespace ru.appforge.capturecastle
{
	public class Achievements : CaptureCastleUnityLogicAdvanced, logic.IUnityLogic
	{
		int pages;
		int currentPage = 1;

		public override void OnEnter (params object[] args)
		{
			base.ParentAdvanced.DoGlobalEvent(this, "on_create_achievs");
			if (AdsOffComponent.IsAdEnabled) {
#if UNITY_ANDROID
			AdMobAndroid.hideBanner(true);
#elif UNITY_IPHONE
			iOSInterstitialAdapter.bannerView.Hide();
#endif
			}
		}
		
		public override void Btn (params object[] args)
		{
//			UnityEngine.Debug.Log(args[0]);
			switch ((string)args[0]) {
			case "btn_BackBtn":
				if (currentPage == 1)
					base.ParentAdvanced.SetActiveLogic("init");
				else {
					currentPage--;
					base.ParentAdvanced.DoGlobalEvent(this, "on_page_back", currentPage);

					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable_only", new string[] {"FrwrdBtn"});
				}
				break;
			case "btn_FrwrdBtn":
				currentPage++;
				base.ParentAdvanced.DoGlobalEvent(this, "on_page_forwrd", currentPage);

				if (currentPage == pages)
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"FrwrdBtn"});
				break;
			}
		}
		
		public override void End (params object[] args)
		{
			object[] paramsArgs = args[1] as object[];
			switch ((string)args [0]) {
			case "end_pages" :
				pages = (int)paramsArgs[0];
				currentPage = 1;
				break;
			}
		}
		
		public override void OnUpdate (params object[] args)
		{
			if (Input.GetKeyDown(KeyCode.Escape)) {
				base.ParentAdvanced.SetActiveLogic("init");
//				Btn("btn_BackBtn");
			}
		}

		public override void OnExit (params object[] args)
		{
			if (AdsOffComponent.IsAdEnabled) {
			#if UNITY_ANDROID
			AdMobAndroid.hideBanner(false);
			#elif UNITY_IPHONE
			iOSInterstitialAdapter.bannerView.Show();
			#endif
			}
		}
	}
}