﻿using System;
using ru.appforge.logic;

namespace ru.appforge.capturecastle
{
	public class Init : CaptureCastleUnityLogicAdvanced, logic.IUnityLogic
	{
		public override void OnEnter (params object[] args)
		{
			base.ParentAdvanced.DoGlobalEvent(this, "on_create_main_menu");
		}

		public override void End (params object[] args)
		{
			switch ((string)args [0]) {
			case "end_created_mainmenu" :
				base.ParentAdvanced.SetActiveLogic("mainmenu");
				break;
			}
		}
	}
}

