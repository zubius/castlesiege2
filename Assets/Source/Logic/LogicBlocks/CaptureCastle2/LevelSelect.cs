﻿using System;
using ru.appforge.logic;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

namespace ru.appforge.capturecastle
{
	public class LevelSelect : CaptureCastleUnityLogicAdvanced, logic.IUnityLogic
	{
		int level;
		int pages = 2;
		int currentPage = 1;
		int bundle = -1;

		public override void OnEnter (params object[] args)
		{
			UnityEngine.Debug.LogError(Controller.LevelIdx);
			pages = 2;
			currentPage = 1;
			bundle = -1;
			if (args.Length == 1) {
				bundle = (int)args[0];
			}
//			Controller.LevelIdx = 13;
//			Controller.DiffLevelSet(0, new int[] {1,1,1,1,1,1,1,1,1,1,1,1} );
//			Controller.DiffLevelSet(1, new int[] {0,0,0,0,0,0,0,0,0,0,0,0} );
			base.ParentAdvanced.DoGlobalEvent(this, "on_create_level_selector");
		}

		public override void Btn (params object[] args)
		{
			UnityEngine.Debug.Log(args[0]);

			switch ((string)args[0]) {
			case "btn_BackBtn":
				if (currentPage == 1)
					base.ParentAdvanced.SetActiveLogic("init");
				else {
					currentPage--;
					base.ParentAdvanced.DoGlobalEvent(this, "on_page_back", currentPage);
					
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable_only", new string[] {"FwdBtn"});
				}
				break;
			case "btn_FwdBtn":
				currentPage++;
				base.ParentAdvanced.DoGlobalEvent(this, "on_page_forwrd", currentPage);
				
				if (currentPage == pages)
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable_only", new string[] {"FwdBtn"});
				break;
			default:
				string r = System.Text.RegularExpressions.Regex.Match((string)args[0], @"\d+").Value;
				level = int.Parse(r)-1;
				int s = Controller.DiffLevelGet(currentPage-1).Sum();
				if (s >= 12 && s < 24) {
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
					base.ParentAdvanced.DoGlobalEvent(this, "on_diff_select", 2);
				} else if (s >= 24) {
					base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
					base.ParentAdvanced.DoGlobalEvent(this, "on_diff_select", 3);
				} else
					base.ParentAdvanced.SetActiveLogic("game", level, 1, currentPage-1);
				break;
			}
		}

		public override void End (params object[] args)
		{
			object[] paramsArgs = args[1] as object[];
			switch ((string)args [0]) {
			case "end_levelselector_init" :
				InitSelector();
				if (bundle > 0)
					Btn("btn_FwdBtn");
				break;
			case "end_diff_selected" :
				base.ParentAdvanced.SetActiveLogic("game", level, (int)paramsArgs[0], currentPage-1);
				break;
			}
		}

		private void InitSelector() {
			base.ParentAdvanced.DoGlobalEvent(this, "on_btn_disable");
			string[] btnsToEnable = new string[Controller.LevelIdx];
			for (int i = 0; i < Controller.LevelIdx; i++) {
				btnsToEnable[i] = "Level"+(i+1);
			}
			List<int> ds = new List<int>(pages * 12);

			for (int i = 0; i < pages; i++) {
				int[] t = Controller.DiffLevelGet(i);
				for (int j = 0; j < t.Length; j++) {
					ds.Add(t[j]);
				}
			}

			base.ParentAdvanced.DoGlobalEvent(this, "on_levels_unlock", Controller.LevelIdx, ds.ToArray());
//			base.ParentAdvanced.DoGlobalEvent(this, "on_levels_unlock", Controller.LevelIdx, Controller.DiffLevelGet(0));
			base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable_only", btnsToEnable);
			base.ParentAdvanced.DoGlobalEvent(this, "on_btn_enable_only", new string[] {"BackBtn"});

			if (Controller.UserName.Length == 0)
				base.ParentAdvanced.DoGlobalEvent(this, "on_show_name_not_set");
		}

		public override void OnUpdate (params object[] args)
		{
			if (Input.GetKeyDown(KeyCode.Escape))
				base.ParentAdvanced.SetActiveLogic("init");
		}
	}
}