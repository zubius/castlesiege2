﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ru.appforge.logic
{
    public abstract class LogicControllerAdvancedBase : LogicControllerBase, ILogicControllerAdvanced
    {
        Dictionary<string, ILogic> logics = new Dictionary<string, ILogic> ();

        public virtual event DLogicEvent OnGlobalEvent;

        public virtual void DoGlobalEvent (object sender, LogicEventArgs args)
        {
            this.OnGlobalEvent (sender, args);
        }

        public virtual void DoGlobalEvent (object sender, string event_name)
        {
            this.DoGlobalEvent (sender, new LogicEventArgs (event_name));
        }

        public virtual void DoGlobalEvent (object sender, string event_name, params object[] args)
        {
            this.DoGlobalEvent (sender, new LogicEventArgs (event_name, args));
        }

        public void SetActiveLogic (string name, params object[] args)
        {
			UnityEngine.Debug.LogWarning(name);
			this.SetActiveLogic (name == "" ? null : this.logics[name], args);
        }

        public virtual ILogic this[string name]
        {
            get { return this.logics[name]; }
        }

        public void Remove (string name)
        {
            this.logics.Remove (name);
        }

        public void Add (string name, ILogic logic)
        {
            logic.Parent = this;
            this.logics.Add (name, logic);
        }

        public IUnityLogic UnityActive
        { get { return this.Active as IUnityLogic; } }

        public int Count
        {
            get { throw new NotImplementedException (); }
        }

        public IEnumerator GetEnumerator ()
        {
            yield return this.logics.GetEnumerator ();
        }

        public virtual void Do (params object[] args)
        {

		}

		#region ILogicControllerAdvanced implementation

		public virtual void Init ()
		{

		}

		public virtual void OnUpdate (params object[] args)
		{

		}

		public virtual void Refresh ()
		{

		}

		#endregion
	}
}
