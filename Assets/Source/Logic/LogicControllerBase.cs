﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ru.appforge.logic
{
    public abstract class LogicControllerBase : ILogicController
    {
        public virtual ILogic Active { get; set; }

        public void SetActiveLogic (ILogic logic, params object[] args)
        {
            if (this.Active != null) {
				this.Active.Active = false;
				this.Active.OnExit (args);
			}

            this.Active = logic;

            if (this.Active != null) {
				this.Active.Active = true;
				this.Active.OnEnter (args);
			}
        }
    }
}
