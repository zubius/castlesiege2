using System;
using System.Collections;
using System.Collections.Generic;

namespace ru.appforge.logic
{
    public class UnityLogicAdvanced : UnityLogicBase, IUnityLogic
    {
        #region Queue

        private Queue<object[]> DDDqueueDo;

        public Queue<object[]> QueueDo
        {
            get { return DDDqueueDo ?? (DDDqueueDo = new Queue<object[]> ()); }
            set { DDDqueueDo = value; }
        }

        public override bool Busy
        {
            get { return base.Busy; }
            set {
                if (base.Busy && base.Busy != value)
                    OnCleared ();
                base.Busy = value;
            }
        }

        private void OnCleared ()
        {
			QueueDo.Clear ();
		//	while (QueueDo.Count > 0)
		//		DoParsing (QueueDo.Dequeue ());
        }

        public override bool Active
        {
            get { return base.Active; }
            set
            {
                if (base.Active && base.Active != value) {
                    QueueDo.Clear ();
                    Busy = false;
                }
                base.Active = value;
            }
        }

        #endregion

        public LogicControllerAdvancedBase ParentAdvanced { get { return (LogicControllerAdvancedBase) this.Parent; } }

        public virtual void End (params object[] args)
        {
        }

        public virtual void Btn (params object[] args)
        {
        }

        public virtual void Mod (params object[] args)
        {
        }

        public virtual void Response (params object[] args)
        {
        }

		public override void Do (params object[] args)
		{            
			if (Active) {
				if (Busy)
					QueueDo.Enqueue (args);
				else
					DoParsing (args);
			}
		}

		void DoParsing (object[] args)
		{
//			UnityEngine.Debug.LogWarning (" ---------------------------------------------------------> \n" + DemoDebug.ToStringEvent(args));

//			UnityEngine.Debug.Log(ParentAdvanced.Active.GetType() );
			string[] cmd = args[0].ToString ().Split ('_');
			switch (cmd[0]) {
			case "btn": if (MainSettings.UkraineMode)  this.Btn (Chanage (args)); else this.Btn(args);  break;
			case "end": this.End (args);  break;
			case "response": this.Response (args); break;
			case "btnd": if (MainSettings.UkraineMode) this.Btn (ChanageD (args)); break;
			case "exception": ParentAdvanced.SetActiveLogic ("exceptionblock");break;
			default: this.Mod (args); break;
			}
		}

        object[] Chanage (object[] args)
        {
            // UnityEngine.Debug.LogError ((string) (args[0]) + " "+args.Length);
            if (args.Length > 1 && (args[1].GetType () == typeof (float)) && (float) args[1] > 1.0f) {
                switch ((string) (args[0])) {
                case "btn_help": return new object[] { "btn_mode" };
                case "btn_start": return new object[] { "btn_auto" };
                }
            }

            return args;
        }

        object[] ChanageD (object[] args)
        {
            // UnityEngine.Debug.LogError ((string) (args[0]) + " "+args.Length);
            if (args.Length > 1 && (args[1].GetType () == typeof (float)) && (float) args[1] > 1.0f) {
                switch ((string) (args[0])) {
                case "btnd_help": return new object[] { "btn_mode" };
                case "btnd_start": return new object[] { "btn_auto" };
                }
            }

            return args;
        }
    }
}

