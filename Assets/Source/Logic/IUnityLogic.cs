﻿using System;
using System.Collections.Generic;

using System.Text;

namespace ru.appforge.logic
{
	public interface IUnityLogic : ILogic
	{
		void OnUpdate (params object[] args);
	}
}
