using System;
using System.Collections.Generic;

namespace ru.appforge.utils
{
	public delegate T DArrayItemOut<T> (T item);

	public delegate void DArrayItem<T> (T item);

	public delegate object DReduceItem<T> (object memo,T item,int key);

	public delegate void DIterator (int index);

	/// <summary>
	/// Делегат совпадения объекта
	/// </summary>
	public delegate bool DFindItem<T> (T item);

	/// <summary>
	/// Делегат сравнения элемента A и Б
	/// </summary>
	public delegate int DCompareItem<T> (T a,T b);

	/// <summary>
	/// Делегат служит для того чтоб определять вес элемента
	/// </summary>
	public delegate float DItemPowerValue<T> (T a);

	public class U
	{

		/// <summary>
		/// Откинуть N первых элементов массива
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Rest<T> (ref T[] array, int count)
		{
			array = Rest (array, count);
		}

		public static void Swap<T> (ref T lhs, ref T rhs)
		{
			T temp;
			temp = lhs;
			lhs = rhs;
			rhs = temp;
		}

		public static T[] Rest<T> (T[] array, int count)
		{
			count = Math.Min (array.Length, count);

			T[] tmp = new T[array.Length - count];
			Array.Copy (array, count, tmp, 0, tmp.Length);
			return tmp;
		}

		/// <summary>
		/// Отбросить один элемент массива сначала
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Rest<T> (ref T[] array)
		{
			Rest (ref array, 1);
		}

		public static T[] Rest<T> (T[] array)
		{
			return Rest (array, 1);
		}

		/// <summary>
		/// Отбросить N последних элементов массива
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="count">Count.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Initial<T> (T[] array, int count)
		{
			count = Math.Min (array.Length, count);

			T[] tmp = new T[array.Length - count];
			Array.Copy (array, 0, tmp, 0, tmp.Length);
			return tmp;
		}

		public static void Initial<T> (ref T[] array, int count)
		{
			array = Initial (array, count);
		}

		public static T[] Initial<T> (T[] array)
		{
			return Initial (array, 1); 
		}

		public static void Initial<T> (ref T[] array)
		{
			array = Initial (array, 1);
		}



		/// <summary>
		/// Перевести любой массив в строковый
		/// </summary>
		/// <returns>The string array.</returns>
		/// <param name="array">Array.</param>
		public static string[] ToStringArray (Array array)
		{
			string[] res = new string[array.Length];
			for (int i=0; i<res.Length; i++)
				res [i] = array.GetValue (i).ToString ();
			return res;
		}

		public static void Out (Array array)
		{
			Console.WriteLine (ToString (array));
		}

		/// <summary>
		/// Показать массив в виде строки
		/// </summary>
		/// <returns>Строка</returns>
		/// <param name="array">Массив</param>
		public static string ToString (Array array)
		{
			string res = "";
			foreach (string item in ToStringArray(array))
				res += item + "\t";

			res += "(" + array.Length + ")";
			return res;
		}

		/// <summary>
		/// Map the specified array and mapDelegate.
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="mapDelegate">Map delegate.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Map<T> (T[] array, DArrayItemOut<T> mapDelegate)
		{
			T[] tmp = new T[array.Length];
			for (int i=0; i<tmp.Length; i++)
				tmp [i] = (T)mapDelegate.DynamicInvoke (array [i]);
			return tmp;
		}

		public static void Map<T> (ref T[] array, DArrayItemOut<T> mapDelegate)
		{
			array = Map (array, mapDelegate);
		}

		public static void Each<T> (T[] array, DArrayItem<T> eachDelegate)
		{
			for (int i=0; i<array.Length; i++)
				eachDelegate.Invoke (array [i]);
		}

		public static object Reduce<T> (T[] array, DReduceItem<T> reduceDelegate)
		{
			object last_memo = null;
			for (int i=0; i<array.Length; i++) {
				last_memo = reduceDelegate.Invoke (last_memo, array [i], i);
			}
			return last_memo;
		}

		public static T Find<T> (T[] array, DFindItem<T> findDelegate)
		{
			for (int i=0; i<array.Length; i++)
				if (findDelegate.Invoke (array [i]))
					return array [i];
			return default(T);
		}

		public static T[] Filter<T> (T[] array, DFindItem<T> findDelegate)
		{
			List<T> res = new List<T> ();

			for (int i=0; i<array.Length; i++) {
				if (findDelegate.Invoke (array [i]))
					res.Add (array [i]);
			}
			return res.ToArray ();
		}

		public static void Filter<T> (ref T[] array, DFindItem<T> findDelegate)
		{
			array = Filter (array, findDelegate);
		}

		/// <summary>
		/// Вернет копию массива array, из которой будут удалены все значения values. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="item">Item.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Without<T> (T[] array, T item)
		{
			return Filter (array, (i) => { 
				if (EqualityComparer<T>.Default.Equals (i, item))
					return false;
				else
					return true;
			});
		}

		/// <summary>
		/// Вернет копию массива array, из которой будут удалены все значения values. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="item">Item.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Without<T> (ref T[] array, T item)
		{
			array = Without (array, item);
		}

		/// <summary>
		/// Вернет копию массива array, из которой будут удалены все значения values. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="items">Items.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Without<T> (T[] array, T[] items)
		{
			return Filter (array, (i) => { 
				foreach (T item in items)
					if (EqualityComparer<T>.Default.Equals (i, item))
						return false;

				return true;
			});

		}

		/// <summary>
		/// Вернет копию массива array, из которой будут удалены все значения values. 
		/// </summary>
		/// <param name="array">Array.</param>
		/// <param name="items">Items.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Without<T> (ref T[] array, T[] items)
		{
			array = Without (array, items);
		}
	
		public static T[] Reject<T> (T[] array, DFindItem<T> findDelegate)
		{
			List<T> res = new List<T> ();

			for (int i=0; i<array.Length; i++) {
				if (!findDelegate.Invoke (array [i]))
					res.Add (array [i]);
			}
			return res.ToArray ();
		}

		public static void Reject<T> (ref T[] array, DFindItem<T> findDelegate)
		{
			array = Reject (array, findDelegate);
		}

		public static bool All<T> (T[] array, DFindItem<T> findDelegate)
		{
			for (int i=0; i<array.Length; i++) 
				if (!findDelegate.Invoke (array [i]))
					return false;
			return true;
		}

		public static bool Any<T> (T[] array, DFindItem<T> findDelegate)
		{
			for (int i=0; i<array.Length; i++) 
				if (findDelegate.Invoke (array [i]))
					return true;
			return false;
		}

		public static bool Include<T> (T[] array, T value)
		{
			for (int i=0; i<array.Length; i++) 
				if (EqualityComparer<T>.Default.Equals (array [i], value))
					return true;
			return false;
		}

		public static object[] Pluck<T> (T[] array, string propertyName)
		{
			object[] res = new object[array.Length];
			for (int i=0; i<array.Length; i++)
				res [i] = array [i].GetType ().GetProperty (propertyName).GetValue (array [i], null);
			return res;
		}

		public static T Max<T> (T[] array, DCompareItem<T> compareDelegate)
		{
			if (array.Length == 0)
				return default(T);

			T res = array [0];
			for (int i=1; i<array.Length; i++) 
				if (compareDelegate.Invoke (res, array [i]) > 0) 
					res = array [i];
				
			return res;
		}

		public static T Min<T> (T[] array, DCompareItem<T> compareDelegate)
		{
			if (array.Length == 0)
				return default(T);

			T res = array [0];
			for (int i=1; i<array.Length; i++) 
				if (compareDelegate.Invoke (res, array [i]) < 0) 
					res = array [i];

			return res;
		}

		public static int SortedIndex<T> (T[] array, T item, DCompareItem<T> compareDelegate)
		{
			throw new NotImplementedException ();
		}

		public static T[] Sort<T> (T[] array, DCompareItem<T> compareDelegate)
		{
			bool replace = true;
			
			T[] tmp = Copy<T> (array, 0, array.Length);
			
			while (replace) {
				replace = false;
				for (int i=0; i<tmp.Length-1; i++) {
					if (compareDelegate.Invoke (tmp [i], tmp [i + 1]) > 0) {
						Swap<T> (ref tmp [i], ref tmp [i + 1]);
						replace = true;
					}
				}
			}
			return tmp;
		}

		public static void Sort<T> (ref T[] array, DCompareItem<T> compareDelegate)
		{
			bool replace = true;
			while (replace) {
				replace = false;
				for (int i=0; i<array.Length-1; i++) {
					if (compareDelegate.Invoke (array [i], array [i + 1]) > 0) {
						T item = array [i];
						array [i] = array [i + 1];
						array [i + 1] = item;
						replace = true;
					}
				}
			}
		}

		public static T[] Shuffle<T> (T[] array)
		{
			Random random = new Random ();

			for (int i = 0; i < array.Length; i++) {
				int randomIndex = random.Next (0, array.Length);
				T item = array [i];
				array [i] = array [randomIndex];
				array [randomIndex] = item;
			}

			return array;
		}

		public static int IndexOf<T> (T[] array, T item)
		{
			return Array.IndexOf (array, item);
		}

		public static int LastIndexOf<T> (T[] array, T item)
		{
			return Array.LastIndexOf (array, item);
		}

		public static T First<T> (T[] array)
		{
			if (array.Length > 0)
				return array [0];
			return default(T);
		}

		public static T[] First<T> (T[] array, int count)
		{
			count = Math.Min (array.Length, count);
			return Copy<T> (array, 0, count);
		}

		public static T[] Last<T> (T[] array, int count)
		{
			count = Math.Min (array.Length, count);
			return Copy<T> (array, string.Format ("-{0}:", count));
		}

		public static void Devide<T> (T[] array, out T[] arrayA, out T[] arrayB, int index)
		{
			if (index >= array.Length) {
				arrayA = array;
				arrayB = new T[0];
				return;
			}

			if (index <= 0) {
				arrayB = array;
				arrayA = new T[0];
				return;
			}

			arrayA = new T[index];
			arrayB = new T[array.Length - index];

			Array.Copy (array, 0, arrayA, 0, arrayA.Length);
			Array.Copy (array, index, arrayB, 0, arrayB.Length);
		}

		#region Insert, Add

		public static T[] Insert<T> (T[] array, T item, int position)
		{
			T[] tmp = new T[array.Length + 1];

			T[] a;
			T[] b;

			Devide (array, out a, out b, position);

			tmp [position] = item;
			Array.Copy (a, 0, tmp, 0, a.Length);
			Array.Copy (b, 0, tmp, position + 1, b.Length);

			return tmp;

		}

		public static void Insert<T> (ref T[] array, T item, int position)
		{
			T[] tmp = new T[array.Length + 1];

			T[] a;
			T[] b;

			Devide (array, out a, out b, position);

			tmp [position] = item;
			Array.Copy (a, 0, tmp, 0, a.Length);
			Array.Copy (b, 0, tmp, position + 1, b.Length);

			array = tmp;

		}

		public static T[] Insert<T> (T[] array, T[] items, int position)
		{
			T[] tmp = new T[array.Length + items.Length];

			T[] a, b;

			Devide (array, out a, out b, position);
			Array.Copy (a, 0, tmp, 0, a.Length);
			Array.Copy (items, 0, tmp, position, items.Length);
			Array.Copy (b, 0, tmp, position + items.Length, b.Length);

			return tmp;

		}

		public static void Insert<T> (ref T[] array, T[] items, int position)
		{
			T[] tmp = new T[array.Length + items.Length];

			T[] a, b;

			Devide (array, out a, out b, position);
			Array.Copy (a, 0, tmp, 0, a.Length);
			Array.Copy (items, 0, tmp, position, items.Length);
			Array.Copy (b, 0, tmp, position + items.Length, b.Length);

			array = tmp;
		}

		public static void Add<T> (ref T[] array, T item)
		{
			array = Add (array, item);
		}

		public static T[] Add<T> (T[] array, T item)
		{
			return Insert (array, item, array.Length);
		}

		#endregion

		#region Remove


		/// <summary>
		/// получить первый элемент массива и усеч его на 1
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T Pop<T> (ref T[] array)
		{
			T[] tmp = new T[array.Length - 1];
			Array.Copy (array, 1, tmp, 0, tmp.Length);
			T res = array [0];
			array = tmp;
			return res;
		}

		/// <summary>
		/// Получить последний элемент массива и усеч его на 1
		/// </summary>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T Shift<T> (ref T[] array)
		{
			T[] tmp = new T[array.Length - 1];
			Array.Copy (array, 0, tmp, 0, tmp.Length);
			T res = array [tmp.Length];
			array = tmp;
			return res;
		}

		public static T[] RemoveFrom<T> (T[] array, int startIndex, int count)
		{

			if (startIndex > array.Length)
				return array;

			if (startIndex + count > array.Length)
				count = array.Length - startIndex;

			T[] tmp = new T[array.Length - count];
			Array.Copy (array, 0, tmp, 0, startIndex);
			Array.Copy (array, startIndex + count, tmp, startIndex, array.Length - (startIndex + count));
			return tmp;
		}

		public static void RemoveFrom<T> (ref T[] array, int startIndex, int count)
		{
			array = RemoveFrom (array, startIndex, count);
		}

		#endregion

		#region Copy



		public static T[] Copy<T> (T[] array, int? start, int? end)
		{


			if (start != null) {
				if (start < 0)
					start = array.Length + start;
			} else {
				start = 0;
			}

			if (end != null) {
				if (end < 0)
					end = array.Length + end;
			} else {
				end = array.Length;
			}

			if (start > end)
				return new T[0];

			T[] tmp = new T[(int)end - (int)start];

			Array.Copy (array, (int)start, tmp, 0, tmp.Length);

			return tmp;
		}

		public static void Copy<T> (ref T[] array, int? start, int? end)
		{
			array = Copy (array, start, end);
		}

		public static void Copy<T> (ref T[] array, string pythonArrayString)
		{
			array = Copy (array, pythonArrayString);
		}

		public static T[] Copy<T> (T[] array, string pythonArrayString)
		{
			int? start = null;
			int? end = null;
			if (pythonArrayString.IndexOf (":") > -1) {
				string[] args = pythonArrayString.Split (':');
				if (args [0] != "")
					start = int.Parse (args [0]);
				if (args [1] != "")
					end = int.Parse (args [1]);
				return Copy (array, start, end);
			} else {
				int index = int.Parse (pythonArrayString);

				if (index < 0)
					index = array.Length + index;

				if (index < 0)
					return new T[0];
				if (index >= array.Length)
					return new T[0];

				return new T[] { array [index] };
			}
		}

		#endregion 

		#region Renge

		public static int[] Range (int start, int end, int step)
		{
			int[] t = new int[0];
			for (int i=start; i<end; i+=step) {
				Add (ref t, i);
			}
			return t;
		}

		public static int[] Range (int start, int end)
		{
			return Range (start, end, 1);
		}

		public static int[] Range (int end)
		{
			return Range (0, end, 1);
		}

		public IEnumerator<int> XRange (int start, int end, int step)
		{
			for (int i=start; i<end; i+=step) {
				yield return i;
			}
		}

		public IEnumerator<int> XRange (int start, int end)
		{
			return XRange (start, end, 1);
		}

		public IEnumerator<int> XRange (int end)
		{
			return XRange (0, end, 1);
		}

		#endregion
	
		#region Objects

		public static string[] Keys<T> (T obj)
		{
			List<string> keys = new List<string> ();
			foreach (System.Reflection.PropertyInfo info in obj.GetType().GetProperties())
				keys.Add (info.Name);
			return keys.ToArray ();
		}

		public static object[] Values<T> (T obj)
		{
			List<object> vals = new List<object> ();
			foreach (System.Reflection.PropertyInfo info in obj.GetType().GetProperties()) {
				try {
					vals.Add (info.GetValue (obj, null));
				} catch {
				}
			}
			return vals.ToArray ();
		}

		public static string[] Functions<T> (T obj)
		{
			List<string> keys = new List<string> ();
			foreach (System.Reflection.MethodInfo info in obj.GetType().GetMethods()) {
				if ((info.IsPublic) && (!info.IsStatic))
					keys.Add (info.Name);
			}
	
			return keys.ToArray ();
		}

		public static bool Has<T> (T obj, string name)
		{
			foreach (System.Reflection.PropertyInfo info in obj.GetType().GetProperties())
				if (info.Name == name)
					return true;
			return false;
		}

		#endregion
	
		#region Union and Intersection

		/// <summary>
		/// Объеденит уникальные элементы всех массивов arrays. Порядок элементов будет определен 
		/// порядком их появления в исходных массивах.
		/// </summary>
		/// <param name="arrays">Arrays.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Union<T> (params object[] arrays)
		{
			List<T> flat = new List<T> ();

			foreach (object array in arrays) {
				foreach (T item in (T[])array) {
					if (flat.IndexOf (item) == -1)
						flat.Add (item);
				}
			}
			return flat.ToArray ();

		}

		/// <summary>
		/// Вернет массив из элементов, встречающихся в каждом из переданных массивов.
		/// </summary>
		/// <param name="arrays">Arrays.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T[] Intersection<T> (params object[] arrays)
		{
			/*
			List<T> flat = new List<T>();

			foreach(object array in arrays)
			{
				bool found = true;	
				foreach (T item in (T[])array) {
					//TODO: Доделать
				}
			}
			*/
			return null;
		}

		#endregion

		#region utils

		public static void Times (int count, DIterator indexer)
		{
			for (int i=0; i<count; i++)
				indexer.Invoke (i);
		}

		public static string UniqueId ()
		{
			return Guid.NewGuid ().ToString ("N");
		}

		public static string UniqueId (string prefix)
		{
			return prefix + Guid.NewGuid ().ToString ("N");
		}

		#endregion
	}

}
