﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Help : GameComponentBase, IGameComponent {

	public tk2dSimpleButton back;
	public tk2dSimpleButton fwd;
	
	public GameObject[] pagesObj;
	private Vector3 pageDelta = new Vector3(15f, 0, 0);

	int currpage = 1;
	int pages = 3;
	
	// Use this for initialization
	IEnumerator Start () {

		yield return new WaitForEndOfFrame();

		ButtonsBase buttons = GameObject.Find("Buttons").GetComponent<ButtonsBase>();
		buttons.buttons = new List<tk2dSimpleButton>();
		buttons.buttons.Add(back);
		buttons.buttons.Add(fwd);
		buttons.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void NextPage(params object[] args) {
		if (++currpage > pages) return;
		if (fwd && currpage == pages) {
			fwd.renderer.enabled = false;
		}

		pagesObj.ToList().ForEach(p => { if (p != null) p.transform.position -= pageDelta; });
	}
	
	public void PrevPage(params object[] args) {
		if (--currpage > pages) return;
		if (fwd && !fwd.renderer.enabled && currpage < pages) {
			fwd.renderer.enabled = true;
		}

		pagesObj.ToList().ForEach(p => { if (p != null) p.transform.position += pageDelta; });
	}

	void OnDestroy() {

		for (int i = 0; i < pagesObj.Length; i++) {
			Destroy(pagesObj[i]);
			pagesObj[i] = null;
		}
		
		Resources.UnloadUnusedAssets();
	}
}
