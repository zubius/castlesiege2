﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HelpUI : GameComponentBase, IGameComponent {

	public HelpMsgItem[] msg;
	public tk2dTextMesh text;
	public tk2dTextMesh[] randommsg;
	public int level;

	private Queue<HelpMsgItem> msgQ = new Queue<HelpMsgItem>();
	private bool isShowinHelp1 = false;
	private bool isShowinHelp2 = false;

	void Awake() {
		this.Hide ();
		HelpMsg.Init();
	}

	// Use this for initialization
	void Start () {

		if (level == 1) {
			randommsg[2].renderer.enabled = true;
			randommsg[3].renderer.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void ShowLvl1Help1() {
		Debug.Log(HelpMsg.helps[level].Count);
		Hide ();
		randommsg[0].text = HelpMsg.helps[1].Dequeue();
		randommsg[0].Commit();
		randommsg[4].text = HelpMsg.helps[1].Dequeue();
		randommsg[4].Commit();
		randommsg[4].renderer.enabled = true;
		randommsg[0].renderer.enabled = true;
	}

	public void ShowLvl1Help2() {
		if (isShowinHelp2) return;
		isShowinHelp2 = true;
		Hide ();
		randommsg[1].text = HelpMsg.helps[1].Dequeue();
		randommsg[1].Commit();
		randommsg[1].renderer.enabled = true;
		randommsg[4].renderer.enabled = true;
	}

	public void StartShowHelp() {
		Hide ();
		for (int i = 0; i < msg.Length; i++)
			msgQ.Enqueue(msg[i]);
//		ShowMsg(true);
		StartCoroutine(ShowingHelp());
	}

	public void StartShowHelp1() {
		if (isShowinHelp1) return;
		for (int i = 0; i < msg.Length; i++)
			msgQ.Enqueue(msg[i]);
		isShowinHelp1 = true;
		Hide ();
		ShowMsg(true);
		randommsg[0].renderer.enabled = false;
		randommsg[1].renderer.enabled = false;
		StartCoroutine(ShowingHelp1());
	}

	private IEnumerator ShowingHelp1() {
		text.text = HelpMsg.helps[1].Dequeue();
		text.Commit();
		yield return new WaitForSeconds(3);
		text.text = HelpMsg.helps[1].Dequeue();
		PlayerPrefs.SetInt("l1help", 1);
		PlayerPrefs.Save();
	}

	private IEnumerator ShowingHelp() {
		Debug.Log(HelpMsg.helps[level].Count);
		while (msgQ.Count > 0) {
			Hide ();
			HelpMsgItem hmi = msgQ.Dequeue();
			hmi.Show();
			for (int i = 0; i < hmi.texts.Length; i++) {
				hmi.texts[i].text = HelpMsg.helps[level].Dequeue();
				hmi.texts[i].Commit();
			}
			yield return new WaitForSeconds(5);
		}
		Hide ();
		PlayerPrefs.SetInt("l" + level + "help", 1);
		PlayerPrefs.Save();
	}

	void ShowMsg(bool enabled) {
		Renderer[] rs = this.GetComponentsInChildren<Renderer>();
		foreach (var r in rs)
			r.enabled = enabled;
	}

	public override void Hide (params object[] args)
	{
		ShowMsg(false);
		base.Hide (args);
	}
}
