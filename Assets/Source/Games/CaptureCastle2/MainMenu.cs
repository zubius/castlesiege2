﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class MainMenu : GameComponentBase, IGameComponent {

	private ButtonsBase buttons;
	public tk2dSimpleButton[] btns;
	public tk2dSimpleButton top;
	public SpriteRenderer back;

	public GameObject shopPanel;

	public override event ru.appforge.logic.DLogicEvent OnResult;

	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForEndOfFrame();
		buttons = GameObject.Find("Buttons").GetComponent<ButtonsBase>();
		buttons.buttons = new System.Collections.Generic.List<tk2dSimpleButton>();
		buttons.buttons.AddRange(btns);
		buttons.Start();
		buttons = null;
		OnResult (this, new LogicEventArgs ("EventMenuRdy"));

		if (shopPanel)
			shopPanel.transform.position = new Vector3(1000,0,-1f);
	}

	public void ShowShopWindow(params object[] args) {
		if (shopPanel == null) return;
		if ((bool)args[0])
			shopPanel.transform.position = new Vector3(0,0f,-1f);
		else
			shopPanel.transform.position = new Vector3(1000,0,-1f);
	}

	public void Fade(params object[] args) {
		if (!this) return;
		float f = (float)args[0];
		back.color = new Color(f,f,f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnDestroy() {

		Resources.UnloadUnusedAssets();
	}
}
