﻿using UnityEngine;
using System.Collections;

public class CaptureCastle2 : GameComponentBase, IGameComponent {

	public GameObject MainMenu;
	public GameObject LevelSelector;
	public GameObject Level;
	public GameObject LeaderBoard;
	public GameObject Achievements;
	public GameObject Help;
	public GameObject Credits;

	public override event ru.appforge.logic.DLogicEvent OnResult;

	private GameObject go;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LoadMenu(params object[] args) {
		go = InstantiateGO(MainMenu);
		OnResult(this, new ru.appforge.logic.LogicEventArgs("EventLoadedMainMenu"));
	}

	public void LoadLeaderBoard(params object[] args) {
		go = InstantiateGO(LeaderBoard);
		OnResult(this, new ru.appforge.logic.LogicEventArgs("EventLoadedLeaderBoard"));
	}

	public void LoadLevelSelector(params object[] args) {
		go = InstantiateGO(LevelSelector);
		OnResult(this, new ru.appforge.logic.LogicEventArgs("EventLoadedLevelSelector"));
	}

	public void LoadLevel(params object[] args) {
		go = InstantiateGO(Level);
		OnResult(this, new ru.appforge.logic.LogicEventArgs("EventLoadedLevel"));
	}

	public void LoadAhievs(params object[] args) {
		go = InstantiateGO(Achievements);
		OnResult(this, new ru.appforge.logic.LogicEventArgs("EventLoadedAchievs"));
	}

	public void LoadHelp(params object[] args) {
		go = InstantiateGO(Help);
		OnResult(this, new ru.appforge.logic.LogicEventArgs("EventLoadedHelp"));
	}

	public void LoadCredits(params object[] args) {
		go = InstantiateGO(Credits);
		OnResult(this, new ru.appforge.logic.LogicEventArgs("EventLoadedCredits"));
	}

	private GameObject InstantiateGO(GameObject proto) {
		GameObject.Destroy(this.go);
		this.go = null;
		Resources.UnloadUnusedAssets();
		GameObject go = Instantiate(proto) as GameObject;
		go.name = proto.name;
		go.transform.parent = this.transform;

		return go;
	}
}
