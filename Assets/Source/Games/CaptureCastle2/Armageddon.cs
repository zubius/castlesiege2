﻿using UnityEngine;
using System.Collections;

public class Armageddon : MonoBehaviour {

	public tk2dSpriteAnimator arma;
	public SpriteRenderer effect;
	public AudioSource sound;

	private bool pause = false;
	private bool isEffect = false;
	private bool isEffectBack = false;
	private float timer = 0;

	// Use this for initialization
	void Start () {
		effect.renderer.enabled = false;
		arma.renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (pause) return;

		if (isEffect) {
			timer += Time.deltaTime / 0.6f;
			effect.color = Color.Lerp(new Color(1,1,1,0), new Color(1,1,1,1), timer);
			if (timer >= 1) {
				isEffect = false;
				isEffectBack = true;
				timer = 0;
			}
		}
		if (isEffectBack) {
			timer += Time.deltaTime / 0.6f;
			effect.color = Color.Lerp(new Color(1,1,1,1), new Color(1,1,1,0), timer);
			if (timer >= 1) {
				isEffectBack = false;
				timer = 0;
			}
		}
	}

	public void Pause(bool p) {
		pause = p;
		if (pause) {
			if (arma.renderer.enabled) {
				arma.Pause();
				sound.Pause();
			}
		} else {
			if (arma.renderer.enabled) {
				arma.Resume();
				sound.Play();
			}
		}
	}

	public void GoArmageddon(System.Action call) {
		effect.renderer.enabled = true;
		arma.renderer.enabled = true;
		sound.Play();
		arma.Play();
		timer = 0;
		isEffect = true;
		effect.color = new Color(1,1,1,0);
		arma.AnimationCompleted = (tk2dSpriteAnimator anim, tk2dSpriteAnimationClip clip) => {
			effect.renderer.enabled = false;
			arma.renderer.enabled = false;
			arma.StopAndResetFrame();
			if (call != null)
			call();
		};
	}
}
