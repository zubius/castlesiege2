﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class Troops : GameComponentBase, IGameComponent {

	public override event DLogicEvent OnResult;
	public event DLogicEvent OnAchieEarned;

	public tk2dTextMesh AmountNum;
	public tk2dSpriteAnimator animation;
	public SpriteRenderer plaha;

	private Owner owner;
	private int amount;
	private string anim;
	Vector3 enPos;
	int enemyAmount;
	bool kill = false;
	bool pause = false;

	bool isBurning = false;
	float burnDelay = 0.6f;

	void Start() {
	}

	void Update() {
		if (pause) return;

		if (isBurning) {
			burnDelay -= Time.deltaTime;
			if (burnDelay <= 0) {
				_Burn();
				isBurning = false;
			}
		}
	}


	public void Pause(params object[] args) {
		pause = (bool)args[0];
		if (pause) {
			animation.Pause();
		} else {
			animation.Resume();
		}
	}

	public void ShowStat(bool enabled) {
		AmountNum.renderer.enabled = enabled;
		plaha.renderer.enabled = enabled;
	}

	public void SetDestroy() {
		kill = true;
	}

	public Owner Owner {
		get {
			return owner;
		}
		set {
			if (!value.Equals(Owner.Neutral)) {
				string sprite = "";
				switch (value) {
				case Owner.Enemy: 
					sprite = "histori_red";
					anim = "Evil_sold";
					break;
				case Owner.Player: 
					sprite = "histori_green"; 
					anim = "Good_sold";
					break;
				}
				this.animation.Play(anim);
				owner = value;
			}
		}
	}

	public float SpeedCoef {
		get {
			float c = amount > 50 ? 45-(50*0.5f) : 45-(amount*0.5f);
			return c;
		}
	}

	public BuildingBase Target { get; set; }

	public int Amount {
		get {
			return amount;
		}
		set {
			amount = value;
			AmountNum.text = amount.ToString();
			AmountNum.Commit();
		}
	}

	void OnDestroy() {
		OnResult(this, new LogicEventArgs());
		Resources.UnloadUnusedAssets();
	}

	void OnTriggerEnter2D(Collider2D enemy) {
		if (kill) return;
//		Debug.Log("------------------"+this.Owner+"------------------");
		this.GetComponent<BoxCollider2D>().isTrigger = false;
		enemy.GetComponent<BoxCollider2D>().isTrigger = false;
		if (this.Amount > enemy.gameObject.GetComponent<Troops>().Amount) {
			if (enemy.gameObject.GetComponent<Troops>().Owner == this.Owner && enemy.gameObject.GetComponent<Troops>().Target == this.Target) {
				this.Amount += enemy.gameObject.GetComponent<Troops>().Amount;
				Destroy(enemy.gameObject);
				OnResult(enemy.gameObject.GetComponent<Troops>(), new LogicEventArgs());
				this.GetComponent<BoxCollider2D>().isTrigger = true;
			} else if (enemy.gameObject.GetComponent<Troops>().Owner != this.Owner) {
				enemy.gameObject.GetComponent<Troops>().SetDestroy();
				enemyAmount = enemy.gameObject.GetComponent<Troops>().Amount;
				enPos = enemy.gameObject.transform.position;
				Destroy(enemy.gameObject);
				ShowBattle();
			}
		} else if (this.Amount == enemy.gameObject.GetComponent<Troops>().Amount) {
			if (enemy.gameObject.GetComponent<Troops>().Owner != this.Owner && this.Owner == Owner.Player) {
				enemy.gameObject.GetComponent<Troops>().SetDestroy();
				enemyAmount = enemy.gameObject.GetComponent<Troops>().Amount;
				enPos = enemy.gameObject.transform.position;
				Destroy(enemy.gameObject);
				ShowBattle();
			}
//			else if (other.gameObject.GetComponent<Troops>().Owner == this.Owner && other.gameObject.GetComponent<Troops>().Target == this.Target) {
//
//			}
		}
	}

	private void ShowBattle() {
//		Debug.Log(this.Owner.ToString() + "   " + this.Amount + "    " + enemyAmount);

		if (!Achvmnts.Fight.Earned) {
			OnAchieEarned(this, new LogicEventArgs("AchieFight"));
		}

		this.Amount -= enemyAmount;

		this.Show();
		this.ShowStat(false);
		this.GetComponent<TransformMovementPosition>().Pause(true);
		Vector3 oldPos = this.transform.position;
		this.transform.position = new Vector3((this.transform.position.x + enPos.x)/2, (this.transform.position.y + enPos.y)/2, oldPos.z);

		this.animation.Play("Fight");
		this.audio.Play();
		this.animation.AnimationCompleted = (tk2dSpriteAnimator anim, tk2dSpriteAnimationClip clip) => {
			this.audio.Stop();
			anim.AnimationCompleted = null;
			if(this.GetComponent<TransformMovementPosition>())
				this.GetComponent<TransformMovementPosition>().Pause(false);
			this.transform.position = oldPos;
//			this.GetComponent<BoxCollider2D>().isTrigger = true;
			this.animation.Play(this.anim);
			this.Show();
			this.ShowStat(true);
			if (this.Amount == 0) {
				OnResult(this, new LogicEventArgs());
				Destroy(this.gameObject);
			}
		};
	}

	public void Burn(float delay) {
		burnDelay = delay;
		isBurning = true;
		Destroy(this.GetComponent<TransformMovementPosition>());
	}

	private void _Burn() {
		ShowStat(false);
		this.animation.Play("BBQ");
		this.animation.AnimationCompleted = (tk2dSpriteAnimator anim, tk2dSpriteAnimationClip clip) => {
			OnResult(this, new LogicEventArgs());
			Destroy(this.gameObject);
		};
	}
}
