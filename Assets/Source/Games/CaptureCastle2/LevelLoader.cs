﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ru.appforge.logic;

public class LevelLoader : GameComponentBase, IGameComponent {

	public override event DLogicEvent OnResult;

	public GameObject[] levels;
	public GameObject scoreboard;
	public GameObject troopsProto;
	public GameObject CastleGoodProto;
	public GameObject CastleEvilProto;
	public GameObject VillageProto;
	public GameObject FortProto;
	public GameObject GMProto;
	public GameObject AcademyProto;
	public GameObject Message;
	public Message MessageM;
	public GameObject MessageWin;
	public MessageWin MessageMWin;
	public AchievementMessage am;
	public tk2dTextMesh GoldAmount;
	public tk2dTextMesh ScoreAmount;
	public tk2dSimpleButton[] btns;
	public tk2dSimpleButton audioBtn;

	public float t_speed = 2f;

	public Sprite[] audioBtns;
	public AudioClip[] winlose;
	public AudioClip[] defences;//0 - defence; 1 - siege
	public AudioSource siege;

	public Armageddon arma;
	public tk2dSimpleButton armaBtn;
	public tk2dSimpleButton plagueBtn;
	private bool isArmageddonEffect = false;
	public AudioClip plagueSound;
	private bool isSPellEnabled = false;

	public SpriteRenderer loading;

	private bool isFortCaptured = false;
	private bool isGMCaptured = false;
	private Level level;
	private Castle PlayerCastle;
	private Castle EnemyCastle;
	private Village[] villages;
	private Fort Fort;
	private GoldMine GM;
	private Academy Academy;
	private float timer;
	private BuildingBase MoveFrom;
	private BuildingBase MoveTo;
	private List<BuildingBase> buildings = new List<BuildingBase>();
	private Player player = new Player();
	private Player enemy = new Player();
	private List<Troops> allTroops = new List<Troops>();
	private AI ai;
	private bool pause = false;
	private float delayAI = 1f;
	private float timerAISteps = 8f;

	private float releaseTimer = 30f;
	private float releaseGMTimer = 30f;
	private float releaseCut = 30f;

	private int ArmageddonPrice = 50;
	private int PlaguePrice = 70;

	private Vector3 CastleMovePos = new Vector3(0, 0.45f, 0);
	private Vector3 VillageMovePos = Vector3.zero;
	private Vector3 FortMovePos = Vector3.zero;
	private Vector3 AcademyMovePos = new Vector3(0, 0.3f, 0);
	
	private bool isfirst = true;

	private int fortReinfCntr = 0;
	private int fimwimgCntr = 0;

	public List<BuildingBase> Buildings {
		get {
			return buildings;
		}
	}

	public Player Player {
		get {
			return player;
		}
	}

	public Player Enemy {
		get {
			return enemy;
		}
	}

	public List<Troops> AllTroops {
		get {
			return allTroops;
		}
	}

	// Use this for initialization
	void Start () {
		loading.renderer.enabled = true;
		player.Owner = Owner.Player;
		player.Gold = 0;
		player.goldTimer = 0f;
		player.Buildings = new List<BuildingBase>();
		player.Troops = new List<Troops>();
		enemy.Owner = Owner.Enemy;
		enemy.Gold = 0;
		enemy.goldTimer = 0f;
		enemy.Buildings = new List<BuildingBase>();
		enemy.Troops = new List<Troops>();

//		InitBtns();
		am.Hide();

		AudioListener.volume = (float)PlayerPrefs.GetInt("audio", 1);
		if (AudioListener.volume == 0)
			audioBtn.gameObject.GetComponent<SpriteRenderer> ().sprite = audioBtns[1];
		else
			audioBtn.gameObject.GetComponent<SpriteRenderer> ().sprite = audioBtns[0];
	}
	
	// Update is called once per frame
	void Update () {
		if (isfirst) {
			isfirst = false;
			OnResult(this, new LogicEventArgs("EventLevelLoaderCreated"));
		}

		if (pause)
			return;

		if(isArmageddonEffect) {
			Vector3 p = level.transform.position;
			p.y = Mathf.Sin(Time.time * 50f) / 10f;
			level.transform.position = p;
		}

		player.goldTimer += Time.deltaTime;
		enemy.goldTimer += Time.deltaTime;

		if (player.goldTimer * player.GoldGenerationSpeed >= 1f) {
			player.goldTimer = 0;
			player.Gold++;
			GoldAmount.text = player.Gold.ToString();
			GoldAmount.Commit();
		}
		if (enemy.goldTimer * enemy.GoldGenerationSpeed >= 1f) {
			enemy.goldTimer = 0;
			enemy.Gold++;
		}

		CheckUpgradeAvailable(player);
		CheckUpgradeAvailable(enemy);
		CheckSpells();

		if (isFortCaptured) {
			releaseTimer -= Time.deltaTime;
			Fort.FortTimer.UpdateTime(Mathf.CeilToInt(releaseTimer));
			if (releaseTimer <= 0) {
				releaseTimer = releaseCut;
				HandleOnFortRealeseCracken();
			}
		}

		if (isGMCaptured) {
			releaseGMTimer -= Time.deltaTime;
			GM.FortTimer.UpdateTime(Mathf.CeilToInt(releaseGMTimer));
			if (releaseGMTimer <= 0) {
				releaseGMTimer = releaseCut;
				HandleOnGMBonus();
			}
		}

		if (delayAI > 0) {
			if (level.N == 1) return;
			delayAI -= Time.deltaTime;
		} else {
			timerAISteps -= Time.deltaTime;
			if (timerAISteps <= 0) {
				timerAISteps = 8f;
				if (ai != null) {
					ai.OnAITacticsUpdate();
				}
			}
			ai.OnAIBuildsUpdate();
		}
	}

	void OnDestroy() {
		Destroy(level);
		level = null;
		Destroy(PlayerCastle);
		PlayerCastle = null;
		Destroy(EnemyCastle);
		EnemyCastle = null;
		Destroy(Fort);
		Fort = null;
		Destroy(GM);
		GM = null;
		Destroy(Academy);
		Academy = null;
		for (int i = 0; i < villages.Length; i++) {
			Destroy(villages[i]);
			villages[i] = null;
		}

		ai.Clear();

		buildings.Clear();
		AllTroops.Clear();

		Resources.UnloadUnusedAssets();
	}

	public void ShowAchiev(params object[] args) {
		am.Show((string)args[0], (string)args[1]);
	}

	public void Pause(params object[] args) {
		pause = (bool)args[0];
		if (isArmageddonEffect)
			arma.Pause(pause);
	}

	public void UpdateScore(params object[] args) {
		ScoreAmount.text = ((int)args[0]).ToString();
		ScoreAmount.Commit();
	}

	public void ShowMessage(params object[] args) {
		if (Message) {
			if ((bool)args[0])
				MessageM.Show();
			else
				MessageM.Hide();
			MessageM.MessageText.text = (string)args[1];
			MessageM.MessageText.Commit();
//
			string state = (string)args[2];
			if (state != "") {
				MessageM.Show();
				MessageM.SetTotalScore((int)args[3]);
				if (state == "win") {
					Message.audio.clip = winlose[0];
					Message.audio.Play();
					GameObject.Find("Restart").GetComponentInChildren<tk2dSimpleButton>().Hide();
					GameObject.Find("Resume").GetComponentInChildren<tk2dSimpleButton>().Hide();
					GameObject.Find("Back2Menu").GetComponentInChildren<tk2dSimpleButton>().Hide();
					MessageM.SetLevelScore((int)args[4]);
				} else if (state == "lose") {
					Message.audio.clip = winlose[1];
					Message.audio.Play();
					GameObject.Find("Restart").transform.position = GameObject.Find("Back2Menu").transform.position;
					GameObject.Find("Resume").GetComponentInChildren<tk2dSimpleButton>().Hide();
					GameObject.Find("Back2Menu").GetComponentInChildren<tk2dSimpleButton>().Hide();
					GameObject.Find("Next").GetComponentInChildren<tk2dSimpleButton>().Hide();
				}
			} else {
				GameObject.Find("Next").GetComponentInChildren<tk2dSimpleButton>().Hide();
			}
		}
	}

	public void ShowMessageWin(params object[] args) {
		if (MessageWin) {
			if ((bool)args[0])
				MessageMWin.Show();
			else
				MessageMWin.Hide();
			MessageMWin.MessageText.text = (string)args[1];
			//
			string state = (string)args[2];
			if (state != "") {
				MessageMWin.Show();
				MessageMWin.TotalScoreAmountText.text = ((int)args[3]).ToString();
				MessageMWin.Rank.text = (string)args[4];
			}
		}
	}

	public void CheckEndLevel(params object[] args) {
		Owner o = (Owner)args[0];
		Debug.Log(o.ToString());
		Debug.Log(player.Troops.Count);
		Debug.Log(enemy.Troops.Count);
		if (o == player.Owner && player.Troops.Count == 0) {
			OnResult(this, new LogicEventArgs("EventEndLevel"));
		} else if (o == enemy.Owner && enemy.Troops.Count == 0) {
			OnResult(this, new LogicEventArgs("EventEndLevel"));
		}
	}

	public void LoadLevel(params object[] args) {
		if (!this)
			return;
		level = (Instantiate(levels[(int)args[0]]) as GameObject).GetComponent<Level>();
		level.name = levels[(int)args[0]].name;
		level.transform.parent = this.transform;

		int dif = (int)args[1];
		float difcoef = dif > 1 && level.N > 1 ? 1+((float)dif/10) : 1;

		level.GetComponentInChildren<tk2dSimpleButton>().OnClickEnable += HandleOnClearSelect;

		GameObject g = CreateObject(scoreboard, new Vector3(1000f, 1000f, 0));
		g.GetComponentInChildren<ScoreBoardSaveUser>().InitBtns = false;
		g.GetComponentInChildren<ScoreBoardScores>().InitBtns = false;
		g = null;

		buildings = new List<BuildingBase>();

		PlayerCastle = CreateObject(CastleGoodProto, level.PlayerCastle.transform.position).GetComponent<Castle>();
		EnemyCastle = CreateObject(CastleEvilProto, level.EnemyCastle.transform.position).GetComponent<Castle>();
		PlayerCastle.gameObject.name = "PlayerCastle";
		PlayerCastle.Owner = level.PlayerCastle.GetComponent<BuildingProto>().Owner;
		PlayerCastle.TroopsAmount = level.PlayerCastle.GetComponent<BuildingProto>().TroopsAmount;
//		PlayerCastle.TroopsLevel = level.PlayerCastle.GetComponent<BuildingProto>().TroopsLvl;
//		PlayerCastle.GoldLevel = level.PlayerCastle.GetComponent<BuildingProto>().GoldLvl;
		PlayerCastle.OnResult += HandleOnUpgraded;
		buildings.Add(PlayerCastle);
		EnemyCastle.gameObject.name = "EnemyCastle";
		EnemyCastle.Owner = level.EnemyCastle.GetComponent<BuildingProto>().Owner;
		EnemyCastle.TroopsAmount = Mathf.CeilToInt(level.EnemyCastle.GetComponent<BuildingProto>().TroopsAmount * difcoef);
		EnemyCastle.isUpgrFromStartTroop = true;
		EnemyCastle.isUpgrFromStartGold = true;
		EnemyCastle.TroopsLevel = level.EnemyCastle.GetComponent<BuildingProto>().TroopsLvl;
		EnemyCastle.GoldLevel = level.EnemyCastle.GetComponent<BuildingProto>().GoldLvl;
		EnemyCastle.OnResult += HandleOnUpgraded;
		buildings.Add(EnemyCastle);

		PlayerCastle.castleSprite.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnClickBuilding;
		EnemyCastle.castleSprite.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnClickBuilding;

		villages = new Village[level.Villages.Length];
		for (int i = 0; i < villages.Length; i++) {
			villages[i] = CreateObject(VillageProto, level.Villages[i].transform.position, (i+1).ToString()).GetComponent<Village>();
			villages[i].gameObject.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnClickBuilding;
			villages[i].Owner = level.Villages[i].GetComponent<BuildingProto>().Owner;
			villages[i].TroopsAmount = villages[i].Owner == Owner.Enemy ? 
				Mathf.CeilToInt(level.Villages[i].GetComponent<BuildingProto>().TroopsAmount * difcoef) : level.Villages[i].GetComponent<BuildingProto>().TroopsAmount;
			villages[i].OnResult += HandleOnUpgraded;
			buildings.Add(villages[i]);
		}

		if (level.Fort != null) {
			Fort = CreateObject(FortProto, level.Fort.transform.position).GetComponent<Fort>();
			Fort.gameObject.name = "Fort";
			Fort.gameObject.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnClickBuilding;
			Fort.Owner = level.Fort.GetComponent<BuildingProto>().Owner;
			Fort.TroopsAmount = level.Fort.GetComponent<BuildingProto>().TroopsAmount;
			Fort.OnFortCaptured += HandleOnFortCaptured;
			buildings.Add(Fort);
		}

		if (level.GoldMine != null) {
			GM = CreateObject(GMProto, level.GoldMine.transform.position).GetComponent<GoldMine>();
			GM.gameObject.name = "GoldMine";
			GM.gameObject.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnClickBuilding;
			GM.Owner = level.GoldMine.GetComponent<BuildingProto>().Owner;
			GM.TroopsAmount = level.GoldMine.GetComponent<BuildingProto>().TroopsAmount;
			GM.OnFortCaptured += HandleOnGMCaptured;
			buildings.Add(GM);
		}

		if (level.Academy != null) {
			Academy = CreateObject(AcademyProto, level.Academy.transform.position).GetComponent<Academy>();
			Academy.gameObject.name = "Academy";
			Academy.gameObject.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnClickBuilding;
			Academy.Owner = level.Academy.GetComponent<BuildingProto>().Owner;
			Academy.TroopsAmount = Academy.Owner == Owner.Enemy ?
				Mathf.CeilToInt(level.Academy.GetComponent<BuildingProto>().TroopsAmount * difcoef) : level.Academy.GetComponent<BuildingProto>().TroopsAmount;
			Academy.OnResult += HandleOnUpgraded;
			foreach (var b in buildings) {
				Academy.OnAcadUpgr += b.OnAcademyUpgraded;
			}
			Academy.OnAcadUpgr += HandleOnAcadGoldUpgr;
			Academy.OnAcadUpgr += OnAcademyUpgraded;
			Academy.OnAcadNeutral += HandleOnAcadNeutral;
			Academy.OnAcadGotAchie += (s, a) => OnResult(this, new LogicEventArgs("EventAchieEarned", Achvmnts.DiligSchol.Name));
			buildings.Add(Academy);
		}

		int p = buildings.Count(x => x.Owner == Owner.Player);
		int e = buildings.Count(x => x.Owner == Owner.Enemy);


		player = FillPlayer(player, buildings);
		enemy = FillPlayer(enemy, buildings);

		ai = new AI(this);
		InitBtns();

		if (level.N == 1 && PlayerPrefs.GetInt("l1help", 0) == 0 && level.help) {
			level.help.ShowLvl1Help1();
		}
		if (level.N == 2 && PlayerPrefs.GetInt("l"+level.N+"help", 0) == 0 && level.help) {
			level.help.StartShowHelp();
		}
		if (level.N == 5 && PlayerPrefs.GetInt("l"+level.N+"help", 0) == 0 && level.help) {
			level.help.StartShowHelp();
		}
		if (level.N == 9 && PlayerPrefs.GetInt("l"+level.N+"help", 0) == 0 && level.help) {
			level.help.StartShowHelp();
		}
		if (level.N == 13 && PlayerPrefs.GetInt("l"+level.N+"help", 0) == 0 && level.help) {
			level.help.StartShowHelp();
		}
		if (level.N == 19 && PlayerPrefs.GetInt("l"+level.N+"help", 0) == 0 && level.help) {
			level.help.StartShowHelp();
		}

		armaBtn.Enabled = false;
		plagueBtn.Enabled = false;
		isSPellEnabled = level.N > 12;
		armaBtn.renderer.enabled = isSPellEnabled;
		plagueBtn.renderer.enabled = isSPellEnabled;

		OnResult(this, new LogicEventArgs("EventLevelCreated", level.N, p, e));

		loading.renderer.enabled = false;
		Destroy(loading);
		loading = null;
		Resources.UnloadUnusedAssets();

//		player.Gole.Tld = 130;
//		PlayerCastroopsAmount = 999;
	}


	void HandleOnClearSelect (object sender, System.EventArgs e)
	{
		if (pause)
			return;
		if (MoveFrom) {
			MoveFrom.IsSelected = false;
			MoveFrom = null;
		}
	}

	void HandleOnAcadNeutral (object sender, LogicEventArgs args)
	{
		if (!Achvmnts.Firmwimg.Earned && ((Owner)args.Args[1]) == Owner.Player) {
			fimwimgCntr++;
			if (fimwimgCntr == 3) OnResult(this, new LogicEventArgs("EventAchieEarned", Achvmnts.Firmwimg.Name));
		}

		if (enemy.Buildings.Contains((BuildingBase)sender))	enemy.Buildings.Remove((BuildingBase)sender);
		if (player.Buildings.Contains((BuildingBase)sender)) player.Buildings.Remove((BuildingBase)sender);
		int n = buildings.Count(x => x.Owner == Owner.Neutral && x.GetType() != typeof(Academy));
		OnResult(this, new LogicEventArgs("EventBuildingCaptured", ((Owner)args.Args[0]).ToString(), ((Owner)args.Args[1]).ToString(), true, n));
	}

	void HandleOnFortCaptured (object sender, LogicEventArgs args)
	{
		fortReinfCntr = 0;
		isFortCaptured = true;
		releaseTimer = 30f;
	}

	void HandleOnGMCaptured (object sender, LogicEventArgs args)
	{
		fortReinfCntr = 0;
		isGMCaptured = true;
		releaseGMTimer = 30f;
	}

	void HandleOnFortRealeseCracken ()
	{
		if (Fort.Owner == player.Owner) {
			MoveTroops(Fort.Owner, 30, level.PlayerFortSource.transform.position, PlayerCastle);
			if (!Achvmnts.UnapFort.Earned) {
				fortReinfCntr++;
				if (fortReinfCntr == 3) OnResult(this, new LogicEventArgs("EventAchieEarned", Achvmnts.UnapFort.Name));
			}
		} else if (Fort.Owner == enemy.Owner) {
			MoveTroops(Fort.Owner, 30, level.EnemyFortSource.transform.position, EnemyCastle);
		}
	}

	void HandleOnGMBonus ()
	{
		if (GM.Owner == player.Owner) {
			player.Gold += 50;
		} else if (GM.Owner == enemy.Owner) {
			enemy.Gold += 50;
		}
	}

	void HandleOnAcadGoldUpgr (object sender, LogicEventArgs args)
	{
		if (args.EventName == "GGS" && ((Academy)sender).Owner == player.Owner) {
//			if (!player.Buildings.Contains((Academy)sender)) player.Buildings.Add((Academy)sender);
			player.GoldGenerationSpeed = GetGoldGenerationSpeed(player.Buildings);
		} else if (args.EventName == "GGS" && ((Academy)sender).Owner == enemy.Owner) {
//			if (!enemy.Buildings.Contains((Academy)sender)) enemy.Buildings.Add((Academy)sender);
			enemy.GoldGenerationSpeed = GetGoldGenerationSpeed(enemy.Buildings);
		} else if (args.EventName == "GGS" && ((Academy)sender).Owner == Owner.Neutral) {
			if (player.Buildings.Contains((Academy)sender)) player.Buildings.Remove((Academy)sender);
			player.GoldGenerationSpeed = GetGoldGenerationSpeed(player.Buildings);
			if (enemy.Buildings.Contains((Academy)sender)) enemy.Buildings.Remove((Academy)sender);
			enemy.GoldGenerationSpeed = GetGoldGenerationSpeed(enemy.Buildings);
		}
	}

	private void InitBtns() {
		ButtonsBase buttons = GameObject.Find("Buttons").GetComponent<ButtonsBase>();;
		buttons.buttons = new List<tk2dSimpleButton>();
		buttons.buttons.AddRange(btns);
		buttons.Start();
		audioBtn.OnClickEnable += (sender, e) => EnableAudio();
		armaBtn.OnClickEnable += (sender, e) => CallArmageddon();
		plagueBtn.OnClickEnable += (sender, e) => Plague();

		buttons = null;
	}

	void Plague() {
		if (pause || player.Gold < PlaguePrice || !isSPellEnabled) return;
		player.Gold -= PlaguePrice;
		buildings.ForEach(x => { if (x.Owner == Owner.Enemy) x.Plague(); });
		siege.clip = plagueSound;
		siege.Play ();
	}

	void CallArmageddon() {
		if (pause || player.Gold < ArmageddonPrice || !isSPellEnabled) return;
		Debug.Log(allTroops.Count);
		allTroops.ForEach(t => t.Burn(0.6f));
		player.Gold -= ArmageddonPrice;
		isArmageddonEffect = true;
		armaBtn.Enabled = false;
		arma.GoArmageddon(() => { 
			isArmageddonEffect = false;
			OnResult(this, new LogicEventArgs("EventEndLevel"));
		});
	}

	void EnableAudio ()
	{
		if (AudioListener.volume > 0) {
			AudioListener.volume = 0;
			PlayerPrefs.SetInt("audio", 0);
			PlayerPrefs.Save();
			audioBtn.gameObject.GetComponent<SpriteRenderer>().sprite = audioBtns[1];
		} else {
			AudioListener.volume = 1;
			PlayerPrefs.SetInt("audio", 1);
			PlayerPrefs.Save();
			audioBtn.gameObject.GetComponent<SpriteRenderer>().sprite = audioBtns[0];
		}
	}

	void HandleOnUpgraded (object sender, LogicEventArgs args)
	{
		if ((Owner)args[0] == Owner.Player) {
			player.Gold -= (int)args[1];
			player.GoldGenerationSpeed = GetGoldGenerationSpeed(player.Buildings);
			OnResult(this, new LogicEventArgs("EventBuildingUpgraded"));

			if (((BuildingBase)sender).GetType() == typeof(Castle)) {
				if (((Castle)sender).TroopsLevel == 6 && ((Castle)sender).GoldLevel == 6)
					OnResult(this, new LogicEventArgs("EventAchieEarned", Achvmnts.CityMngr.Name));
			}

		} else if ((Owner)args[0] == Owner.Enemy) {
			enemy.Gold -= (int)args[1];
			enemy.GoldGenerationSpeed = GetGoldGenerationSpeed(enemy.Buildings);
		}
	}

	void OnAcademyUpgraded(object sender, LogicEventArgs args) {
		if (!Achvmnts.AncntKnow.Earned) {
			if (((Academy)sender).Level == 6) {
				OnResult(this, new LogicEventArgs("EventAchieEarned", Achvmnts.AncntKnow.Name));
			}
		}
		if (args.EventName == "TMS" && ((Academy)sender).Owner == player.Owner) {
			player.Tms_mod = (float)args.Args[0];
		} else if (args.EventName == "TMS" && ((Academy)sender).Owner == enemy.Owner) {
			enemy.Tms_mod = (float)args.Args[0];
		} else if (args.EventName == "TMS") {
			player.Tms_mod = 1f;
			enemy.Tms_mod = 1f;
		}
	}


	public void MoveTroops(Owner owner, int amount, Vector3 From, BuildingBase To) {
		Vector3 moveMod = new Vector3();

		var @switch = new Dictionary<System.Type, System.Action> {
			{ typeof(Castle), () => moveMod = CastleMovePos },
			{ typeof(Village), () => moveMod = VillageMovePos },
			{ typeof(Academy), () => moveMod = AcademyMovePos },
			{ typeof(Fort), () => moveMod = FortMovePos },
			{ typeof(GoldMine), () => moveMod = FortMovePos }
		};

		@switch[To.GetType()]();

		Vector2 tp = GetTargetCoord(From - moveMod, To.gameObject.transform.position - moveMod, 0.7f);
		Vector3 ToPos = new Vector3(tp.x, tp.y, To.gameObject.transform.position.z);
		Vector2 fp = GetTargetCoord(To.gameObject.transform.position - moveMod, From - moveMod, 0.7f);
		Vector3 FromPos = new Vector3(fp.x, fp.y, To.gameObject.transform.position.z);

		GameObject t = CreateObject(troopsProto, FromPos);
		Troops troop = t.GetComponent<Troops>();
		TransformMovementPosition tt = t.GetComponent<TransformMovementPosition>();
		t = null;
		troop.Owner = owner;
		troop.Amount = amount;
		troop.Target = To;
		troop.OnResult += HandleOnTroopDestroyed;
		if (!Achvmnts.Fight.Earned) {
			troop.OnAchieEarned += HandleOnAchieEarned;
		}

		allTroops.Add(troop);
		if (troop.Owner == player.Owner)
			player.Troops.Add(troop);
		else if (troop.Owner == enemy.Owner)
			enemy.Troops.Add(troop);

		float d = Vector3.Distance(From, To.gameObject.transform.position);
		if (From.x > To.transform.position.x) {
			troop.animation.GetComponent<tk2dSprite>().FlipX = true;
		}
		float mod = owner == player.Owner ? player.Tms_mod : enemy.Tms_mod;
//		Debug.Log("mod = " + mod);

		tt.Move(FromPos, ToPos, (d / (t_speed * ((troop.SpeedCoef * mod)/ 50f))));
		tt.OnResult += HandleTroopsMoved;
	}

	void HandleOnAchieEarned (object sender, LogicEventArgs args)
	{
		if (args.EventName == "AchieFight")
			OnResult(this, new LogicEventArgs("EventAchieEarned", Achvmnts.Fight.Name));
	}

	private Vector2 GetTargetCoord(Vector2 x1, Vector2 x2, float delt) {
		float d = Vector3.Distance(x1, x2);
		float m1 = d-delt;
		float m2 = delt;
		float x = (m2*x1.x + m1*x2.x) / (m1+m2);
		float y = (m2*x1.y + m1*x2.y) / (m1+m2);

		return new Vector2(x, y);
	}

	void HandleOnTroopDestroyed (object sender, LogicEventArgs args)
	{
		Troops t = (Troops)sender;
		allTroops.Remove(t);
		if (t.Owner == player.Owner) {
			player.Troops.Remove(t);
		} else if (t.Owner == enemy.Owner) {
			enemy.Troops.Remove(t);
		}
		t = null;
	}

	private void CheckUpgradeAvailable(Player p) {
		if (level.N == 1) return;
//		if (p.Owner == Owner.Enemy)
//			Debug.Log(ru.appforge.utils.U.ToString(p.Buildings.ToArray()));
		foreach (BuildingBase b in p.Buildings) {
//			if (b.GetType() == typeof(Academy) && p.Owner != b.Owner) {
//				Debug.Log(ru.appforge.utils.U.ToString(enemy.Buildings.ToArray()));
//				Debug.Log(p.Owner + "  " + b.Owner);
//				Debug.Break();
//			}
			b.SetUpgrGoldButton(p.Gold >= b.GoldUpgrPrice && b.GoldUpgrPrice > 0);
			b.SetUpgrTroopsButton(p.Gold >= b.TroopsUpgrPrice && b.TroopsUpgrPrice > 0);
		}
	}

	private void CheckSpells() {
		if (!isSPellEnabled) return;
		plagueBtn.Enabled = player.Gold >= PlaguePrice;
		if (!isArmageddonEffect)
			armaBtn.Enabled = player.Gold >= ArmageddonPrice;
	}

	private GameObject CreateObject(GameObject proto, Vector3 pos, string nameAppdx = "") {
		GameObject obj = Instantiate(proto, pos, proto.transform.rotation) as GameObject;
		obj.transform.parent = level.transform;
		obj.name = proto.name + nameAppdx;

		return obj;
	}

	private Player FillPlayer (Player p, List<BuildingBase> buildings)
	{
		p.Buildings = buildings.Where(b => b.Owner == p.Owner).ToList();
		p.GoldGenerationSpeed = GetGoldGenerationSpeed(p.Buildings);
		return p;
	}

	private void SiegeBuilding (Troops t, BuildingBase target)
	{
		string anim = "";
		int r = t.Amount - target.TroopsAmount;
		if (r > 0) {

			if (target.IsSelected) {
				target.IsSelected = false;
				MoveFrom = null;
			}

			anim = "DefenceFail";
			siege.clip = defences[1];
			siege.Play();
			target.PlayDefenceAnim(anim);
			var oldOwner = target.Owner;
			target.Owner = t.Owner;
			target.TroopsAmount = r;
			if (target.GetType() == typeof(Academy)) {
				if (t.Owner == Owner.Player) {
					((Academy)target).SetBonus();
				} else if (t.Owner == Owner.Enemy) {
					Academy.BonusType[] bns = {Academy.BonusType.GoldGenSpeed, Academy.BonusType.TroopsGenSpeed, Academy.BonusType.TroopsMoveSpeed};
					Academy.Bonus = bns[Random.Range(0, bns.Length)];
				}
			} 

			if (t.Owner == Owner.Player) {

				if (target.GetType() == typeof(Castle) && !Achvmnts.Cptrd.Earned)
					OnResult(this, new LogicEventArgs("EventAchieEarned", Achvmnts.Cptrd.Name));

				if (!player.Buildings.Contains(target)) player.Buildings.Add(target);
				if (enemy.Buildings.Contains(target)) enemy.Buildings.Remove(target);
				if (target.GetType() != typeof(Academy))
					player.GoldGenerationSpeed = GetGoldGenerationSpeed(player.Buildings);
			} else if (t.Owner == Owner.Enemy) {
				if (!enemy.Buildings.Contains(target)) enemy.Buildings.Add(target);
				if (player.Buildings.Contains(target)) player.Buildings.Remove(target);
				if (target.GetType() != typeof(Academy))
					enemy.GoldGenerationSpeed = GetGoldGenerationSpeed(enemy.Buildings);
			}

			bool isAcademy = target.GetType() == typeof(Academy);
			int n = buildings.Count(x => x.Owner == Owner.Neutral && x.GetType() != typeof(Academy));
			OnResult(this, new LogicEventArgs("EventBuildingCaptured", target.Owner.ToString(), oldOwner.ToString(), isAcademy, n));
		} else {
			target.TroopsAmount = -r;
			anim = "DefenceSucc";
			siege.clip = defences[0];
			siege.Play();
			target.PlayDefenceAnim(anim);
		}

		if (t.transform.position.x < target.transform.position.x)
			target.DefenceAnim.Sprite.FlipX = true;
	}

	private float GetGoldGenerationSpeed(List<BuildingBase> builds) {
		float ggs = 0;
		foreach (var b in builds) {
			if (b.GetType() != typeof(Academy))
				ggs += b.GoldGenerationSpeed;
		}
		return ggs;
	}

	void HandleTroopsMoved (object obj)
	{
		Troops t = (obj as TransformMovementPosition).gameObject.GetComponent<Troops>();

		allTroops.Remove(t);
		if (t.Owner == player.Owner)
			player.Troops.Remove(t);
		else if (t.Owner == enemy.Owner)
			enemy.Troops.Remove(t);

		if (t.Owner == t.Target.Owner) {
			if (t.Target.TroopsAmount + t.Amount <= 999)
				t.Target.TroopsAmount += t.Amount;
			else
				t.Target.TroopsAmount = 999;
		} else {
			SiegeBuilding(t, t.Target);
		}
		DestroyImmediate((obj as TransformMovementPosition).gameObject);
	}

	void HandleOnClickBuilding (object sender, System.EventArgs e)
	{
		if (pause)
			return;
		BuildingBase b = (sender as tk2dSimpleButton).gameObject.GetComponent<BuildingBase>();
		if (!b)
			b = (sender as tk2dSimpleButton).transform.parent.gameObject.GetComponent<BuildingBase>();
		if (b.Owner == Owner.Player && MoveFrom == null) {
			if (level.N == 1 && PlayerPrefs.GetInt("l1help", 0) == 0 && level.help) {
				level.help.ShowLvl1Help2();
			}
			MoveFrom = b;
			MoveFrom.IsSelected = true;
		} else if (MoveFrom != null && !MoveFrom.Equals(b)) {
			if (level.N == 1 && PlayerPrefs.GetInt("l1help", 0) == 0 && level.help) {
				level.help.StartShowHelp1();
			}
			MoveTo = b;
			if (MoveFrom.TroopsAmount > 0) {
				int amount = Mathf.CeilToInt((float)MoveFrom.TroopsAmount / 2f);
				MoveFrom.TroopsAmount -= amount;
				MoveTroops(Owner.Player, amount, MoveFrom.gameObject.transform.position, MoveTo);

				MoveTo = null;
				MoveFrom.IsSelected = false;
				MoveFrom = null;
			}
		} else if (MoveFrom != null && MoveFrom.Equals(b)) {
			MoveFrom.IsSelected = false;
			MoveFrom = null;
		}
	}
}
