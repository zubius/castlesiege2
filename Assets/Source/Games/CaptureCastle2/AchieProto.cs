﻿using UnityEngine;
using System.Collections;

public class AchieProto : GameComponentBase, IGameComponent {

	public SpriteRenderer cup;
	public Sprite cupwin;
	public Sprite cup_;
	public tk2dTextMesh name;
	public tk2dTextMesh data;
	public tk2dTextMesh descr;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Fill(Achiev a) {
		if (a.Earned) cup.sprite = cupwin;
		else cup.sprite = cup_;
		name.text = a.Name;
		name.Commit();
		data.text = a.OnAchieved.ToString() == "01/01/0001 00:00:00" ? "" : a.OnAchieved.ToString();
		data.Commit();
		descr.text = a.Description;
		descr.Commit();
	}
}
