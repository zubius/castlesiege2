﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour {

	public tk2dSimpleButton back;

	// Use this for initialization
	void Start () {
		ButtonsBase buttons = GameObject.Find("Buttons").GetComponent<ButtonsBase>();;
		buttons.buttons = new System.Collections.Generic.List<tk2dSimpleButton>();
		buttons.buttons.Add(back);
		buttons.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
