﻿using UnityEngine;
using System.Collections;

public class DifficultySelecter : GameComponentBase, IGameComponent {

	public override event ru.appforge.logic.DLogicEvent OnResult;

	public tk2dSimpleButton[] Stars;

	private int diff;
	private float delay = 0.3f;
	private bool isHiding = false;
	private int selectedDiff = 0;
	
	// Use this for initialization
	void Start () {

//		this.GetComponentInChildren<TextMesh>().renderer.sortingLayerID = this.renderer.sortingLayerID;

		foreach (var s in Stars) {
			s.OnClickEnable += HandleOnClicStar;
//			s.Disable();
//			s.gameObject.GetComponent<tk2dSprite>().SetSprite("_0000_L_star_2");
		}
		Hide ();
	}

	void HandleOnClicStar (object sender, System.EventArgs e)
	{
		selectedDiff = int.Parse(System.Text.RegularExpressions.Regex.Match(((tk2dSimpleButton)sender).name, @"\d+").Value);
		for (int i = 0; i < selectedDiff; i++) {
			Stars[i].gameObject.GetComponent<tk2dSprite>().SetSprite("_0001_L_star_3");
		}
		isHiding = true;
		delay = 0.3f;
	}
	
	// Update is called once per frame
	void Update () {
		if (isHiding) {
			delay -= Time.deltaTime;
			if (delay <= 0) {
				Hide ();
				OnResult(this, new ru.appforge.logic.LogicEventArgs("EventDiffSelected", selectedDiff));
			}
		}
	}

	public override void Show (params object[] args)
	{
		foreach (var s in Stars) {
			if (s) {
				s.Disable();
				s.gameObject.GetComponent<tk2dSprite>().SetSprite("star_3_1");
			}
		}

		diff = (int)args[0];

		for (int i = 0; i < diff; i ++) {
			if (Stars[i]) {
				Stars[i].Enable();
				Stars[i].gameObject.GetComponent<tk2dSprite>().SetSprite("_0000_L_star_2");
			}
		}

		if (this) {
			this.transform.position = new Vector3(0,0,-2);
			base.Show (args);
		}
	}

	public override void Hide (params object[] args)
	{
		this.transform.position = new Vector3(1000,1000,-2);
		base.Hide (args);
	}
}
