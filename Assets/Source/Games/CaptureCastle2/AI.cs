﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AI {

	LevelLoader world;
	List<BuildingBase> allBuildings;
	List<BuildingBase> playerBuildings;
	List<BuildingBase> myBuildings;
	List<Troops> allTroops;
	List<Troops> myTroops;
	List<Troops> playerTroops;
	BuildingBase playerCastle;
	BuildingBase myCastle;
	BuildingBase Academy;
	BuildingBase Fort;
	BuildingBase GM;
	Player player;
	Player me;

	BuildingBase target;

	public AI (LevelLoader world) {
		this.world = world;
		this.allBuildings = world.Buildings;
		this.player = world.Player;
		this.me = world.Enemy;
		this.playerBuildings = player.Buildings;
		this.myBuildings = me.Buildings;
		this.allTroops = world.AllTroops;
		this.myTroops = me.Troops;
		this.playerTroops = player.Troops;

		List<Castle> tc;
		tc = playerBuildings.OfType<Castle>().ToList();
		this.playerCastle = tc.Count > 0 ? tc[0] as BuildingBase : null;
		tc = myBuildings.OfType<Castle>().ToList();
		this.myCastle = tc.Count > 0 ? tc[0] as BuildingBase : null;

		List<Academy> ta;
		ta = allBuildings.OfType<Academy>().ToList();
		this.Academy = ta.Count > 0 ? ta[0] as BuildingBase : null;

		List<Fort> tf;
		tf = allBuildings.OfType<Fort>().ToList();
		this.Fort = tf.Count > 0 ? tf[0] as BuildingBase : null;

		List<GoldMine> tgm;
		tgm = allBuildings.OfType<GoldMine>().ToList();
		this.GM = tgm.Count > 0 ? tgm[0] as BuildingBase : null;
	}

	public void Clear() {
		this.world = null;
		this.allBuildings.Clear();
		this.player = null;
		this.me = null;
		this.playerBuildings.Clear();
		this.myBuildings.Clear();
		this.allTroops.Clear();
		this.myTroops.Clear();
		this.playerTroops.Clear();
		this.playerCastle = null;
		this.myCastle = null;
		this.Academy = null;
		this.Fort = null;
		this.GM = null;
	}

	public void OnAITacticsUpdate() {
		if (myCastle.Owner != me.Owner && myBuildings.Count != 0) {
			myCastle = myBuildings[0];
		}
		if (Fort && Fort.Owner == player.Owner && myBuildings.Count > 2) {
			target = Fort;
			Attack(target);
			return;
		}
		if (GM && GM.Owner == player.Owner && myBuildings.Count > 2) {
			target = GM;
			Attack(target);
			return;
		}
		if (myBuildings.Count != 0) {
			target = GetMinDistance();
			if (target != myCastle) {
				Attack(target);
			}
		}
	}

	public void OnAIBuildsUpdate () {
//		myBuildings = allBuildings.Where(x => x.Owner == me.Owner).ToList();
//		Debug.Log("Update " +ru.appforge.utils.U.ToString(myBuildings.ToArray()));
		foreach (var b in myBuildings) {
			if (!Academy || Academy.Owner != me.Owner) {
				UpgradeBuild(b);
			} else if (Academy && ((Academy)Academy).Level <= b.GoldLevel) {
				if (me.Gold > ((Academy)Academy).GoldUpgrPrice && ((Academy)Academy).Level < 6) {
					if (((Academy)Academy).Bonus == global::Academy.BonusType.None) {
							Academy.BonusType[] bns = {global::Academy.BonusType.GoldGenSpeed, global::Academy.BonusType.TroopsGenSpeed, global::Academy.BonusType.TroopsMoveSpeed};
						((Academy)Academy).Bonus = bns[Random.Range(0, bns.Length)];
					}
					((Academy)Academy).UpgradeGold();
				}
			} else if (b.Owner == me.Owner){
				UpgradeBuild(b);	
			}
		}
	}

	private void UpgradeBuild (BuildingBase b)
	{
		if (me.Gold > b.TroopsUpgrPrice && b.TroopsLevel < 6) {
			b.UpgradeTroops();
		}
		if (me.Gold > b.GoldUpgrPrice && b.GoldLevel < 6) {
			b.UpgradeGold();
		}
	}

	private BuildingBase GetMinDistance() {
		Dictionary<float, BuildingBase> dists = new Dictionary<float, BuildingBase>();

		var notMyBuilding = allBuildings.Where(x => x.Owner != me.Owner).ToList();
		for (int i = 0; i < notMyBuilding.Count; i++) {
			var building = notMyBuilding[i];

			float d = Vector3.Distance(building.gameObject.transform.position, myCastle.gameObject.transform.position);
			float di = d * 0.5f * (building.TroopsAmount+1);
			if (building == Academy) {

			}

			dists.Add(di, building);
		}

		if (dists.Count > 0) {
			var keys = dists.Keys.ToList();
			keys.Sort();
			return dists[keys[0]];
		}

		return myCastle;
	}

	private void Attack(BuildingBase target) {
//		myBuildings = allBuildings.Where(x => x.Owner == me.Owner).ToList();
//		Debug.Log("Attack " + ru.appforge.utils.U.ToString(myBuildings.ToArray()));
		for (int i = 0; i < myBuildings.Count; i++) {
			var building = myBuildings[i];

			if (building.TroopsAmount > 5  && building != Academy && building != Fort) {
				 var b_troops = Mathf.FloorToInt(building.TroopsAmount * 0.5f);

				if (building.TroopsAmount > target.TroopsAmount * 0.7f && building != target) {
					building.TroopsAmount -= b_troops;
					world.MoveTroops(me.Owner, b_troops, building.gameObject.transform.position, target);
				} else {
					if (Academy && Academy.Owner == player.Owner && Academy.TroopsAmount < 10) {
						building.TroopsAmount -= b_troops;
						world.MoveTroops(me.Owner, b_troops, building.gameObject.transform.position, Academy);
						return;
					}

					var randomBuilding = myBuildings[UnityEngine.Random.Range(0, myBuildings.Count)];

					if (randomBuilding != building && randomBuilding != Academy) {
						building.TroopsAmount -= b_troops;
						world.MoveTroops(me.Owner, b_troops, building.gameObject.transform.position, randomBuilding);
					}
				}
			}
		}
	}
}
