﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class Achvmnts {

	public static Achiev Cptrd = new Achiev("Captured!", "Caprute the enemy castle");
	public static Achiev CityMngr = new Achiev("City manager", "Upgrade castle to maximum level");
	public static Achiev Fight = new Achiev("Fight!", "Perform an outdoor battle");
	public static Achiev NbdsPrpty = new Achiev("Nobody's property!", "Finish stage with neutral building");
	public static Achiev UnapFort = new Achiev("Unapproachable Fort!", "Receive reinforcements three time in a row");
	public static Achiev AncntKnow = new Achiev("Ancient knowledge", "Upgrade academy to maximum level");
	public static Achiev DiligSchol = new Achiev("Diligent scholar", "Make three different branches upgrade in academy");
	public static Achiev Hrglss = new Achiev("Hourglasses", "Win final stage in less than 3 minutes");
	public static Achiev Firmwimg = new Achiev("For in much wisdom is much grief", "All the troops perished in academy three time in one stage");
	public static Achiev LnlyStar = new Achiev("Lonely star", "Win atleast one star on each stage of the first map");
	public static Achiev SweetCpl = new Achiev("Sweet couple", "Win atleast two star on each stage of the first map");
	public static Achiev TeamAllStrs = new Achiev("Team All-Stars", "Win three star on each stage of the first map");

	public static Dictionary<string, Achiev> Achvs = new Dictionary<string, Achiev>() {
		{ Cptrd.Name, Cptrd },
		{ CityMngr.Name, CityMngr },
		{ Fight.Name, Fight },
		{ NbdsPrpty.Name, NbdsPrpty },
		{ UnapFort.Name, UnapFort },
		{ AncntKnow.Name, AncntKnow },
		{ DiligSchol.Name, DiligSchol },
		{ Hrglss.Name, Hrglss },
		{ Firmwimg.Name, Firmwimg },
		{ LnlyStar.Name, LnlyStar },
		{ SweetCpl.Name, SweetCpl },
		{ TeamAllStrs.Name, TeamAllStrs }
	};

	static Achvmnts() {
		var achs = Achvs.Keys;
		foreach (var a in achs) {
			if (PlayerPrefs.GetString(a, "").Length > 0) {
				Achvs[a].Earned = true;
				try {
					Achvs[a].OnAchieved = Convert.ToDateTime(PlayerPrefs.GetString(a));
				} catch {}
			}
		}
	}
}

public class Achiev {

	public string Name { get; set; }
	public string Description { get; set; }
	public bool Earned { get; set; }
	public DateTime OnAchieved {get; set; }

	public Achiev(string n, string d, bool e = false) {
		Name = n;
		Description = d;
		Earned = e;
	}
}
