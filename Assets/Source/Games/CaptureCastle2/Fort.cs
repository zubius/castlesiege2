﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class Fort : BuildingBase {

	public event DLogicEvent OnFortCaptured;

	public FortTimer FortTimer;
	public Transform p_fpos;
	public Transform e_fpos;

	public override void PlayPlagueAnim ()
	{
		DefenceAnim.transform.localPosition = new Vector3(0, 0.1347407f, -0.5f);
		DefenceAnim.Sprite.scale = new Vector3(1.3f, 1.3f, 1f);
		base.PlayPlagueAnim ();
	}

	public override void PlayDefenceAnim (string anim)
	{
		DefenceAnim.transform.localPosition = new Vector3(0, -0.1728098f, -0.5f);
		DefenceAnim.Sprite.scale = new Vector3(1f, 1f, 1f);
		base.PlayDefenceAnim (anim);
	}

	public override float GoldGenerationSpeed {
		get {
			return 0f;
		}
	}

	public override float TroopsGenerationSpeed {
		get {
			return 1f;
		}
	}

	public override void SetUpgrGoldButton (bool enable) {base.SetUpgrGoldButton(false);}
	public override void SetUpgrTroopsButton (bool enable) {base.SetUpgrTroopsButton(false);}

	public override void UpgradeGold () {}
	public override void UpgradeTroops () {}

	public override Owner Owner {
		get {
			return base.Owner;
		}
		set {
			if (value != Owner.Neutral) {
				base.Owner = value;

				if (base.Owner == Owner.Player) {
					FortTimer.Show(p_fpos, Owner.Player);
				} else if (base.Owner == Owner.Enemy) {
					FortTimer.Show(e_fpos, Owner.Enemy);
				}

				OnFortCaptured(this, new LogicEventArgs("FortCaptured", Owner));
			}
		}
	}

	protected override void Update () 
	{

	}

	protected override void Awake ()
	{
		TroopsLevelNumber.renderer.enabled = false;
		GoldLevelNumber.renderer.enabled = false;
		GGSNumber.renderer.enabled = false;
		TGSNumber.renderer.enabled = false;
		FortTimer.Hide();
		base.Awake ();
	}
}
