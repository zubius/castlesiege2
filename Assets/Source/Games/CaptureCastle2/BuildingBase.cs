﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;
using System.Collections.Generic;

public class BuildingBase : GameComponentBase, IGameComponent {

	public override event ru.appforge.logic.DLogicEvent OnResult;

	public tk2dTextMesh TroopsAmountNumber;
	public tk2dTextMesh GoldLevelNumber;
	public tk2dTextMesh TroopsLevelNumber;
	public tk2dTextMesh GGSNumber;
	public tk2dTextMesh TGSNumber;
	public tk2dSpriteAnimator Selection;
	public tk2dSpriteAnimator Upgrade;
	public SpriteRenderer UpgrTroops;
	public SpriteRenderer UpgrGold;
	public SpriteRenderer OwnerIndicator;
	public tk2dSpriteAnimator DefenceAnim;

	public AudioClip UpgrGoldSound;
	public AudioClip UpgrTroopsSound;

	public Sprite[] levels;
	public Sprite[] owners;

	public bool isInit = true;

	protected float troopsTimer;

	protected int goldLevel;
	protected int tgs_speed_mod = 1;
	protected int ggs_speed_mod = 1;
	protected int troopsLevel;
	protected Dictionary<int, float[]> GGS = new Dictionary<int, float[]>(); //key = building level, values = coefs for this key by academy levels
	protected Dictionary<int, float[]> TGS = new Dictionary<int, float[]>();
	public int troopsAmount;
	private bool isSelected = false;
	public Owner owner;
	public bool pause;

	public virtual void PlayDefenceAnim(string anim) {
		PlayAnim(anim);
	}

	public virtual void PlayPlagueAnim() {
		DefenceAnim.Sprite.FlipX = false;
		PlayAnim("Plague");
	}

	private void PlayAnim(string anim) {
		DefenceAnim.StopAndResetFrame();
		DefenceAnim.Play(anim);
		DefenceAnim.renderer.enabled = true;
		DefenceAnim.AnimationCompleted = (tk2dSpriteAnimator a, tk2dSpriteAnimationClip c) => {
			DefenceAnim.renderer.enabled = false;
			DefenceAnim.Sprite.FlipX = false;
		};
	}

	public virtual int GoldLevel {
		get {
			return goldLevel;
		}
		set {
			if (value <= GoldUpgrCostSteps.Length) {
				goldLevel = value;
				if (goldLevel > 1) {
					UpgradAnim();
					this.audio.clip = UpgrGoldSound;
					this.audio.Play();
				}
				GoldLevelNumber.text = goldLevel.ToString();
				GoldLevelNumber.Commit();
				GGSNumber.text = (GoldGenerationSpeed).ToString() + "/s";
				GGSNumber.Commit();
			}
		}
	}

	public virtual int TroopsLevel {
		get {
			return troopsLevel;
		}
		set {
			if (value <= TroopsUpgrCostSteps.Length) {
				troopsLevel = value;
				if (troopsLevel > 1) {
					UpgradAnim();
					this.audio.clip = UpgrTroopsSound;
					this.audio.Play();
				}
				TroopsLevelNumber.text = troopsLevel.ToString();
				TroopsLevelNumber.Commit();
				TGSNumber.text = (TroopsGenerationSpeed).ToString() + "/s";
				TGSNumber.Commit();
				if (levels.Length > 0) {
					this.GetComponent<SpriteRenderer>().sprite = levels[troopsLevel-1];
				}
			}
		}
	}

	public virtual int TroopsAmount {
		get {
			return troopsAmount;
		}
		set {
			troopsAmount = value;
			TroopsAmountNumber.text = troopsAmount.ToString();
			TroopsAmountNumber.Commit();
		}
	}

	public virtual bool IsSelected {
		get {
			return isSelected;
		}
		set {
			isSelected = value;
			Selection.renderer.enabled = isSelected;
			if (isSelected)
				Selection.Play();
			else
				Selection.StopAndResetFrame();
		}
	}

	public virtual Owner Owner {
		get {
			return owner;
		}
		set {
			if (value != Owner.Player) {
				SetUpgrTroopsButton(false);
				SetUpgrGoldButton(false);
			}
			switch (value) {
			case Owner.Enemy: OwnerIndicator.sprite = owners[0];	break;
			case Owner.Neutral: OwnerIndicator.sprite = owners[2]; break;
			case Owner.Player: OwnerIndicator.sprite = owners[1]; break;
			}

			GameObject go = GameObject.Find("Academy");
			if (go) {
				Academy ac = go.GetComponent<Academy>();
				if (ac.Owner != value) {
					tgs_speed_mod = 1;
					ggs_speed_mod = 1;
				} else {
//					Debug.Log(ac.ggs_level);
//					Debug.Log(ac.tgs_level);
					tgs_speed_mod = ac.tgs_level > 0 ? ac.tgs_level : 1;
					ggs_speed_mod = ac.ggs_level > 0 ? ac.ggs_level : 1;
				}
			}

			owner = value;
		}
	}

	public void Pause(params object[] args) {
		pause = (bool)args[0];
		if (pause) DefenceAnim.Pause();
		else DefenceAnim.Resume();
	}

	public void Plague ()
	{
		PlayPlagueAnim();
		this.TroopsAmount = Mathf.CeilToInt(this.TroopsAmount/2f);
	}

	public virtual float GoldGenerationSpeed { get; set; }
	public virtual float TroopsGenerationSpeed { get; set; }
	public virtual int GoldUpgrPrice { get { return GoldLevel < GoldUpgrCostSteps.Length ? GoldUpgrCostSteps[GoldLevel] : -1;} }
	public virtual int TroopsUpgrPrice { get { return TroopsLevel < TroopsUpgrCostSteps.Length ? TroopsUpgrCostSteps[TroopsLevel] : -1;} }
	
	protected int[] TroopsUpgrCostSteps = {0, 15, 20, 25, 35, 50};
	protected int[] GoldUpgrCostSteps = {0, 10, 15, 20, 30, 40};

	public virtual void SetUpgrTroopsButton (bool enable) {
		if (enable && Owner != Owner.Player || pause)
			return;
		UpgrTroops.renderer.enabled = enable;
		UpgrTroops.gameObject.GetComponent<tk2dSimpleButton>().ButtonDisable = !enable;
	}

	public virtual void SetUpgrGoldButton (bool enable) {
		if (enable && Owner != Owner.Player || pause)
			return;
		UpgrGold.renderer.enabled = enable;
		UpgrGold.gameObject.GetComponent<tk2dSimpleButton>().ButtonDisable = !enable;
	}

	public virtual void UpgradeTroops() {
		if (pause) return;
		int price = TroopsUpgrPrice;
		TroopsLevel++;
//		Debug.Log(TroopsGenerationSpeed);
		OnResult(this, new ru.appforge.logic.LogicEventArgs("upgraded", Owner, price));
	}

	public virtual void UpgradeGold() {
		if (pause) return;
		int price = GoldUpgrPrice;
		GoldLevel++;
		OnResult(this, new ru.appforge.logic.LogicEventArgs("upgraded", Owner, price));
	}

	public void OnAcademyUpgraded(object sender, LogicEventArgs args) {
		if (args.EventName == "TGS" && ((Academy)sender).Owner == this.Owner) {
			tgs_speed_mod = (int)args.Args[0];
//			Debug.Log ("Got TGS! " + tgs_speed_mod + " " + this);
			TGSNumber.text = (TroopsGenerationSpeed).ToString() + "/s";
			TGSNumber.Commit();
		}
		if (args.EventName == "GGS" && ((Academy)sender).Owner == this.Owner) {
			ggs_speed_mod = (int)args.Args[0];
//			Debug.Log ("Got GGS! " + ggs_speed_mod + " " + this);
			GGSNumber.text = (GoldGenerationSpeed).ToString() + "/s";
			GGSNumber.Commit();
		}
	}

//	protected virtual float GetGGS(int levelAcademy) {
//
//	}
//
//	protected virtual float GetTGS(int levelAcademy) {
//		
//	}

//	private float GetCoef(int lvlAcad, int[] coefs

	protected virtual void Awake() {
		GoldLevel = 1;
		TroopsLevel = 1;
		IsSelected = false;
		
		UpgrTroops.gameObject.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnUpgrTroops;
		UpgrGold.gameObject.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnUpgrGold;
		
		SetUpgrTroopsButton(false);
		SetUpgrGoldButton(false);

		Upgrade.renderer.enabled = false;

		DefenceAnim.renderer.enabled = false;
		OwnerIndicator.sprite = owners[2];

	}

	protected virtual void Start() {

	}

	protected virtual void HandleOnUpgrGold (object sender, System.EventArgs e)
	{
		if (Owner != Owner.Player)
			return;
		UpgradeGold();
	}

	protected void UpgradAnim() {
//		if (isInit) {isInit = false; return; }
//		Debug.Log( this.name + " " + this.Owner);
		Upgrade.renderer.enabled = true;
		Upgrade.Play();
		Upgrade.AnimationCompleted = (tk2dSpriteAnimator a, tk2dSpriteAnimationClip c) => {
			Upgrade.StopAndResetFrame();
			Upgrade.renderer.enabled = false;
		};
	}

	void HandleOnUpgrTroops (object sender, System.EventArgs e)
	{
		if (Owner != Owner.Player)
			return;
		UpgradeTroops();
	}

	protected virtual void Update() {
		if (pause)
			return;
		if (this.Owner != Owner.Neutral) {
			troopsTimer += Time.deltaTime;

			if (troopsTimer * TroopsGenerationSpeed >= 1f && TroopsAmount < 999) {
//				Debug.Log((TroopsGenerationSpeed + tgs_speed_mod) + " " + this.gameObject.name);
				troopsTimer = 0;
				TroopsAmount++;
			}
		}
	}
}
