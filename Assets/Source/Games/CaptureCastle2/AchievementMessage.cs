﻿using UnityEngine;
using System.Collections;

public class AchievementMessage : MonoBehaviour {

	public tk2dTextMesh Achiev;
	public tk2dTextMesh Descr;

	private SpriteRenderer me;
	private float ShowTimer = 1.5f;
	private bool isSHowed = false;

	// Use this for initialization
	void Start () {
		me = this.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if(isSHowed) {
			ShowTimer -= Time.deltaTime;
			if (ShowTimer <= 0)
				Hide();
		}
	}

	public void Show (string achie, string descr) {
		if (this) {
			this.GetComponent<SpriteRenderer>().renderer.enabled = true;
			Achiev.renderer.enabled = true;
			Descr.renderer.enabled = true;

			Achiev.text = achie;
			Achiev.Commit();
			Descr.text = descr;
			Descr.Commit();

			isSHowed = true;
			ShowTimer = 1.5f;
		}
	}

	public void Hide() {
		this.GetComponent<SpriteRenderer>().renderer.enabled = false;
		Achiev.renderer.enabled = false;
		Descr.renderer.enabled = false;

		isSHowed = false;
	}
}
