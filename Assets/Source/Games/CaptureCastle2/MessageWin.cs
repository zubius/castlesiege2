﻿using UnityEngine;
using System.Collections;

public class MessageWin : GameComponentBase, IGameComponent {

	public tk2dTextMesh MessageText;
	public tk2dTextMesh TotalScoreAmountText;
	public tk2dTextMesh Rank;
	public tk2dTextMesh RankText;
	public tk2dTextMesh TotalScoreText;
	public tk2dTextMesh BlaBlaText;

	// Use this for initialization
	void Start () {
		Hide();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void Hide (params object[] args)
	{
		Renderer[] o = GetComponentsInChildren<Renderer>();
		foreach (var t in o) {
			t.enabled = false;
		}
		
		transform.position = new Vector3(1000f, 1000f, -2f);
		base.Hide(args);
	}
	
	public override void Show (params object[] args)
	{
		Renderer[] o = GetComponentsInChildren<Renderer>();
		foreach (var t in o) {
			t.enabled = true;
		}
		transform.position = new Vector3(0f, 0f, -2f);
		base.Show (args);
	}
}
