﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class Academy : BuildingBase {


	public event DLogicEvent OnAcadUpgr;
	public event DLogicEvent OnAcadNeutral;
	public event DLogicEvent OnAcadGotAchie;
	public SpriteRenderer UpgrMoveSpeed;
	public SpriteRenderer UpgrTroopsGenSpeed;
	public SpriteRenderer UpgrGoldGenSpeed;
	public SpriteRenderer BonusTypeSprite;

	public Sprite[] bonusTypeSprites;

	private int level = 1;
	public BonusType bonus;
	private bool isBonusSet = false;
	public int tgs_level = 1;
	public int ggs_level = 1;
	private float suicideTimer = 1f;

	public override int GoldLevel {
		get {
			return base.GoldLevel;
		}
		set {
			if (value <= GoldUpgrCostSteps.Length) {
				goldLevel = value;
				GoldLevelNumber.text = goldLevel.ToString();
				GoldLevelNumber.Commit();
				if (goldLevel > 1) {
					UpgradAnim();
					this.audio.clip = UpgrGoldSound;
					this.audio.Play();
				}
				if (levels.Length > 0) {
					this.GetComponent<SpriteRenderer>().sprite = levels[goldLevel-1];
				}
			}
		}
	}

	public override void PlayPlagueAnim ()
	{
		DefenceAnim.transform.localPosition = new Vector3(0, 0, -0.25f);
		DefenceAnim.Sprite.scale = new Vector3(1.3f, 1.3f, 1f);
		base.PlayPlagueAnim ();
	}

	public override void PlayDefenceAnim (string anim)
	{
		DefenceAnim.transform.localPosition = new Vector3(0, -0.2943658f, -0.25f);
		DefenceAnim.Sprite.scale = new Vector3(1f, 1f, 1f);
		base.PlayDefenceAnim (anim);
	}

	public override float TroopsGenerationSpeed {
		get {
			return TroopsUpgrSteps[tgs_level];
		}
	}

	public override float GoldGenerationSpeed {
		get {
			return GoldUpgrSteps[ggs_level];
		}
	}

	public float TroopsMovingSpeedMulty {
		get {
			return (float)(1f + ((float)Level - 1f) / 8f);//Level
		}
	}

	public override Owner Owner {
		get {
			return base.Owner;
		}
		set {
			if (value != Owner.Neutral && Bonus != BonusType.None) {
				if (value == Owner.Enemy)
					HideUpgrBtns();
				ResetAcademyBonusForPlayer(bonus);
				base.Owner = value;
				if (base.Owner == Owner.Player) suicideTimer = 1f;
				else if (base.Owner == Owner.Enemy) suicideTimer = 2f;
				Level = Level;
			} else if (value == Owner.Neutral && Bonus != BonusType.None) {
				HideUpgrBtns();
				ResetAcademyBonusForPlayer(bonus);
				base.Owner = value;
			} else {
				base.Owner = value;
				if (base.Owner == Owner.Player) suicideTimer = 1f;
				else if (base.Owner == Owner.Enemy) suicideTimer = 2f;
			}
		}
	}

	private void ResetAcademyBonusForPlayer(BonusType b) {
		switch (b) {
		case BonusType.GoldGenSpeed:
			ggs_level = 1;
			OnAcadUpgr(this, new LogicEventArgs("GGS", 1));
			break;
		case BonusType.TroopsGenSpeed:
			tgs_level = 1;
			OnAcadUpgr(this, new LogicEventArgs("TGS", 1));
			break;
		case BonusType.TroopsMoveSpeed:
			OnAcadUpgr(this, new LogicEventArgs("TMS", 1f));
			break;
		}
	}

	public void SetBonus ()
	{
		if (pause) return;
		if (Bonus == BonusType.None) {
			SetUpgrTMSButton(true);
			SetUpgrGGSButton(true);
			SetUpgrTGSButton(true);
		}
	}

	public int Level {
		get {
			return level;
		}
		set {
			level = value;
			switch (Bonus) {
			case BonusType.GoldGenSpeed:
				ggs_level = level;
				OnAcadUpgr(this, new LogicEventArgs("GGS", level));
				break;
			case BonusType.TroopsGenSpeed:
				tgs_level = level;
				OnAcadUpgr(this, new LogicEventArgs("TGS", level));
				break;
			case BonusType.TroopsMoveSpeed:
				Debug.Log(TroopsMovingSpeedMulty);
				OnAcadUpgr(this, new LogicEventArgs("TMS", TroopsMovingSpeedMulty));
				break;
			default: break;
			}
		}
	}

	public virtual void SetUpgrTMSButton (bool enable) {
		if (enable && Owner != Owner.Player || pause)
			return;
		UpgrMoveSpeed.renderer.enabled = enable;
		UpgrMoveSpeed.gameObject.GetComponent<tk2dSimpleButton>().ButtonDisable = !enable;
	}

	public virtual void SetUpgrGGSButton (bool enable) {
		if (enable && Owner != Owner.Player || pause)
			return;
		UpgrGoldGenSpeed.renderer.enabled = enable;
		UpgrGoldGenSpeed.gameObject.GetComponent<tk2dSimpleButton>().ButtonDisable = !enable;
	}

	public virtual void SetUpgrTGSButton (bool enable) {
		if (enable && Owner != Owner.Player || pause)
			return;
		UpgrTroopsGenSpeed.renderer.enabled = enable;
		UpgrTroopsGenSpeed.gameObject.GetComponent<tk2dSimpleButton>().ButtonDisable = !enable;
	}

	public BonusType Bonus {
		get {
			return bonus;
		}
		set {
			if (value != BonusType.None && !isBonusSet) {
				bonus = value;
				isBonusSet = true;
				Level = Level;

				switch (bonus) {
				case BonusType.GoldGenSpeed:
					BonusTypeSprite.sprite = bonusTypeSprites[0];
					UpgrGold.sprite = UpgrGoldGenSpeed.sprite;
					UpgrGold.transform.position = UpgrGoldGenSpeed.transform.position;
					break;
				case BonusType.TroopsGenSpeed:
					BonusTypeSprite.sprite = bonusTypeSprites[1];
					UpgrGold.sprite = UpgrTroopsGenSpeed.sprite;
					UpgrGold.transform.position = UpgrTroopsGenSpeed.transform.position;
					break;
				case BonusType.TroopsMoveSpeed:
					BonusTypeSprite.sprite = bonusTypeSprites[2];
					UpgrGold.sprite = UpgrMoveSpeed.sprite;
					UpgrGold.transform.position = UpgrMoveSpeed.transform.position;
					break;
				}
				BonusTypeSprite.renderer.enabled = true;
			}
		}
	}

	protected override void Update ()
	{
		if (pause)
			return;
		if (this.Owner != Owner.Neutral) {
			troopsTimer += Time.deltaTime;
			
			if (troopsTimer  >= suicideTimer) {
				troopsTimer = 0;
				if (this.TroopsAmount > 0)
					this.TroopsAmount--;
				if (TroopsAmount == 0) {
					OnAcadNeutral(this, new LogicEventArgs("", Owner.Neutral, this.Owner));
					this.Owner = Owner.Neutral;
				}
			}
		}
	}

	protected override void Awake ()
	{
		HideUpgrBtns();

		UpgrGoldGenSpeed.gameObject.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnClickGGS;
		UpgrTroopsGenSpeed.gameObject.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnClickTGS;
		UpgrMoveSpeed.gameObject.GetComponent<tk2dSimpleButton>().OnClickEnable += HandleOnClickTMS;

		GoldUpgrCostSteps = new int[] {0, 10, 20, 30, 40, 60};

		TroopsLevelNumber.renderer.enabled = false;
		TGSNumber.renderer.enabled = false;
		GGSNumber.renderer.enabled = false;

		BonusTypeSprite.renderer.enabled = false;

		base.Awake ();
	}

	public override void UpgradeGold ()
	{
		if (pause) return;
		Level++;
		base.UpgradeGold ();
	}

	void HandleOnClickTMS (object sender, System.EventArgs e)
	{
		if (pause) return;
		HideUpgrBtns();
		Bonus = BonusType.TroopsMoveSpeed;
		if (!Achvmnts.DiligSchol.Earned) {
			string s = PlayerPrefs.GetString("dilig_scholar", "");
			if (!s.Contains("m")) {
				s += "m";
				PlayerPrefs.SetString("dilig_scholar", s);
				PlayerPrefs.Save();
				if (s.Length == 3) OnAcadGotAchie(this, new LogicEventArgs());
			}
		}
	}

	void HandleOnClickTGS (object sender, System.EventArgs e)
	{
		if (pause) return;
		HideUpgrBtns();
		Bonus = BonusType.TroopsGenSpeed;
		if (!Achvmnts.DiligSchol.Earned) {
			string s = PlayerPrefs.GetString("dilig_scholar", "");
			if (!s.Contains("t")) {
				s += "t";
				PlayerPrefs.SetString("dilig_scholar", s);
				PlayerPrefs.Save();
				if (s.Length == 3) OnAcadGotAchie(this, new LogicEventArgs());
			}
		}
	}

	void HandleOnClickGGS (object sender, System.EventArgs e)
	{
		HideUpgrBtns();
		Bonus = BonusType.GoldGenSpeed;
		if (!Achvmnts.DiligSchol.Earned) {
			string s = PlayerPrefs.GetString("dilig_scholar", "");
			if (!s.Contains("g")) {
				s += "g";
				PlayerPrefs.SetString("dilig_scholar", s);
				PlayerPrefs.Save();
				if (s.Length == 3) OnAcadGotAchie(this, new LogicEventArgs());
			}
		}
	}

	private void HideUpgrBtns() {
		SetUpgrTMSButton(false);
		SetUpgrGGSButton(false);
		SetUpgrTGSButton(false);
	}

	private float[] TroopsUpgrSteps = {0, 0f, 1.07f, 1.25f, 1.42f, 1.66f, 2f};
	private float[] GoldUpgrSteps = {0, 1f, 1.11f, 1.25f, 1.42f, 1.66f, 2f};
	
	public override void SetUpgrTroopsButton (bool enable)	{ base.SetUpgrTroopsButton(false);	}
	public override void SetUpgrGoldButton (bool enable)
	{
		if (enable && Owner != Owner.Player || pause)
			return;
		UpgrGold.renderer.enabled = enable && Bonus != BonusType.None;
		UpgrGold.gameObject.GetComponent<tk2dSimpleButton>().Enabled = enable && Bonus != BonusType.None;
	}
	public override void UpgradeTroops () { }

	public enum BonusType {
		GoldGenSpeed,
		TroopsGenSpeed,
		TroopsMoveSpeed,
		None
	}
}

