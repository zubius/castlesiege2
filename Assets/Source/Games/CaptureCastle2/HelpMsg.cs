﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HelpMsg {

	public static Dictionary<int, Queue<string>> helps;
	private static Queue<string> level1 = new Queue<string>();
	private static Queue<string> level2 = new Queue<string>();
	private static Queue<string> level5 = new Queue<string>();
	private static Queue<string> level9 = new Queue<string>();
	private static Queue<string> level13 = new Queue<string>();
	private static Queue<string> level19 = new Queue<string>();


	public static void Init() {
		level1 = new Queue<string>();
		level2 = new Queue<string>();
		level5 = new Queue<string>();
		level9 = new Queue<string>();
		level13 = new Queue<string>();
		level19 = new Queue<string>();

		level1.Enqueue("Choose your Castle\nusing tap");
		level1.Enqueue("To deselect tap your Castle again or empty space on a stage");
		level1.Enqueue("Send your troops to the\nenemy's castle tapping on it");
		level1.Enqueue("Half of the troops stationed\nin the castle will go on the attack");
		level1.Enqueue("Your mission: defeat the enemy!!!");
		
		level2.Enqueue("Upgrade is available when you have\n the necessary amount of gold in your treasury\n" +
		               "Villages provide more gold than troops\n" +
		               "Castles provide more troops than gold");
		level2.Enqueue("Speeds up the troops recruitment");
		level2.Enqueue("Accelerates the growth of gold in the treasury");
		level2.Enqueue("The greater the number of soldiers in the\nsquad, the slower the movement speed of this\nsquad");
		
		level5.Enqueue("This is Academy. When it is under your control,\n" +
		               "you can get some important cnhancements.\n" +
		               "But remember that the Academy\n" +
		               "slowly reduce the amount of troops stationed there");
		level5.Enqueue("Increase the moveming speed of your troops");
		level5.Enqueue("Speed up the troops recruitment from all your buildings");
		level5.Enqueue("Accelerates the flow of gold coming from all your buildings");
		
		level9.Enqueue("This is the Fort.\n" +
		               "You will receive reinforcements every\n" +
		               "time your troops hold\n" +
		               "out there for 30 seconds.");

		level13.Enqueue("Pestilence halves the population in ALL enemy castles and villages");
		level13.Enqueue("Armageddon destroys all troops outside the buildings");

		level19.Enqueue("This is the Gold Mine.\n" +
		               "You will receive gold every\n" +
		               "time your troops hold\n" +
		               "out there for 30 seconds.");

		helps = new Dictionary<int, Queue<string>>() {
			{ 1, level1 },
			{ 2, level2 },
			{ 5, level5 },
			{ 9, level9 },
			{ 13, level13 },
			{ 19, level19 }
		};
	}
}
