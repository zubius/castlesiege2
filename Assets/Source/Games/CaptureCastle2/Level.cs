﻿using UnityEngine;
using System.Collections;

public class Level : GameComponentBase, IGameComponent {

	public int N;
	public GameObject EnemyCastle;
	public GameObject PlayerCastle;
	public GameObject Fort;
	public GameObject GoldMine;
	public GameObject PlayerFortSource;
	public GameObject EnemyFortSource;
	public GameObject Academy;
	public GameObject[] Villages;
	public HelpUI help;
}
