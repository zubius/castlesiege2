﻿using UnityEngine;
using System.Collections;

public class FortTimer : MonoBehaviour {

	public tk2dTextMesh timer;
	public SpriteRenderer flag;
	public Sprite[] flags;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Show(Transform pos, Owner own) {
		if (own == Owner.Player)
			flag.sprite = flags[0];
		else if (own == Owner.Enemy)
			flag.sprite = flags[1];
		this.transform.position = pos.position;
	}

	public void Hide() {
		this.transform.position = new Vector3(1000f, 1000f, 0f);
	}

	public void UpdateTime(int time) {
		timer.text = time.ToString();
		timer.Commit();
	}
}
