﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Village : BuildingBase {

	protected override void Start() {
//		TroopsAmount = 5;
	}

	public override void PlayPlagueAnim ()
	{
		DefenceAnim.transform.localPosition = new Vector3(0, 0.2994235f, -0.5f);
		DefenceAnim.Sprite.scale = new Vector3(1.3f, 1.3f, 1f);
		base.PlayPlagueAnim ();
	}

	public override void PlayDefenceAnim (string anim)
	{
		DefenceAnim.transform.localPosition = new Vector3(0, 0, -0.5f);
		DefenceAnim.Sprite.scale = new Vector3(1f, 1f, 1f);
		base.PlayDefenceAnim (anim);
	}

	public override float TroopsGenerationSpeed {
		get {
			return TGS[TroopsLevel][tgs_speed_mod-1];
//			return TroopsUpgrSteps[TroopsLevel-1];
		}
	}
	
	public override float GoldGenerationSpeed {
		get {
			return GGS[GoldLevel][ggs_speed_mod-1];
//			return GoldUpgrSteps[GoldLevel-1];
		}
	}

	protected override void Awake ()
	{
		GGS = new Dictionary<int, float[]>() {
			{ 1, new float[] {1f, 		1.07f, 	1.15f, 	1.25f, 	1.36f, 	1.5f} 	},
			{ 2, new float[] {1.11f, 	1.2f, 	1.29f, 	1.4f, 	1.5f,	1.76f} 	},
			{ 3, new float[] {1.25f,	1.36f,	1.5f,	1.66f,	1.87f,	2.14f} },
			{ 4, new float[] {1.42f,	1.57f,	1.77f,	2f,		2.3f,	2.72f} },
			{ 5, new float[] {1.66f,	1.87f,	2.14f,	2.5f,	3f,		3.75f} },
			{ 6, new float[] {2f,		2.68f,	3.45f,	4.3f,	5.47f,	6f} }
		};
		
		TGS = new Dictionary<int, float[]>() {
			{ 1, new float[] {0.6f,		0.62f,	0.65f,	0.68f,	0.71f,	0.75f} },
			{ 2, new float[] {0.66f,	0.69f,	0.73f,	0.76f,	0.81f,	0.85f} },
			{ 3, new float[] {0.75f,	0.78f,	0.83f,	0.88f,	0.93f,	1f} },
			{ 4, new float[] {0.85f,	0.9f,	0.96f,	1.03f,	1.11f,	0.2f} },
			{ 5, new float[] {1f, 		1.07f, 	1.15f, 	1.25f, 	1.36f, 	1.5f} },
			{ 6, new float[] {1.2f,		1.3f,	1.42f,	1.57f,	1.76f,	2f} }
		};
		base.Awake ();
	}

	private float[] TroopsUpgrSteps = {0.6f, 0.66f, 0.75f, 0.85f, 1f, 1.2f};
	private float[] GoldUpgrSteps = {1f, 1.11f, 1.25f, 1.42f, 1.66f, 2f};
}
