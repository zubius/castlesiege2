﻿using UnityEngine;
using System.Collections;
using com.shephertz.app42.paas.sdk.csharp;    
using com.shephertz.app42.paas.sdk.csharp.achievement;
using System.Collections.Generic;
using ru.appforge.logic;

public class AchievementController : GameComponentBase, IGameComponent {

	public override event ru.appforge.logic.DLogicEvent OnResult;

	public string userName;
	private string achievementName;
	public string gameName = "";
	private string description;
	AchievementService achievementService;

	public string AchievementName {
		get {
			return achievementName;
		}
		set {
			achievementName = value;
		}
	}

	public string Description {
		get {
			return description;
		}
		set {
			description = value;
		}
	}

	void Start () {
		App42API.Initialize("26813493bb846358b7a744798b6d3ee3dc822ab7f8ef80822affc13830e8cbcf","06615c8153af8772f63d6699ac1d3e76659aea67dc4e3a7950369bd35068170c");  
		achievementService = App42API.BuildAchievementService();
		App42Log.SetDebug(true);
		userName = PlayerPrefs.GetString ("player_name", "");
	}

	public void EarnAchievment(params object[] args) {
		string a = (string)args[0];
		Debug.Log(a);
		if (userName.Length == 0) userName = PlayerPrefs.GetString ("player_name", "");
		if (userName.Length > 0)
			achievementService.EarnAchievement(userName, Achvmnts.Achvs[a].Name, gameName, Achvmnts.Achvs[a].Description, 
			                                   new UnityCallBack(() => {OnResult(this, new LogicEventArgs("EventAchieEarned", Achvmnts.Achvs[a]));}));
	}

	class UnityCallBack : App42CallBack  
	{  
		private System.Action callBack;

		public UnityCallBack(System.Action callBack = null) {
			this.callBack = callBack;
		}

		public void OnSuccess(object response)  
		{  
			Achievement achievement = (Achievement) response;     
			App42Log.Console("userName is :" + achievement.GetUserName());  
			App42Log.Console("gameName is :" + achievement.GetGameName());  
			App42Log.Console("achievementName is :" + achievement.GetName());  
			App42Log.Console("description is :" + achievement.GetDescription());  
			App42Log.Console("achievedOn is :" + achievement.GetAchievedOn());
			
			PlayerPrefs.SetString(achievement.GetName(), achievement.GetAchievedOn().ToString());
			PlayerPrefs.Save();
			Achvmnts.Achvs[achievement.GetName()].Earned = true;
			Achvmnts.Achvs[achievement.GetName()].OnAchieved = achievement.GetAchievedOn();
			if (this.callBack != null) callBack();
		}  
		public void OnException(System.Exception e)  
		{  
			App42Log.Console("Exception : " + e);  
		}  
	}  
}
