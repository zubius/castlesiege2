﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player {

	private float tms_mod = 1f;

	public Owner Owner { get; set; }
	public int Gold { get; set; }
	public float GoldGenerationSpeed { get; set; }
	public float goldTimer { get; set; }
	public List<BuildingBase> Buildings { get; set; }
	public List<Troops> Troops { get; set; }

	public float Tms_mod {
		get {
			return tms_mod;
		}
		set {
			tms_mod = value;
		}
	}
}
