﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ru.appforge.logic;

public class AchievementsMngmnt : GameComponentBase, IGameComponent {

	public override event DLogicEvent OnResult;

	public GameObject selection;
	public GameObject ap;
	public tk2dSimpleButton back;
	public tk2dSimpleButton frwrd;
	List<Achiev> achvs = new List<Achiev>();
	List<GameObject> aps = new List<GameObject>();
	int pages;
	int currpage = 1;
	float stepx = 0;

	// Use this for initialization
	IEnumerator Start () {
		achvs = Achvmnts.Achvs.Values.ToList();
		achvs = achvs.OrderByDescending(a => a.OnAchieved).ToList();

		float stepy = 0;
		stepx = selection.renderer.bounds.size.x;

		for (int i = 1; i < 4; i++) {
			GameObject s = Instantiate(selection) as GameObject;
			stepy = s.renderer.bounds.size.y;
			s.transform.position = new Vector3(0, s.transform.position.y - (s.renderer.bounds.size.y * i * 2), s.transform.position.z);
			s.transform.parent = this.transform;
			s = null;
		}
		int dx = 0;
		int dy = 0;
		for (int i = 0; i < achvs.Count; i++) {
			if (dy > 7) {
				dx++;
				dy = 0;
			}
			GameObject a = Instantiate(ap) as GameObject;
			a.transform.position = new Vector3(0 + (selection.renderer.bounds.size.x * dx), 
			                                   selection.transform.position.y - (stepy * dy), a.transform.position.z);
			dy++;
			a.transform.parent = this.transform;
			a.GetComponent<AchieProto>().Fill(achvs[i]);
			a.GetComponent<TransformMovementPosition>().OnResult += (obj) => { 
				back.Enabled = true; 
				frwrd.Enabled = (currpage < pages); 
			};
			aps.Add(a);
			a = null;
		}

		pages = Mathf.CeilToInt((float)aps.Count/8);

		ButtonsBase buttons = GameObject.Find("Buttons").GetComponent<ButtonsBase>();
		buttons.buttons = new List<tk2dSimpleButton>();
		buttons.buttons.Add(back);
		buttons.buttons.Add(frwrd);
		buttons.Start();

		buttons = null;

		yield return new WaitForEndOfFrame();
		if (OnResult != null)
			OnResult(this, new LogicEventArgs("EventPages", pages));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void NextPage(params object[] args) {
		currpage = (int)args[0];
		if (frwrd && currpage == pages) {
			frwrd.renderer.enabled = false;
			back.Enabled = false; 
			frwrd.Enabled = false;
		}

		foreach (var a in aps) {
			if (a) {
				a.GetComponent<TransformMovementPosition>().Move(new Vector3(a.transform.position.x - stepx, a.transform.position.y, a.transform.position.z), 0.3f);
			}
		}
	}

	public void PrevPage(params object[] args) {
		currpage = (int)args[0];
		if (frwrd && !frwrd.renderer.enabled && currpage < pages) {
			frwrd.renderer.enabled = true;
			back.Enabled = false; 
			frwrd.Enabled = false;
		}

		foreach (var a in aps) {
			if (a) {
				a.GetComponent<TransformMovementPosition>().Move(new Vector3(a.transform.position.x + stepx, a.transform.position.y, a.transform.position.z), 0.3f);
			}
		}
	}

	void OnDestroy() {
		for (int i = 0; i < aps.Count; i++) {
			Destroy(aps[i]);
			aps[i] = null;
		}
		
		Resources.UnloadUnusedAssets();
	}
}
