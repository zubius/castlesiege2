using UnityEngine;
using System.Collections;
using System.Linq;

public class LevelSelector : GameComponentBase, IGameComponent {

	public override event ru.appforge.logic.DLogicEvent OnResult;

	public GameObject LevelIconProto;
	public Transform LevelsIconFolder;
	public Transform LFS; //LevelFirstPosition
	public NoticeName nm;
	public tk2dSimpleButton back;
	public tk2dSimpleButton fwd;

	private GameObject[] pagesObj;

	private ButtonsBase buttons;

	private GameObject[] levels = new GameObject[24];
	private float deltax = 2.3f;
	private float deltay = -2.2f;
	private bool isfirst = true;
	private string[] states = new string[] {};
	private Vector3 pageDelta = new Vector3(15f, 0, 0);
	int currpage = 1;
	int pages = 2;
	bool once = false;

	// Use this for initialization
	void Awake () {
		levels = new GameObject[24];
		InitLevelsIcons(LevelIconProto);
	}

	IEnumerator Start () {		
		yield return new WaitForEndOfFrame();

		if (OnResult != null)
			OnResult(this, new ru.appforge.logic.LogicEventArgs("EventLevelSelectorInit"));

		ButtonsBase buttons = GameObject.Find("Buttons").GetComponent<ButtonsBase>();
		buttons.buttons = new System.Collections.Generic.List<tk2dSimpleButton>();
		buttons.buttons.Add(back);
		buttons.buttons.Add(fwd);
		buttons.Start();
	}

	public void SetNotice(params object[] args) {
		nm.Show();
	}

	public void SetUnlockLevels(params object[] args) {
		if (once) return;
		once = true;
		int l = (int)args[0];
		int[] d = (int[])args[1];
		int[][] dd = new int[2][];
		dd[0] = d.Take(12).ToArray();
		dd[1] = d.Skip(12).Take(12).ToArray();
		for (int i = 0; i < l; i++) {
			if (levels[i]) {
				LevelBtnController lbc = levels[i].GetComponent<LevelBtnController>();
				lbc.Lock.renderer.enabled = false;
				lbc.Stars.renderer.enabled = true;
				if (d[i] == 0) {
					lbc.Star1.SetSprite("_0000_L_star_2");
					lbc.Star1.renderer.enabled = true;
					lbc.Star2.renderer.enabled = false;
					lbc.Star3.renderer.enabled = false;
				} else if (d[i] == 1) {
					lbc.Star1.SetSprite("_0001_L_star_3");
					lbc.Star1.renderer.enabled = true;
					lbc.Star2.SetSprite("_0000_L_star_2");
					lbc.Star2.renderer.enabled = true;
					lbc.Star3.renderer.enabled = false;
				} else if (d[i] == 2) {
					lbc.Star1.SetSprite("_0001_L_star_3");
					lbc.Star1.renderer.enabled = true;
					lbc.Star2.SetSprite("_0001_L_star_3");
					lbc.Star2.renderer.enabled = true;
					lbc.Star3.SetSprite("_0000_L_star_2");
					lbc.Star3.renderer.enabled = true;
				} else if (d[i] == 3) {
					lbc.Star1.SetSprite("_0001_L_star_3");
					lbc.Star1.renderer.enabled = true;
					lbc.Star2.SetSprite("_0001_L_star_3");
					lbc.Star2.renderer.enabled = true;
					lbc.Star3.SetSprite("_0001_L_star_3");
					lbc.Star3.renderer.enabled = true;
				}

//				if (d.Sum() < 12) {
//					lbc.Star2.renderer.enabled = false;
//					lbc.Star3.renderer.enabled = false;
//				} else if (d.Sum() < 24) {
//					lbc.Star3.renderer.enabled = false;
//				}
				if (dd[i/12].Sum() < 12) {
					lbc.Star2.renderer.enabled = false;
					lbc.Star3.renderer.enabled = false;
				} else if (dd[i/12].Sum() < 24) {
					lbc.Star3.renderer.enabled = false;
				}
			}
		}
	}

	public void NextPage(params object[] args) {
		if (++currpage > pages) return;
		if (fwd && currpage == pages) {
			fwd.renderer.enabled = false;
		}

		pagesObj.ToList().ForEach(p => { if (p != null) p.transform.position -= pageDelta; });
	}
	
	public void PrevPage(params object[] args) {
		if (--currpage > pages) return;
		if (fwd && !fwd.renderer.enabled && currpage < pages) {
			fwd.renderer.enabled = true;
		}
		
		pagesObj.ToList().ForEach(p => { if (p != null) p.transform.position += pageDelta; });
	}

	private void InitLevelsIcons(GameObject proto) {
		float dx = 0;
		float dy = 0;
		pagesObj = new GameObject[levels.Length/12];
		for (int i = 0; i < pagesObj.Length; i++) {
			pagesObj[i] = new GameObject();
			pagesObj[i].transform.parent = LevelsIconFolder;
			pagesObj[i].transform.position += pageDelta * i;
			pagesObj[i].name = "P"+(i+1).ToString();
		}

		buttons = GameObject.Find("Buttons").GetComponent<ButtonsBase>();
		buttons.buttons = new System.Collections.Generic.List<tk2dSimpleButton>();
		for (int i = 0; i < levels.Length; i++) {
			GameObject l = Instantiate(proto) as GameObject;
			l.name = "Level"+(i+1);
			l.transform.parent = pagesObj[i/12].transform;
			l.GetComponentInChildren<tk2dTextMesh>().text = (i+1).ToString();
			l.GetComponentInChildren<tk2dTextMesh>().Commit();
			buttons.buttons.Add(l.GetComponent<tk2dSimpleButton>());

			l.transform.localPosition = new Vector3(LFS.position.x + deltax * dx, LFS.position.y + deltay * dy, LFS.position.z);
			dx++;
			if (dx == 4) {
				dx = 0;
				dy++;
				if (dy == 3)
					dy = 0;
			}

			levels[i] = l;
			l = null;
		}

		buttons.Start();

		buttons = null;
	}

	void OnDestroy() {
		for (int i = 0; i < levels.Length; i++) {
			Destroy(levels[i]);
			levels[i] = null;
		}

		for (int i = 0; i < pagesObj.Length; i++) {
			Destroy(pagesObj[i]);
			pagesObj[i] = null;
		}
		
		Resources.UnloadUnusedAssets();
	}
}
