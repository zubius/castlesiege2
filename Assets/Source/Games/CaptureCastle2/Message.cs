﻿using UnityEngine;
using System.Collections;

public class Message : GameComponentBase, IGameComponent {

	public tk2dTextMesh MessageText;
	public tk2dTextMesh TotalScoreAmountText;
	public tk2dTextMesh LevelScoreAmountText;
	public tk2dTextMesh TotalScoreText;
	public tk2dTextMesh LevelScoreText;

	// Use this for initialization
	void Start () {
		TotalScoreText.text = "Total Score:";
		TotalScoreText.Commit();
		LevelScoreText.text = "Level Score:";
		LevelScoreText.Commit();

		Hide();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void Hide (params object[] args)
	{
		Renderer[] o = GetComponentsInChildren<Renderer>();
		foreach (var t in o) {
			t.enabled = false;
		}

		transform.position = new Vector3(1000f, 1000f, -2f);
		base.Hide(args);
	}

	public override void Show (params object[] args)
	{
		Renderer[] o = GetComponentsInChildren<Renderer>();
		foreach (var t in o) {
			t.enabled = true;
		}
		transform.position = new Vector3(0f, 0f, -2f);
		base.Show (args);
		HideLevelScore();
		HideTotalScore();
	}

	public void SetTotalScore(int score) {
		TotalScoreText.renderer.enabled = true;
		TotalScoreAmountText.renderer.enabled = true;
		TotalScoreAmountText.text = score.ToString();
		TotalScoreAmountText.Commit();
	}

	public void SetLevelScore(int score) {
		LevelScoreText.renderer.enabled = true;
		LevelScoreAmountText.renderer.enabled = true;
		LevelScoreAmountText.text = score.ToString();
		LevelScoreAmountText.Commit();
	}

	private void HideTotalScore() {
		LevelScoreText.renderer.enabled = false;
		LevelScoreAmountText.renderer.enabled = false;
	}
	
	private void HideLevelScore() {
		TotalScoreText.renderer.enabled = false;
		TotalScoreAmountText.renderer.enabled = false;
	}
}
