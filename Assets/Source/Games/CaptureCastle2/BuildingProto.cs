﻿using UnityEngine;
using System.Collections;

public class BuildingProto : MonoBehaviour {

	public Owner Owner;
	public int TroopsAmount;
	public int TroopsLvl = 1;
	public int GoldLvl = 1;
}
