﻿using UnityEngine;
using System.Collections;

public class NoticeName : GameComponentBase, IGameComponent {

	public tk2dTextMesh notice;

	// Use this for initialization
	void Start () {
		Hide ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void Show (params object[] args)
	{
		if (notice) {
			notice.text = "Please enter your name to save your scores\nin leaderboard and earn achievements";
			notice.Commit();
		}
		if (this)
			base.Show (args);
	}
}
