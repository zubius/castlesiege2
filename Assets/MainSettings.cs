using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.Text;

public class MainSettings : MonoBehaviour
{
	#region global data
	
	/// <summary> 1
	/// Текущая версия продукта
	/// </summary>
	public const int PRODUCT_VERSION = 359;
	
	/// <summary>
	/// Текущая версия бандлов
	/// </summary>
	public const int BUNDLE_VERSION = 359;
	
	/// <summary>
	/// Хост
	/// </summary>
	public const string host = @"http://mc.inbet.cc:9002/";
//	public const string host = @"http://192.168.88.86:8000/";
	
	/// <summary>
	/// Минимальное требуемое свободное место на устройстве
	/// </summary>
	public const decimal minFreeSpace = 104857600; // 100 MB;
	
	/// <summary>
	/// Главный ответ Вселенной
	/// </summary>
	public const string salt = "42";
	
	/// <summary>
	/// Хост для бандлов
	/// </summary>
	//  public const string bundlesHost = "http://mc.casino7.cc/bundles/";
	public const string bundlesHostMain = "http://bucket-bundle.s3.amazonaws.com/";
	public const string bundlesHostLocal = "http://192.168.0.250:80/";
	
	public static string bundlesAssetsPrefix = "mc_p_" + BUNDLE_VERSION + "_";
	public static double coefficient = 100;
	public static string DeviceId {	get { return deviceId; } }
	public static bool MainAutoMode { get { return mAutoMode; } set { mAutoMode = value; } }
	private static bool mAutoMode = false;
	private static string deviceId = "";
	public static string BillingKey { get; set; }
	#endregion
	
	#region local data
	
	public static string gameProducer = "";
	public string nameGame = "";
	public int countFreeGame = 15;
	public bool ChangeCamera = false;
	public float sizeCamera = 1.0f;
	
	public static int SendTimeout = 2500;
	public static int ReceiveTimeout = 2000;
	public static int CountConnectionError = 0;
	
	public static bool ReqFail = false;
//	public static LitJson.JsonData updateData;
	public static bool emergencyExit = false;
	
	public static string currentNameMode = "reels";
	public static string currentNameScene = "r";
	public static bool scratchMode = false;
	
	public static string mainLanguage = "eng";
	public static string[] addLocal = new string[] { "eng", "ru", "esp" };
	
	public static int finalBonusBalance = 0;
	public static int finalBonusBalanceExp = 0;
	
	public static string nameGameSpecialWM = "";
	public static int ticketPrice = 20;
	
	public static bool UkraineMode = false;
	public static bool disableKeys = false;
	public static int typeOfLogo = 0;
	
	#endregion

	public static bool AutoButtonClick = false;

	protected void Awake ()
	{
		//if (DeviceId.Length == 0) deviceId = GetMD5 (SystemInfo.deviceUniqueIdentifier + SystemInfo.deviceModel + SystemInfo.deviceName);
		SetDevId();
	}
	
	public static void SetDevId()
	{
		if (DeviceId.Length == 0)
		{
#if UNITY_STANDALONE_WIN

			if(PlayerPrefs.GetString("DevIDKey").Length == 0){
				deviceId = GetMD5 (SystemInfo.deviceUniqueIdentifier + SystemInfo.deviceModel + SystemInfo.deviceName);
				PlayerPrefs.SetString("DevIDKey", deviceId);
				PlayerPrefs.Save();
			}
			else
				deviceId = PlayerPrefs.GetString("DevIDKey");
#else
//			deviceId = GetMD5 (SystemInfo.deviceUniqueIdentifier + SystemInfo.deviceModel + SystemInfo.deviceName);
#endif
		}
	}

	// Use this for initialization
	protected void Start ()
	{
		if (ChangeCamera) Camera.main.orthographicSize = sizeCamera;
	}
	
	public static string GetMD5 (string text)
	{
//		MD5 md5Hasher = MD5.Create ();
//		byte[] data = md5Hasher.ComputeHash (Encoding.Default.GetBytes (text));
//		
//		StringBuilder sBuilder = new StringBuilder ();
//		
//		for (int i = 0; i < data.Length; i++)
//			sBuilder.Append (data[i].ToString ("x2"));
//		
//		return sBuilder.ToString ();
		return "";
	}
}
