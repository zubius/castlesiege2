﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;

public class AdsOffComponent : GameComponentBase, IGameComponent{
	
	private int selectedItemIndex;
	private PurchasableItem[] items;
	
	public override event ru.appforge.logic.DLogicEvent OnResult;

	private string[][] itemsName;

	IEnumerator Start () {
		Debug.Log(IsAdEnabled);
		yield return new WaitForEndOfFrame();
		UnibillInit ();
	}

	void UnibillInit ()
	{
		if (UnityEngine.Resources.Load ("unibillInventory.json") == null) {
			Debug.LogError ("You must define your purchasable inventory within the inventory editor!");
			this.gameObject.SetActive (false);
			return;
		}
		// We must first hook up listeners to Unibill's events.
		Unibiller.onBillerReady += onBillerReady;
		Unibiller.onTransactionsRestored += onTransactionsRestored;
		Unibiller.onPurchaseCancelled += onCancelled;
		Unibiller.onPurchaseFailed += onFailed;
		Unibiller.onPurchaseCompleteEvent += onPurchased;
		// Now we're ready to initialise Unibill.
		Unibiller.Initialise ();
		items = Unibiller.AllPurchasableItems;
		RestoreItem();
		//Hide ();
	}
	
	public override void Show (params object[] args)
	{
		//this.transform.position = new Vector3(0, this.transform.position.y, this.transform.position.z);
	}

	public override void Hide (params object[] args)
	{
		//this.transform.position = new Vector3(-1000f, this.transform.position.y, this.transform.position.z);
	}
	
	/// <summary>
	/// This will be called when Unibill has finished initialising.
	/// </summary>
	private void onBillerReady(UnibillState state) {
		UnityEngine.Debug.Log("onBillerReady:" + state);
	}
	
	/// <summary>
	/// This will be called after a call to Unibiller.restoreTransactions().
	/// </summary>
	private void onTransactionsRestored (bool success) {
		Debug.Log("Transactions restored.");
		if(success){
			SetPurchase();
			OnResult (this, new LogicEventArgs ("EventOnTransactionRestored"));
		}
	}
	
	/// <summary>
	/// This will be called when a purchase completes.
	/// </summary>
	private void onPurchased(PurchaseEvent e) {
		IsAdEnabled = false;
		OnResult (this, new LogicEventArgs ("EventOnPurchased"));
		Debug.Log("Purchase OK: " + e.PurchasedItem.Id);
		Debug.Log ("Receipt: " + e.Receipt);
		Debug.Log(string.Format ("{0} has now been purchased {1} times.",
		                         e.PurchasedItem.name,
		                         Unibiller.GetPurchaseCount(e.PurchasedItem)));
	}
	
    

	/// <summary>
	/// This will be called if a user opts to cancel a purchase
	/// after going to the billing system's purchase menu.
	/// </summary>
	private void onCancelled(PurchasableItem item) {
		OnResult (this, new LogicEventArgs ("EventOnCancelled"));
		Debug.Log("Purchase cancelled: " + item.Id);
	}
	
	/// <summary>
	/// This will be called is an attempted purchase fails.
	/// </summary>
	private void onFailed(PurchasableItem item) {
		OnResult (this, new LogicEventArgs ("EventOnFailed"));
		Debug.Log("Purchase failed: " + item.Id);
	}

	public void BuyItem(params object[] args){
		args = args[0] as object[];
		selectedItemIndex = (int)args[0];
		Unibiller.initiatePurchase(items[0]);
	}

	public void RestoreItem(){
		Unibiller.restoreTransactions();
	}

	void SetPurchase ()
	{
		Debug.Log (Unibiller.GetPurchaseCount (items [0]));
		if (Unibiller.GetPurchaseCount (items [0]) > 0) {
			IsAdEnabled = false;
		}
	}

	void OnDestroy() {
		Unibiller.onBillerReady -= onBillerReady;
		Unibiller.onTransactionsRestored -= onTransactionsRestored;
		Unibiller.onPurchaseCancelled -= onCancelled;
		Unibiller.onPurchaseFailed -= onFailed;
		Unibiller.onPurchaseCompleteEvent -= onPurchased;
		#if !UNITY_WP8
		Resources.UnloadUnusedAssets();
		#endif
	}

	public static bool IsAdEnabled {
		get {
			return PlayerPrefs.GetInt("ads_enabled", 1) == 1;
		}

		private set {
			if (value)
				PlayerPrefs.SetInt("ads_enabled", 1);
			else
				PlayerPrefs.SetInt("ads_enabled", 0);
			PlayerPrefs.Save();
		}
	}
}
