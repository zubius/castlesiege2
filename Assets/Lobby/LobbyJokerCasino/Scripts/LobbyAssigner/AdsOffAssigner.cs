﻿using UnityEngine;
using System.Collections;
using ru.appforge.logic;
using System.Collections.Generic;

public class AdsOffAssigner : AssignerBase, IAssigner
{		
	protected override void HandleOnGlobalEvent (object sender, LogicEventArgs args)
	{	
		switch (args.EventName) {
		case "on_show_items"		: Do ("Show"); break;
		case "on_hide_items"		: Do ("Hide"); break;
		case "on_buy_items"			: Do ("BuyItem", args.Args); break;
		case "on_restore_items"		: Do ("RestoreItem"); break;
		}
	}
	
	protected override void HandleOnResult (object sender, LogicEventArgs args)
	{
		Debug.LogError(args.EventName);
		switch (args.EventName) {
		case "EventOnPurchased":
			SendActiveLogic ("end_purchased_items");
			break;
		case "EventOnCancelled":
			SendActiveLogic ("end_cancelled_items");
			break;
		case "EventOnFailed":
			SendActiveLogic ("end_failed_items");
			break;
		case "EventOnTransactionRestored":
			SendActiveLogic ("end_restored_items");
			break;
		case "EventMenuRdy":
			SendActiveLogic ("end_rdy");
			break;
		}
	}
}
