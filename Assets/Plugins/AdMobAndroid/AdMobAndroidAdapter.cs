using UnityEngine;
using System.Collections;


public class AdMobAndroidAdapter : MonoBehaviour
{
#if UNITY_ANDROID
	public string adMobPubliserId;
	private bool _bannerCreated;
	
	
	IEnumerator Start()
	{
		// keep this game object alive because if we are backgrounded then returned from the background we need to recreate the banner
		DontDestroyOnLoad( gameObject );
		adMobPubliserId = "ca-app-pub-3936176332568699/5474063486";
		yield return new WaitForEndOfFrame();

		if( !_bannerCreated )
			createBanner();
		_bannerCreated = true;
	}
	
	
	void OnApplicationPause( bool isPaused )
	{
		if( !isPaused )
			createBanner();
	}
	
	
	private void createBanner()
	{
		if (IsAdEnabled) {
			AdMobAndroid.init( adMobPubliserId );
			AdMobAndroid.createBanner( AdMobAndroidAd.smartBanner, 0, 1 );
		}
	}

	public static bool IsAdEnabled {
		get {
			return PlayerPrefs.GetInt("ads_enabled", 1) == 1;
		}
		
		private set {
			if (value)
				PlayerPrefs.SetInt("ads_enabled", 1);
			else
				PlayerPrefs.SetInt("ads_enabled", 0);
			PlayerPrefs.Save();
		}
	}
#endif
}
