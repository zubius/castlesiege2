using UnityEngine;
using System.Collections.Generic;


public class AdMobUIManager : MonoBehaviour
{
#if UNITY_ANDROID
	void OnGUI()
	{
		float yPos = 5.0f;
		float xPos = 5.0f;
		float width = ( Screen.width >= 800 || Screen.height >= 800 ) ? 320 : 160;
		float height = ( Screen.width >= 800 || Screen.height >= 800 ) ? 70 : 35;
		float heightPlus = height + 10.0f;


		if( GUI.Button( new Rect( xPos, yPos, width, height ), "Init" ) )
		{
			AdMobAndroid.init( "YOUR_APP_ID_HERE" );
		}
	
	
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Set Test Devices" ) )
		{
			AdMobAndroid.setTestDevices( new string[] { "6D13FA054BC989C5AC41900EE14B0C1B", "123456678", "3BAB93112BBB08713B6D6D0A09EDABA1" } );
		}

		
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Create Smart Banner" ) )
		{
			// place it on the top
			AdMobAndroid.createBanner( AdMobAndroidAd.smartBanner, 0, 0 );
		}
		
		
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Create 320x50 banner" ) )
		{
			// place it on the top
			AdMobAndroid.createBanner( AdMobAndroidAd.phone320x50, 0, 0 );
		}
	
	
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Create 300x250 banner" ) )
		{
			// center it on the top
			AdMobAndroid.createBanner( AdMobAndroidAd.tablet300x250, 0, 0 );
		}
	
	
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Destroy Banner" ) )
		{
			AdMobAndroid.destroyBanner();
		}


		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Hide Banner" ) )
		{
			AdMobAndroid.hideBanner( true );
		}


		xPos = Screen.width - width - 5.0f;
		yPos = 5.0f;
	
	
		if( GUI.Button( new Rect( xPos, yPos, width, height ), "Refresh Ad" ) )
		{
			AdMobAndroid.refreshAd();
		}


		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Request Interstitial" ) )
		{
			AdMobAndroid.requestInterstital( "a14de56b4e8babd" );
		}
		
		
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Is Interstitial Ready?" ) )
		{
			var isReady = AdMobAndroid.isInterstitalReady();
			Debug.Log( "is interstitial ready? " + isReady );
		}
		
		
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Display Interstitial" ) )
		{
			AdMobAndroid.displayInterstital();
		}

	
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Show Banner" ) )
		{
			AdMobAndroid.hideBanner( false );
		}
	}
#endif
}
