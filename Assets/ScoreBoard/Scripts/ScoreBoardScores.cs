﻿using UnityEngine;
using System.Collections;
using com.shephertz.app42.paas.sdk.csharp.game;
using System.Collections.Generic;
using System.Linq;

public class ScoreBoardScores : ScoreBoardBase
{
    public SpriteSmoothlyShow background;
    public tk2dSimpleButton buttonBack;
    public tk2dTextMesh messageMesh;

	public bool InitBtns = true;

    public ScoreTable scoreTable;

    // Use this for initialization    
    public override void Initialization ()
    {
        base.Initialization ();
        buttonBack.OnClickEnable += (sender, args) => Close (sender, args);

		if (InitBtns) {
			ButtonsBase buttons = GameObject.Find("Buttons").GetComponent<ButtonsBase>();
			buttons.buttons = new System.Collections.Generic.List<tk2dSimpleButton>();
			buttons.buttons.Add(buttonBack);
			buttons.Start();
		}
    }

    public override void Close (params object[] args)
    {
        if (!isActive) return;
        base.Close (args);
        scoreTable.Hide ();
        Reset ();
        background.renderer.enabled = true;
        background.Hide (() => {
            if (methodCallBack != null)
                methodCallBack ();
        });


    }

    public override void Reset (params object[] args)
    {
        SetRendererButton (buttonBack, false);
        background.renderer.enabled = false;
        messageMesh.renderer.enabled = false;
        scoreTable.Reset ();
    }

    private void SetRendererButton (tk2dSimpleButton button, bool p)
    {
        button.renderer.enabled = p;
        button.gameObject.SetActive (p);
    }

    public override void Play (string command, System.Action callback = null)
    {
        base.Play (command, callback);

        background.renderer.enabled = true;

        background.Show (() => {
            SetRendererButton (buttonBack, true);
            isDone = true;
        });

        ScoreBoardData.userName = PlayerPrefs.GetString ("player_name", "");
        if (ScoreBoardData.userName.Length == 0) {
            OnExceptionCallBack ("404", "", "");
            return;
        }

        switch (command) {
            case "get_user": GetScoresByUser (); break;
            case "get_top": GetTopNRankers (); break;
        }
    }

    public override void OnResponceCallBack (Game gameResponce)
    {
        if (!isActive) return;
        List<ScoreTableItem> items = new List<ScoreTableItem> ();
        foreach (var item in gameResponce.scoreList)
            items.Add (new ScoreTableItem (item.userName, item.value));

		if (this)
			StartCoroutine (ShowResponce (items));
    }

    private IEnumerator ShowResponce (List<ScoreTableItem> items)
    {
        while (isDone == false)
            yield return new WaitForEndOfFrame ();

        scoreTable.Show (items);
    }

    public override void OnExceptionCallBack (string httpErrorCode, string message, string details)
    {
        if (!isActive) return;
        try {
            message = httpErrorCode == "404" ? "You have not saved your result" : string.Format ("Error: {0}\n{1}\n{2}", httpErrorCode, message, details);
        } catch (System.Exception e) { Debug.LogError (e.Message); }

		if (this)
        	StartCoroutine (ShowException (message));
    }

    private IEnumerator ShowException (string message)
    {
        while (isDone == false)
            yield return new WaitForEndOfFrame ();

		if (this) {
	        messageMesh.renderer.enabled = true;
	        messageMesh.text = message;
			messageMesh.Commit();
		}
    }
}
