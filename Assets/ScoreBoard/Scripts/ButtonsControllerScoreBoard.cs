using UnityEngine;
using System.Collections;

public class ButtonsControllerScoreBoard : MonoBehaviour 
{

	// Use this for initialization
	void Start () {	}
	
	// Update is called once per frame
	void Update () { }
	
	public static bool sublitScore 	= false;
	public static bool leaderBoard 	= false;
	public static bool register 	= false;
	public static bool rename 		= false;
	
	public void SubmitScore()
	{
		sublitScore = true;

	}
	
	public void LeaderBoard()
	{
		leaderBoard = true;
	}
			
	public void Register()
	{
	}
	
	public void Rename()
	{
		rename = true;
	}
}
