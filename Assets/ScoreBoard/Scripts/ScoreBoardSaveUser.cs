﻿using UnityEngine;
using System.Collections;
using com.shephertz.app42.paas.sdk.csharp.game;

public class ScoreBoardSaveUser : ScoreBoardBase
{
    public SpriteSmoothlyShow background;
    public tk2dSimpleButton buttonBack;

	public bool InitBtns = true;

	bool isSilent = false;
	bool isNameChecked = false;

    string commandName = "";

    public ScoreSaveController mes;

    // Use this for initialization    
    public override void Initialization ()
    {
        base.Initialization ();
        ScoreBoardData.userName = PlayerPrefs.GetString ("player_name", "");
		for (int i = 0; i < 12; i++)
			ScoreBoardData.gameScore += GetScore(i);

		if (InitBtns) {
			ButtonsBase buttons = GameObject.Find("Buttons").GetComponent<ButtonsBase>();
			buttons.buttons = new System.Collections.Generic.List<tk2dSimpleButton>();
			buttons.buttons.Add(buttonBack);
			buttons.Start();
		}
    }

	private int GetScore(int level) {
		return PlayerPrefs.GetInt("score_"+level.ToString());
	}

    public override void Close (params object[] args)
    {
//        if (!isActive) return;
        base.Close (args);
        Reset ();
        background.renderer.enabled = true;
        mes.Reset ();
        background.Hide (() => { if (methodCallBack != null) methodCallBack (); });
    }

    public override void Reset (params object[] args)
    {
        buttonBack.renderer.enabled = false;
//        background.renderer.enabled = false;
    }

	public void SaveScore(int score, bool silent) {
		isSilent = silent;
		ScoreBoardData.gameScore = score;
		methodCallBack = null;
		isDone = false;
		if (ScoreBoardData.userName.Length > 0 && ScoreBoardData.gameScore > 0) SaveScore ();
		isDone = true;
	}

	public void CheckName(bool silent, System.Action callBack = null) {
		isSilent = silent;
		if (this)
			StartCoroutine (CheckName (callBack));
	}

	private IEnumerator CheckName(System.Action callBack = null) {
		ScoreBoardData.userName = PlayerPrefs.GetString ("player_name", "");
		Debug.Log(ScoreBoardData.userName);
		if (ScoreBoardData.userName.Length == 0) {
			buttonBack.renderer.enabled = true;
			ShowEnterName ("Enter name");
		} else {
			isNameChecked = true;
		}

		while (!isNameChecked) yield return new WaitForEndOfFrame();

		Close();
		if (callBack != null) callBack ();
	}

    public override void Play (string command, System.Action callback = null)
    {
        base.Play (command, callback);

        background.renderer.enabled = true;
        background.Show (() => {
            buttonBack.renderer.enabled = true;

			if (ScoreBoardData.userName.Length > 0 && ScoreBoardData.gameScore > 0) SaveScore ();
            isDone = true;
        });
    }

    private void SaveScore ()
    {
        SaveUserScore ();
        commandName = "save_score";
        mes.ShowText ("Please wait...");
    }

    private void ShowEnterName (string message)
    {
        mes.ShowEnterName (message, (result) => {
            string p = result as string;
            if (p == null || p.Length == 0) ShowEnterName ("This field can't be blank");
            else {
                ScoreBoardData.userName = p;
                mes.ShowText ("Processing");
				buttonBack.Disable();
                commandName = "get_score";
                GetScoresByUser ();
            }
        });
    }

    public override void OnResponceCallBack (Game gameResponce)
    {
//        if (!isActive) return;
        switch (commandName) {
            case "get_score": ShowEnterName ("Name already in use"); ScoreBoardData.userName = ""; break;
            case "save_score":
				PlayerPrefs.SetString ("player_name", ScoreBoardData.userName);
				PlayerPrefs.Save();
				buttonBack.Enable();
			Debug.Log("Scores Saved");
				if (isSilent) return;
                mes.ShowText ("Success!");
				isNameChecked = true;
                break;
        }
    }

    public override void OnExceptionCallBack (string httpErrorCode, string message, string details)
    {
//        if (!isActive) return;
//		if (isSilent) return;
		if (httpErrorCode == "9042") {
			mes.ShowText ("No Internet connection");
			buttonBack.Enable();
		}

        if (commandName == "get_score") {
            if (httpErrorCode == "404") SaveScore ();

        } else {
            try {
                message = httpErrorCode == "404" ? "You have not saved your result" : string.Format ("Error {0}\n{1}\n{2}", httpErrorCode, message, details);
            } catch (System.Exception e) { Debug.LogError (e.Message); }
        }
    }
}
