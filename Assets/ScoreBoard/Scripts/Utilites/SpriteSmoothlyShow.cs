﻿using UnityEngine;
using System.Collections;

public class SpriteSmoothlyShow : MonoBehaviour
{  
    public float timeoutShow = 1.5f;
    public float maxAlpha = 0.9f;

	SpriteRenderer sprite;

	void Awake ()
	{
		sprite = this.GetComponent <SpriteRenderer>();
	}

	public void Show (System.Action callBack = null)
    {
		StartCoroutine (Showing (callBack));
    }

	public void Hide (System.Action callBack=null)
    {
        StartCoroutine (Hidding (callBack));
    }

	private IEnumerator Showing (System.Action callBack)
    {
        float counterTime = 0.0f;

        while (counterTime < timeoutShow) {
            if ((counterTime += Time.deltaTime) > timeoutShow) counterTime = timeoutShow;
            SetBackgroundAlpha (maxAlpha * counterTime / timeoutShow);
            yield return new WaitForEndOfFrame ();
        }

		if (callBack != null) callBack ();
    }

	private IEnumerator Hidding (System.Action callBack)
    {
        float counterTime = timeoutShow;

        while (counterTime > 0.0f) {
            if ((counterTime -= Time.deltaTime) < 0.0f) counterTime = 0.0f;
            SetBackgroundAlpha (maxAlpha * counterTime / timeoutShow);
            yield return new WaitForEndOfFrame ();
        }

		if (callBack != null) callBack ();
    }

    private void SetBackgroundAlpha (float p)
    {
        Color color = sprite.color;
        color.a = p;
        sprite.color = color;
    }
}
