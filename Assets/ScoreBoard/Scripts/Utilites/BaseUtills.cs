﻿using UnityEngine;
using System.Collections;
using System;

public class BaseUtills : MonoBehaviour
{
	public void Pause (int countFrame, Action method)
	{
		StartCoroutine (Wait (countFrame, method));
	}

	public void Pause (float timeSecond, Action method)
	{
		StartCoroutine (Wait (timeSecond, method));
	}

	IEnumerator Wait (int count, Action method)
	{
		int repeat = 0;
		while (repeat++ < count)
			yield return new WaitForEndOfFrame ();
		if (method != null)
			method ();
	}

	IEnumerator Wait (float timeSecond, Action method)
	{
		yield return new WaitForSeconds (timeSecond);
		if (method != null)
			method ();
	}
}
