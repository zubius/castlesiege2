﻿using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.game;
using com.shephertz.app42.paas.sdk.csharp.log;
using System.Collections;
using System.Net;
using UnityEngine;

public class ScoreBoardController : GameComponentBase, IGameComponent
{
    public ScoreBoardBase scoreSave;
    public ScoreBoardBase scoreGetByUser;


	public override event ru.appforge.logic.DLogicEvent OnResult;

    IEnumerator Start ()
    {
        scoreSave.Reset ();
        scoreGetByUser.Reset ();

        yield return new WaitForEndOfFrame ();

		OnResult(this, new ru.appforge.logic.LogicEventArgs("EventLeaderBoardCreated"));
    }

	public void SaveScore(params object[] args) {
		int score = (int)args[0];
		((ScoreBoardSaveUser)scoreSave).SaveScore(score, true);
	}

	public void GetTop(params object[] args) {
		((ScoreBoardSaveUser)scoreSave).CheckName(false, () => {
			scoreGetByUser.Play ("get_top");
		});
	}
}

