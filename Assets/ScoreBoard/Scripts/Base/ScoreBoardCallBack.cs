﻿using UnityEngine;
using System.Collections;
using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.game;
using System.Collections.Generic;
using System;

public class ScoreBoardCallBack : App42CallBack
{
    public event System.Action<Game> OnResponce;
    public event System.Action<string, string, string> OnExceptionResponce;

    public void OnSuccess (object responce)
    {
        Game game = (Game) responce;
        if (OnResponce != null) OnResponce (game);
    }

    public void OnException (System.Exception exception)
    {
        string httpErrorCode = "", message = "", details = "";
        Debug.Log (exception.Message);
        var data = ParseJson (exception.Message);

		if (data.Count == 0) {
			httpErrorCode = "9042";
		}

        try {
            httpErrorCode = data["httpErrorCode"];
            message = data["message"];
            details = data["details"];
        } catch (Exception exceptionTry) { Debug.LogError (exceptionTry.Message); }


        if (OnExceptionResponce != null) OnExceptionResponce (httpErrorCode, message, details);
    }

    static Dictionary<string, string> ParseJson (string res)
    {
        var lines = res.Split (",".ToCharArray (), StringSplitOptions.RemoveEmptyEntries);
        var ht = new Dictionary<string, string> (20);
        var st = new Stack<string> (20);

        for (int i = 0; i < lines.Length; ++i) {
            var line = lines[i];
            var pair = line.Split (":".ToCharArray (), 2, StringSplitOptions.RemoveEmptyEntries);

            if (pair.Length == 2) {
                var key = ClearString (pair[0]);
                var val = ClearString (pair[1]);

                if (val == "{") {
                    st.Push (key);
                } else {
                    if (st.Count > 0) {
                        key = string.Join ("_", st.ToArray ()) + "_" + key;
                    }

                    if (ht.ContainsKey (key)) {
                        ht[key] += "&" + val;
                    } else {
                        ht.Add (key, val);
                    }
                }
            } else if (line.IndexOf ('}') != -1 && st.Count > 0) {
                st.Pop ();
            }
        }

        return ht;
    }

    private static string ClearString (string p)
    {
        string output = "";
        for (int i = 0; i < p.Length; i++)
            if (p[i] == '\"' || p[i] == '{' || p[i] == '}') output += ' ';
            else output += p[i];
        return output.Trim ();
    }

}