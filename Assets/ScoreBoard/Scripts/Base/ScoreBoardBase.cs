using UnityEngine;
using System.Collections;
using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.game;
using System.Net;

public static class ScoreBoardData
{
    public static string userName = string.Empty;
    public static string gameName = "CtC";
    public static double gameScore = 0.0;
}

public class ScoreBoardBase : BaseUtills
{
    protected System.Action methodCallBack = null;

    public bool isDone { get; set; }
    public bool isActive { get; set; }

    private ServiceAPI service = null;
    private ScoreBoardService scoreBoardService = null;
    private ScoreBoardCallBack unityCallBack = null;

#if UNITY_EDITOR
    public static bool Validator (object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; }
#endif

    void Start ()
    {
        Initialization ();
    }

    public virtual void Initialization ()
    {
        unityCallBack = new ScoreBoardCallBack ();
        unityCallBack.OnResponce += OnResponceCallBack;
        unityCallBack.OnExceptionResponce += OnExceptionCallBack;

#if UNITY_EDITOR
        ServicePointManager.ServerCertificateValidationCallback = Validator;
#endif
		service = new ServiceAPI ("26813493bb846358b7a744798b6d3ee3dc822ab7f8ef80822affc13830e8cbcf", "06615c8153af8772f63d6699ac1d3e76659aea67dc4e3a7950369bd35068170c");
        scoreBoardService = service.BuildScoreBoardService ();
    }

    public void GetScoresByUser ()
    {
        scoreBoardService.GetScoresByUser (ScoreBoardData.gameName, ScoreBoardData.userName, unityCallBack);
    }

    public void SaveUserScore ()
    {
        scoreBoardService.SaveUserScore (ScoreBoardData.gameName, ScoreBoardData.userName, ScoreBoardData.gameScore, unityCallBack);
    }

    public void GetTopNRankers ()
    {
        scoreBoardService.GetTopNRankers (ScoreBoardData.gameName, 12, unityCallBack);
    }

    public virtual void OnResponceCallBack (Game gameResponce)
    {

    }

    public virtual void OnExceptionCallBack (string httpErrorCode, string message, string details)
    {

    }

    public virtual void Play (string command, System.Action callback = null)
    {
        methodCallBack = callback;
        isDone = false;
        isActive = true;
    }

    public virtual void Close (params object[] args)
    {
        isActive = false;
    }

    public virtual void Reset (params object[] args)
    {

    }
}
