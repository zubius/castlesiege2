﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ScoreTable : MonoBehaviour
{
    public tk2dTextMesh prototypeBottomRight;
	public tk2dTextMesh prototypeTopLeft;

    private const int COUNT_RECORDS = 12;

	tk2dTextMesh[] meshUsers = new tk2dTextMesh[COUNT_RECORDS];
	tk2dTextMesh[] meshScores = new tk2dTextMesh[COUNT_RECORDS];

    // Use this for initialization
    void Awake ()
    {
        float startPosition = prototypeTopLeft.transform.localPosition.y;
        float dy = (startPosition - prototypeBottomRight.transform.localPosition.y) / (COUNT_RECORDS - 1);

        Vector3 prototypePositionTop = prototypeTopLeft.transform.localPosition;
        Vector3 prototypePositionBottom = prototypeBottomRight.transform.localPosition;

        for (int i = 0; i < COUNT_RECORDS; i++) {
			tk2dTextMesh meshUser = Instantiate (prototypeTopLeft) as tk2dTextMesh;
            meshUser.transform.parent = this.transform;
            meshUser.transform.localScale= new Vector3 (1.0f, 1.0f, 1.0f);
            meshUser.transform.localPosition = prototypePositionTop;
            meshUser.name = "User " + (i + 1).ToString ();
            meshUsers[i] = meshUser;
            prototypePositionTop.y -= dy;

			tk2dTextMesh meshScore = Instantiate (prototypeBottomRight) as tk2dTextMesh;
            meshScore.transform.parent = this.transform;
            meshScore.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
            meshScore.transform.localPosition = prototypePositionBottom;
            meshScore.name = "Score " + (COUNT_RECORDS - i).ToString ();
            meshScores[COUNT_RECORDS - i - 1] = meshScore;
            prototypePositionBottom.y += dy;
        }

        Reset ();

        DestroyObject (prototypeTopLeft);
        DestroyObject (prototypeBottomRight);

        Hide ();
    }

    public void Show (List<ScoreTableItem> items)
    {
        Reset ();

        if (items.Count > 1) Sorting (items);

		var me = items.Where(x => x.UserName == ScoreBoardData.userName).ToList();
		if (me.Count == 0) {
			items[items.Count-2].UserName = "...";
			items[items.Count-2].UserScore = -1.0;
			items[items.Count-1].UserName = ScoreBoardData.userName;
			items[items.Count-1].UserScore = ScoreBoardData.gameScore;
		}

        for (int i = 0; i < items.Count && i < COUNT_RECORDS; i++) {
            meshUsers[i].text = items[i].UserName;
			meshUsers[i].Commit();
			meshScores[i].text = items[i].UserScore > 0 ? items[i].UserScore.ToString () : "";
			meshScores[i].Commit();
        }
    }

    private void Sorting (List<ScoreTableItem> items)
    {
        int n = items.Count;
        while (n-- > 0)
            for (int i = 0; i < items.Count - 1; i++) {
                if (items[i].UserScore < items[i + 1].UserScore) {
                    ScoreTableItem temp = items[i];
                    items[i] = items[i + 1];
                    items[i + 1] = temp;
                } else {
                    if (items[i].UserScore == items[i + 1].UserScore && items[i].UserName == items[i + 1].UserName)
                        items.RemoveAt (i);
                }
            }
    }

    public void Hide ()
    {
        Reset ();
    }

    public void Reset ()
    {
        foreach (var item in meshScores) item.text = "";
        foreach (var item in meshUsers) item.text = "";
    }
}

public class ScoreTableItem
{
    

    public string UserName { get; set; }
    public double UserScore { get; set; }

    public ScoreTableItem (string userName, double score)
    {
        this.UserName = userName;
        this.UserScore = score;
    }
}