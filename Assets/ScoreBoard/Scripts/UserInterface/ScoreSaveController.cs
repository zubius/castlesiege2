﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ScoreSaveController : MonoBehaviour
{
    public tk2dSimpleButton buttonOk;
    public tk2dTextMesh messageMeshError;
	public tk2dTextMesh messageMeshInfo;

    public bool ShowGUI { get; set; }

    private Queue<Action<object>> methodCallBack = new Queue<Action<object>> ();

    private string nameForGUI = "";

    // Use this for initialization
    void Awake ()
    {
        buttonOk.OnClickEnable += OnClickButtonOk;
    }

    void Start ()
    {
        Reset ();
    }

    void OnClickButtonOk (object sender, EventArgs e)
    {
        if (methodCallBack.Count > 0) {
            var method = methodCallBack.Dequeue ();
            if (methodCallBack != null) method (Filter (nameForGUI));
        }
    }

    // Update is called once per frame
    void OnGUI ()
    {
        if (ShowGUI) {
			nameForGUI = GUI.TextArea (new Rect (Screen.width / 2 - (Screen.width * 0.4f), (int) (Screen.height * 0.3), Screen.width * 0.8f, 25), Filter (nameForGUI));
        }
    }

    private string Filter (string result)
    {
        result = "";
        for (int i = 0; i < nameForGUI.Length && i < 12; i++) {
//            if (Char.IsLetterOrDigit (nameForGUI[i]) && nameForGUI[i] != '\n')
			if (System.Text.RegularExpressions.Regex.IsMatch(nameForGUI[i].ToString(), "^[a-zA-Z0-9]*$"))
                result += nameForGUI[i];
        }

        return result;
    }

    public void Reset ()
    {
        ShowGUI = false;
		if (buttonOk) {
	        buttonOk.gameObject.SetActive (false);
	        messageMeshError.renderer.enabled = false;
	        messageMeshInfo.renderer.enabled = false;
		}
    }

    public void ShowMessage (string message, Action<object> callBack)
    {
        Reset ();
        messageMeshInfo.renderer.enabled = true;
        messageMeshInfo.text = message;
		messageMeshInfo.Commit();

        buttonOk.gameObject.SetActive (true);
        methodCallBack.Enqueue (callBack);
    }

    public void ShowEnterName (string message, Action<object> callBack)
    {
        ShowMessage (message, callBack);
        ShowGUI = true;
    }

    public void ShowText (string message)
    {
        Reset ();
		if (messageMeshInfo) {
	        messageMeshInfo.renderer.enabled = true;
	        messageMeshInfo.text = message;
			messageMeshInfo.Commit();
		}
    }
}
